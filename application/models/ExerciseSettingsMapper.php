<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ExerciseSettingsMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_ExerciseSettings';
    }

    /**
     * @param Application_Model_ExerciseSettings $settings
     * @return int
     */
    public function save(Application_Model_ExerciseSettings $settings)
    {
        $table = $this->getDbTable();

        $data = array('exeset_amount'           => $settings->Amount,
                      'exeset_hour'             => $settings->Hour,
                      'exeset_quarterpast'      => $settings->QuarterPast,
                      'exeset_halfhour'         => $settings->HalfHour,
                      'exeset_quarterto'        => $settings->QuarterTo,
                      'exeset_minutesincrement' => $settings->MinutesIncrement,
                      'exeset_seconds'          => $settings->Seconds,
                      'exeset_mode'             => $settings->Mode,
                      'exeset_assistance'       => $settings->Assistance,
                      'exeset_subtitles'        => $settings->Subtitles,
                );

        if ($settings->Id === null) {

//            $db = $table->getAdapter();
//            $db->query('LOCK TABLE ExerciseSettings WRITE');
//
//            $select = $table->select()
//                            ->from($table,
//                                   array('new_exeset_id' => new Zend_Db_Expr('MAX(exeset_id)+1')));
//
//            if ($row = $table->fetchRow($select) ) {
//                $row = $row->toArray();
//                $data['exeset_id'] =  $row['new_exeset_id'];
//            }
//            $table->insert($data);
//
//            $db->query('UNLOCK TABLES');

            return $settings->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('exeset_id = ?' => $settings->Id));
        }
    }

    /**
     * @param int $id
     * @throws Exception
     * @return array
     */
    public function readRow($id = null)
    {
        $table = $this->getDbTable();

        $column_map = array('id'               => 'exeset_id',
                            'amount'           => 'exeset_amount',
                            'hour'             => 'exeset_hour',
                            'quarterPast'      => 'exeset_quarterpast',
                            'halfHour'         => 'exeset_halfhour',
                            'quarterTo'        => 'exeset_quarterto',
                            'minutesIncrement' => 'exeset_minutesincrement',
                            'seconds'          => 'exeset_seconds',
                            'mode'             => 'exeset_mode',
                            'assistance'       => 'exeset_assistance',
                            'subtitles'        => 'exeset_subtitles',
                      );

        if ($id === null) {
            $select = $table->select()
                            ->from($table,
                                   $column_map
                            )
                            ->order('exeset_id DESC')
                            ->order('exeset_timestamp DESC')
                            ->limit(1)
            ;
        } else {
            $select = $table->select()
                            ->from($table,
                                   $column_map
                            )
                            ->where('exeset_id = :id')
                            ->bind(array(':id' => $id))
                            ->order('exeset_timestamp DESC')
                            ->limit(1)
            ;
        }

        if ($row = $table->fetchRow($select) ) {
            $row = $row->toArray();

            $row['minutes'] = ($row['minutesIncrement'] > 0);

            return $row;
        }
        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param int $id
     * @return Application_Model_ExerciseSettings
     */
    public function read($id = null)
    {
        if ($row = $this->readRow($id)) {
            return new Application_Model_ExerciseSettings($row);
        };
    }

    public function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        // @todo Implement
        ;
    }
}
