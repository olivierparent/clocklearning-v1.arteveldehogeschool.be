<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_User extends Application_Model_PersonAbstract
{
    const ROLE_CLIENT = 'CLIENT';
    const ROLE_PUPIL  = 'PUPIL';

    /**
     * @var string
     */
    protected $_birthday;

    /**
     * @var integer
     */
    protected $_groupId;

    /**
     * @var array
     */
    protected $_sets = array();

    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
    }

    /**
     * @param array $properties
     * @return array
     */
    public function toArray(array $properties = array('id', 'username', 'givenname', 'familyname', 'birthday', 'groupId', 'exerciseSettingsId', 'moduleSettingsId') )
    {
        return parent::toArray($properties);
    }
}
