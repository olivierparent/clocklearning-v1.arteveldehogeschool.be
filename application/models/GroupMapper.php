<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 * *****************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Application_Model_GroupMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_Group';
    }

    /**
     * @param Application_Model_Group $group
     * @return int
     */
    public function save(Application_Model_Group $group)
    {
        $table = $this->getDbTable();

        $data = array('grp_id'    => $group->Id,
                      'grp_name'  => $group->Name,
                      'sup_id'    => $group->SupervisorId,
                      'exeset_id' => $group->ExerciseSettingsId,
                      'modset_id' => $group->ModuleSettingsId,
        );

        if ($group->Id === null) {
            return $group->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('grp_id = ?' => $group->Id));
        }
    }

    /**
     * @param Application_Model_Supervisor $supervisor
     * @return Application_Model_Group
     * @throws Exception
     */
    public function readDefaultWhereSupervisor(Application_Model_Supervisor $supervisor)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->where('sup_id = :sup_id')
                        ->where('grp_name = :grp_name')
                        ->bind(
                            array(
                                ':sup_id'   => $supervisor->Id,
                                ':grp_name' => Application_Model_Group::DEFAULT_NAME,
                            )
                        )
                        ->order('grp_timestamp ASC')
                        ->limit(1)
        ;

//        Zend_Debug::dump($supervisor->Id);
//        Zend_Debug::dump($table->info(Zend_Db_Table::NAME));
//        Zend_Debug::dump($select->assemble());
//        exit;

        if ($row = $table->fetchRow($select) ) {

            return $this->_toObject($row);
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param Application_Model_Supervisor $supervisor
     * @param null $id
     * @return Application_Model_Group|array
     * @throws Exception
     */
    public function readWhereSupervisor(Application_Model_Supervisor $supervisor, $id = null)
    {
        $table = $this->getDbTable();

        if ($id) {
            $select = $table->select()
                            ->from($table,
                                array(
                                    'id'                 => 'grp_id',
                                    'name'               => 'grp_name',
                                    'supervisorId'       => 'sup_id',
                                    'exerciseSettingsId' => 'exeset_id',
                                    'moduleSettingsId'   => 'modset_id',
                                )
                            )
                            ->where('grp_id = :id')
                            ->where('sup_id = :supId')
                            ->order('grp_timestamp DESC')
                            ->limit(1)
                            ->bind(array(':id' => $id, ':supId' => $supervisor->Id))
            ;
            if ($row = $table->fetchRow($select)) {
                $row = $row->toArray();

                return new Application_Model_Group($row);
            }
        } else {
            $select = $table->select('*')
                            ->where('sup_id = :id')
                            ->bind(array(':id' => $supervisor->Id))
                            ->order('grp_name')
            ;
            if ($rowset = $table->fetchAll($select) ) {

                return $this->_toObjects($rowset);
            }
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }


    /**
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function readRow($id = 1)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table,
                            array(
                                'id'                 => 'grp_id',
                                'name'               => 'grp_name',
                                'supervisorId'       => 'sup_id',
                                'exerciseSettingsId' => 'exeset_id',
                                'moduleSettingsId'   => 'modset_id',
                            )
                        )
                        ->where('grp_id = :id')
                        ->order('grp_timestamp DESC')
                        ->bind(array(':id' => $id))
        ;
        if ($row = $table->fetchRow($select)) {
            $row = $row->toArray();

            return $row;
        }
        throw new Exception('Could not find record in database');
    }

    /**
     * @param int $id
     * @return Application_Model_Group
     */
    public function read($id = 1)
    {
        if ($row = $this->readRow($id) ) {

            return new Application_Model_Group($row);
        }
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_Group
     */
    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        $group = new Application_Model_Group();

        if ($row) {
            $group->Id                 = (int) $row['grp_id'];
            $group->Name               =       $row['grp_name'];
            $group->SupervisorId       = (int) $row['sup_id'];
            $group->ExerciseSettingsId = (int) $row['exeset_id'];
            $group->ModuleSettingsId   = (int) $row['modset_id'];
        }

        return $group;
    }
}
