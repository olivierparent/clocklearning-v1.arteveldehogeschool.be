<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_UserMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_User';
    }

    /**
     * @param Application_Model_User $user
     * @return int
     */
    public function save(Application_Model_User $user)
    {
        $table = $this->getDbTable();

        $data = array('usr_id'         => $user->Id,
                      'usr_username'   => $user->Username,
                      'usr_password'   => $user->Password,
                      'usr_givenname'  => $user->Givenname,
                      'usr_familyname' => $user->Familyname,
                      'usr_birthday'   => $user->Birthday,
                      'usr_role'       => $user->Role,
                      'grp_id'         => $user->GroupId,
                      'exeset_id'      => $user->ExerciseSettingsId,
                      'modset_id'      => $user->ModuleSettingsId,
                );

        if ($user->Id === null) {
            return $user->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('usr_id = ?' => $user->Id));
            return 0;
        }
    }

    /**
     * @param int $id
     * @throws Exception
     * @return array
     */
    public function readRow($id = 1)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table,
                               array(
                                   'id'                 => 'usr_id',
                                   'username'           => 'usr_username',
                                   'password'           => 'usr_password',
                                   'givenname'          => 'usr_givenname',
                                   'familyname'         => 'usr_familyname',
                                   'birthday'           => 'usr_birthday',
                                   'role'               => 'usr_role',
                                   'groupId'            => 'grp_id',
                                   'exerciseSettingsId' => 'exeset_id',
                                   'moduleSettingsId'   => 'modset_id',
                               )
                        )
                        ->where('usr_id = :id')
                        ->where('usr_active = TRUE')
                        ->where('usr_deleted = FALSE')
                        ->order('usr_timestamp DESC')
                        ->bind(array(':id' => $id))
        ;
        if ($row = $table->fetchRow($select) ) {
            $row = $row->toArray();

//            Zend_Debug::dump($row); exit;
            return $row;
        }
        throw new Exception('Could not find record in database');
    }

    /**
     * @param integer $id
     * @return Application_Model_User
     */
    public function read($id = 1)
    {
        $row = $this->readRow($id);

        return new Application_Model_User($row);
    }

    /**
     * @param Application_Model_Group $group
     * @return array Application_Model_User
     */
    public function readWhereGroup(Application_Model_Group $group)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->where('grp_id = :id')
                        ->where('NOT (usr_givenname = :givenname AND usr_familyname = :familyname)')
                        ->where('usr_active = TRUE')
                        ->where('usr_deleted = FALSE')
                        ->order(array('usr_familyname','usr_givenname'))
                        ->bind(
                            array(
                                ':id'         => $group->Id,
                                ':givenname'  => 'Default',
                                ':familyname' => 'User',
                            )
                        )
        ;

        $rowset = $table->fetchAll($select);

        return $this->_toObjects($rowset);
    }

    /**
     * @param Application_Model_Supervisor $supervisor
     * @param integer $id
     * @return Application_Model_User
     * @throws Exception
     */
    public function readWhereSupervisor(Application_Model_Supervisor $supervisor, $id = null)
    {
        $table = $this->getDbTable();
        $tableGroup = new Application_Model_DbTable_Group();

        if ($id) {
            $select = $table->select()
                            ->from(array('u' => $table->info(Zend_Db_Table::NAME)),
                                array(
                                    'id'                 => 'usr_id',
                                    'username'           => 'usr_username',
                                    'password'           => 'usr_password',
                                    'givenname'          => 'usr_givenname',
                                    'familyname'         => 'usr_familyname',
                                    'birthday'           => 'usr_birthday',
                                    'role'               => 'usr_role',
                                    'groupId'            => 'grp_id',
                                    'exerciseSettingsId' => 'exeset_id',
                                    'moduleSettingsId'   => 'modset_id',
                                )
                            )
                            ->setIntegrityCheck(false)
                            ->join(
                                array('g' => $tableGroup->info(Zend_Db_Table::NAME)),
                                'u.grp_id = g.grp_id',
                                array()
                            )
                            ->where('usr_id = :id')
                            ->where('sup_id = :supId')
                            ->where('usr_active = TRUE')
                            ->where('usr_deleted = FALSE')
                            ->order('usr_timestamp DESC')
                            ->limit(1)
                            ->bind(
                                array(
                                    ':id'    => $id,
                                    ':supId' => $supervisor->Id,
                                )
                            )
            ;
            if ($row = $table->fetchRow($select) ) {
                $row = $row->toArray();

                return new Application_Model_User($row);
            }
        } else {
            $select = $table->select()
                            ->setIntegrityCheck(false)
                            ->from(array('u' => $table->info(Zend_Db_Table::NAME)))
                            ->join(
                                array('g' => $tableGroup->info(Zend_Db_Table::NAME)),
                                'u.grp_id = g.grp_id')
                            ->where('sup_id = :id')
                            ->bind(array(':id' => $supervisor->Id))
                            ->order(array('usr_familyname', 'usr_givenname'))
            ;
            if ($rowset = $table->fetchAll($select) ) {

                return $this->_toObjects($rowset);
            }
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param $id UserId
     * @return bool success
     */
    public function delete($id)
    {
        $table = $this->getDbTable();

        return (bool) $table->delete(array('usr_id = ?' => $id));
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_User
     */
    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        $user = new Application_Model_User();

        if ($row) {
            $user->Id                 = (int) $row['usr_id'        ];
            $user->Username           =       $row['usr_username'  ];
            $user->Password           =       $row['usr_password'  ];
            $user->Givenname          =       $row['usr_givenname' ];
            $user->Familyname         =       $row['usr_familyname'];
            $user->Birthday           =       $row['usr_birthday'  ];
            $user->Role               =       $row['usr_role'      ];
            $user->GroupId            = (int) $row['grp_id'        ];
            $user->ExerciseSettingsId = (int) $row['exeset_id'     ];
            $user->ModuleSettingsId   = (int) $row['modset_id'     ];
        }

        return $user;
    }
}
