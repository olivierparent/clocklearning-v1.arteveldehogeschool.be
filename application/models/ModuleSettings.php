<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ModuleSettings extends Ahs_Model_Abstract
{
    const QUADRANTS_ALTERNATIVE = 'ALTERNATIVE';
    const QUADRANTS_OFFICIAL    = 'OFFICIAL';

    /**
     * @var int
     */
    protected $_id;

    /**
     * User 24-hour clock.
     *
     * @var bool
     */
    protected $_twentyfour;

    /**
     * Use official Dutch clock system. Accepted constants: QUADRANTS_OFFICIAL,
     * QUADRANTS_ALTERNATIVE.
     *
     * @var string
     */
    protected $_quadrants;

    /**
     * @var bool
     */
    protected $_sound;

    /**
     * @var bool
     */
    protected $_mascot;

	/**
     * @var bool
     */
    protected $_colour;

    /**
     * @var bool
     */
    protected $_colourSwitch;

    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (empty($options)) {
            $options = array(
                'twentyfour'   => true,
                'quadrants'    => self::QUADRANTS_OFFICIAL,
                'sound'        => false,
                'mascot'       => true,
                'colour'       => true,
                'colourSwitch' => false,
            );
        }
        parent::__construct($options);
    }

    /**
     * NL: Kloksysteem
     *
     * @param array $options
     */
    public function setQuadrantsForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * NL: Vierentwintiguursklok
     *
     * @param array $options
     */
    public function setTwentyfourForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * NL: Mascotte
     *
     * @param array $options
     */
    public function setMascotForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * NL: Geluid
     *
     * @param array $options
     */
    public function setSoundForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * NL: Kleur
     *
     * @param array $options
     */
    public function setColourForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * @param array $options
     */
    protected function subform(array $options = null)
    {
        foreach ($options as $field => $value) {
            $field = ucfirst($field);
            $this->$field = $value;
        }
    }
}
