<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_Group extends Ahs_Model_Abstract
{
    const DEFAULT_NAME = '--DEFAULT--';
    const DEFAULT_ACTIVE = true;
    const DEFAULT_DELETED = false;

	/**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_supervisorId;

    /**
     * @var string
     */
    protected $_name = self::DEFAULT_NAME;

	/**
     * @var boolean
     */
    protected $_active = self::DEFAULT_ACTIVE;

    /**
     * @var boolean
     */
    protected $_deleted = self::DEFAULT_DELETED;

    /**
     * @var integer
     */
    protected $_exerciseSettingsId;

    /**
     * @var integer
     */
    protected $_moduleSettingsId;

    /**
     * @var array
     */
    protected $_users = array();

    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
    }

    /**
     * @param array $properties
     * @return array
     */
    public function toArray(array $properties = array('id', 'name', 'exerciseSettingsId', 'moduleSettingsId'))
    {
        return parent::toArray($properties);
    }
}
