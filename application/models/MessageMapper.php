<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_MessageMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_Message';
    }

    /**
     * @param Application_Model_Message $message
     * @return integer
     */
    public function save(Application_Model_Message $message)
    {
        $table = $this->getDbTable();

        $data = array('msg_from'    => $message->From,
                      'msg_subject' => $message->Subject,
                      'msg_body'    => $message->Body,
                );
        switch ($message->Status) {
            case 'PENDING':
            case 'FAILED':
            case 'SENT':
                $data['msg_status'] = $message->Status;
                break;
            default:
                break;
        }

        if ($message->Id === null) {
            return $message->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('msg_id = ?' => $message->Id));
        }
    }

    /**
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function readRow($id = 1)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table,
                               array('id'      => 'msg_id',
                                     'from'    => 'msg_from',
                                     'subject' => 'msg_subject',
                                     'body'    => 'msg_body',
                                     'status'  => 'msg_status',
                               )
                        )
                        ->where('msg_id = :id')
                        ->order('msg_timestamp DESC')
                        ->bind(array(':id' => $id))
        ;
        if ($row = $table->fetchRow($select) ) {
            $row = $row->toArray();
//            Zend_Debug::dump($row); exit;

            return $row;
        }
        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param int $id
     * @return Application_Model_Message
     */
    public function read($id = 1)
    {
        $row = $this->readRow($id);
        return new Application_Model_Message($row);
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_Language
     */
    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        $message = new Application_Model_Message();

        if ($row) {
            $message->Id      = (int) $row['msg_id'     ];
            $message->From    =       $row['msg_from'   ];
            $message->Subject =       $row['msg_subject'];
            $message->Body    =       $row['msg_body'   ];
            $message->Status  =       $row['msg_status' ];
        }

        return $message;
    }
}
