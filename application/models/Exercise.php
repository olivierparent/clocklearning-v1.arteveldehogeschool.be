<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_Exercise extends Ahs_Model_Abstract
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_questions = null;

    /**
     * @var string
     */
    protected $_answers = null;

    /**
     * @var string
     */
    protected $_modes = null;

    /**
     * @var string
     */
    protected $_solution = null;

    /**
     * @var bool
     */
    protected $_correct = null;

    /**
     * @var string
     */
    protected $_timeStart = null;

    /**
     * @var string
     */
    protected $_timeEnd = null;

    /**
     * @var int
     */
    protected $_exerciseTypeId = null;

    /**
     * @var string
     */
    protected $_type = null;

    /**
     * @var int
     */
    protected $_setId;

    /**
     * @var array
     */
    protected $_attempts = array();

    /**
     * @param array $properties
     * @return array
     */
    public function toArray(array $properties = array('id', 'timeStart', 'timeEnd', 'exerciseTypeId', 'setId') )
    {
        return parent::toArray($properties);
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = (int) $id;
    }

    /**
     * @param bool|string $correct
     */
    public function setCorrect($correct)
    {
        $this->_correct = (bool) $correct;
    }

    /**
     * @param string|int $time
     */
    public function setTimeStart($time)
    {
        if (!empty($time)) {
            $this->_timeStart = is_int($time) ? date('Y-m-d H:i:s', (int) ($time / 1000)) : $time;
        }
    }

    /**
     * @param string|int $time
     */
    public function setTimeEnd($time)
    {
        if (!empty($time)) {
            $this->_timeEnd = is_int($time) ? date('Y-m-d H:i:s', (int) ($time / 1000)) : $time;
        }
    }

    /**
     * @param int $id
     */
    public function setExerciseTypeId($id)
    {
        $this->_exerciseTypeId = empty($id) ? null : (int) $id;
    }

    /**
     * @param int $id
     */
    public function setSetId($id)
    {
        $this->_setId = empty($id) ? null : (int) $id;
    }
}
