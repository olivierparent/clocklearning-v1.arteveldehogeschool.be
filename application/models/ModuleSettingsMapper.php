<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ModuleSettingsMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_ModuleSettings';
    }

    /**
     * @param Application_Model_ModuleSettings $settings
     * @return integer
     */
    public function save(Application_Model_ModuleSettings $settings)
    {
        $table = $this->getDbTable();

        $data = array('modset_twentyfour'    => $settings->Twentyfour,
                      'modset_quadrants'     => $settings->Quadrants,
                      'modset_sound'         => $settings->Sound,
                      'modset_mascot'        => $settings->Mascot,
                      'modset_colour'        => $settings->Colour,
                      'modset_colour-switch' => $settings->ColourSwitch,
        );

        if ($settings->Id === null) {

//            $db = $table->getAdapter();
//            $db->query('LOCK TABLE ModuleSettings WRITE');
//
//            $select = $table->select()
//                            ->from($table,
//                                   array('new_modset_id' => new Zend_Db_Expr('MAX(modset_id)+1')));
//
//            if ($row = $table->fetchRow($select) ) {
//                $row = $row->toArray();
//                $data['modset_id'] =  $row['new_modset_id'];
//            }
//
//            $table->insert($data);
//
//            $db->query('UNLOCK TABLES');
            return $settings->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('modset_id = ?' => $settings->Id));
        }
    }

    /**
     * @param int|null $id
     * @return array
     * @throws Exception
     */
    public function readRow($id = null)
    {
        $table = $this->getDbTable();

        $column_map = array('id'           => 'modset_id',
                            'twentyfour'   => 'modset_twentyfour',
                            'quadrants'    => 'modset_quadrants',
                            'sound'        => 'modset_sound',
                            'mascot'       => 'modset_mascot',
                            'colour'       => 'modset_colour',
                            'colourSwitch' => 'modset_colour-switch',
                      );

        if ($id === null) {
            $select = $table->select()
                            ->from($table,
                                   $column_map
                            )
                            ->order('modset_id DESC')
                            ->order('modset_timestamp DESC')
                            ->limit(1)
            ;
        } else {
            $select = $table->select()
                            ->from($table,
                                   $column_map
                            )
                            ->where('modset_id = :id')->bind(array(':id' => $id))
                            ->order('modset_timestamp DESC')
                            ->limit(1)
            ;
        }



        if ($row = $table->fetchRow($select) ) {
            $row = $row->toArray();
            return $row;
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param int $id
     * @return Application_Model_ModuleSettings
     */
    public function read($id = null)
    {
        if ($row = $this->readRow($id)) {
            return new Application_Model_ModuleSettings($row);
        };
    }

    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        // @todo Implement
        ;
    }
}
