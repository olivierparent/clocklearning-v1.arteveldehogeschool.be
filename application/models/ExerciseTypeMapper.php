<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ExerciseTypeMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_ExerciseType';
    }

    /**
     * @param int $id
     * @return Application_Model_ExerciseType
     * @throws Exception
     */
    public function read($id = 1)
    {
        $table = $this->getDbTable();

        $select = $table->select()
            ->from($table, array(
                'id'       => 'exetyp_id',
                'name'     => 'exetyp_name',
                'digital'  => 'exetyp_digital',
                'moduleId' => 'mod_id',
            ))
            ->where('exetyp_id = :id')->bind(array(':id' => $id))
        ;
        if ($row = $table->fetchRow($select) ) {
            return new Application_Model_ExerciseType($row->toArray());
        }
        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param Application_Model_Module $module
     * @return array Application_Model_ExerciseType
     */
    public function readAllActiveWhereModule(Application_Model_Module $module)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->where('mod_id = :moduleId')->bind(array(':moduleId' => $module->Id))
                        ->where('exetyp_active = TRUE')
                        ->order('exetyp_id ASC')
        ;

        $rowset = $table->fetchAll($select);

        return $this->_toObjects($rowset);
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_ExerciseType
     */
    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        $exerciseType = new Application_Model_ExerciseType();

        if ($row) {
            $exerciseType->Id       = (int)  $row['exetyp_id'     ];
            $exerciseType->Name     =        $row['exetyp_name'   ];
            $exerciseType->Digital  = (bool) $row['exetyp_digital'];
            $exerciseType->ModuleId = (int)  $row['mod_id'        ];
        }

        return $exerciseType;
    }
}
