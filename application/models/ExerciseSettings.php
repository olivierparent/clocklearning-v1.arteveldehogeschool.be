<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ExerciseSettings extends Ahs_Model_Abstract
{
    const ASSISTANCE_MODELLING = 'MODELLING';
    const ASSISTANCE_VERBAL    = 'VERBAL';
    const ASSISTANCE_VISUAL    = 'VISUAL';

    const MINUTES_INCREMENT_00 =  '0';
    const MINUTES_INCREMENT_01 =  '1';
    const MINUTES_INCREMENT_05 =  '5';
    const MINUTES_INCREMENT_10 = '10';

    const MODE_EXERCISE = 'EXERCISE';
    const MODE_TEST     = 'TEST';

	/**
     * @var int
     */
    protected $_id;

    /**
     * @var int
     */
    protected $_amount;

    /**
     * @var bool
     */
    protected $_hour;

    /**
     * @var bool
     */
    protected $_quarterPast;

	/**
     * @var bool
     */
    protected $_halfHour;

    /**
     * @var bool
     */
    protected $_quarterTo;

    /**
     * @var string
     */
    protected $_minutes;

    /**
     * @var string
     */
    protected $_minutesIncrement;

	/**
     * @var boolean
     */
    protected $_seconds = false;

    /**
     * @var string Exercise mode. Accepted constants: MODE_EXERCISE, MODE_TEST.
     */
    protected $_mode;

    /**
     * @var string
     */
    protected $_assistance;

    /**
     * @var string
     */
    protected $_subtitles;

    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (empty($options)) {
            $options = array(
                'amount'           => 10,
                'hour'             => true,
                'quarterPast'      => true,
                'halfHour'         => true,
                'quarterTo'        => true,
                'minutesIncrement' => self::MINUTES_INCREMENT_00,
                'seconds'          => false,
                'mode'             => self::MODE_EXERCISE,
                'assistance'       => self::ASSISTANCE_MODELLING,
                'subtitles'        => true,
            );
        }
        parent::__construct($options);
    }

    public function setAmount($amount)
    {
        $this->_amount = (int) $amount;
    }

    /**
     * @param array $options
     */
    public function setAmountForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * @param $form
     */
    public function setLevelForm($form)
    {
        foreach ($form as $field => $value) {
            if ($field == 'minutes' || $field == 'minutesIncrement' ) {
                continue;
            }
            $field = ucfirst($field);
            $this->$field = (bool) $value;
        }
        $this->MinutesIncrement = (isset($form['minutes']) && $form['minutes']) ? $form['minutesIncrement'] : self::MINUTES_INCREMENT_00;
    }

    /**
     * @param array $options
     */
    public function setModeForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * @param array $options
     */
    public function setAssistanceForm(array $options = null)
    {
        $this->subform($options);
    }

    /**
     * @param array $options
     */
    protected function subform(array $options = null)
    {
        foreach ($options as $field => $value) {
            $field = ucfirst($field);
            $this->$field = $value;
        }
    }
}
