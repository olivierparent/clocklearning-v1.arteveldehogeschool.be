<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ExerciseAttempt extends Ahs_Model_Abstract
{
    /**
     * @var int
     */
    protected $_id = null;

    /**
     * @var string
     */
    protected $_answer = null;

    /**
     * @var string
     */
    protected $_mode = null;

    /**
     * @var bool
     */
    protected $_correct = false;

    /**
     * @var string
     */
    protected $_timeEnd = null;

    /**
     * @var int
     */
    protected $_exerciseId = null;

    /**
     * @param array $properties
     * @return array
     */
    public function toArray(array $properties = array('id', 'answer', 'mode', 'correct', 'timeEnd', 'exerciseId') )
    {
        return parent::toArray($properties);
    }

    /**
     * @param string|int $time
     */
    public function setTimeEnd($time)
    {
        if (!empty($time)) {
            $this->_timeEnd = is_int($time) ? date('Y-m-d H:i:s', (int) ($time / 1000)) : $time;
        }
    }

    /**
     * @param int $id
     */
    public function setExerciseId($id)
    {
        $this->_exerciseId = empty($id) ? null : (int) $id;
    }
}
