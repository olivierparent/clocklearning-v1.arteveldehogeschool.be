<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ModuleMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_Module';
    }

    /**
     * @param integer $id
     * @return Application_Model_User
     */
    public function read($id = 1)
    {
        $row = $this->readRow($id);

        return new Application_Model_Module($row);
    }

    /**
     * @return array Application_Model_Module
     */
    public function readAllActive()
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->where('mod_active = TRUE')
                        ->order('mod_id ASC')
        ;

        $rowset = $table->fetchAll($select);

        return $this->_toObjects($rowset);
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_Module
     */
    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        $module = new Application_Model_Module();

        if ($row) {
            $module->Id   = (int) $row['mod_id'  ];
            $module->Name =       $row['mod_name'];
        }

        return $module;
    }
}
