<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ExerciseMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_Exercise';
    }

    /**
     * @param Application_Model_Exercise $exercise
     * @return int
     */
    public function save(Application_Model_Exercise $exercise)
    {
        $table = $this->getDbTable();

        $data = array(
            'exe_id'        => $exercise->Id,
            'exe_questions' => json_encode($exercise->Questions),
            'exe_answers'   => json_encode($exercise->Answers),
            'exe_modes'     => json_encode($exercise->Modes),
            'exe_solution'  => json_encode($exercise->Solution),
            'exe_correct'   => $exercise->Correct,
            'exe_timestart' => $exercise->TimeStart,
            'exe_timeend'   => $exercise->TimeEnd,
            'exetyp_id'     => $exercise->ExerciseTypeId,
            'set_id'        => $exercise->SetId,
        );

        if ($exercise->Id === null) {
            return $exercise->Id = $table->insert($data);
        } else {
            $table->update($data, array('exe_id = ?' => $exercise->Id));
            return 0;
        }
    }

    /**
     * @param int $id
     * @return Application_Model_Exercise
     * @throws Exception
     */
    public function read($id = 1)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table, array(
                            'id'             => 'exe_id',
                            'timeStart'      => 'exe_timestart',
                            'timeEnd'        => 'exe_timeend',
                            'exerciseTypeId' => 'exetyp_id',
                            'setId'          => 'set_id',
                        ))
                        ->where('exe_id = :id')->bind(array(':id' => $id))
        ;
        if ($row = $table->fetchRow($select) ) {
            return new Application_Model_Exercise($row->toArray());
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param Application_Model_Set $set
     * @return array
     * @throws Exception
     */
    public function readWhereSet(Application_Model_Set $set)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table, array(
                            'id'             => 'exe_id',
                            'correct'        => 'exe_correct',
                            'timeStart'      => 'exe_timestart',
                            'timeEnd'        => 'exe_timeend',
                            'exerciseTypeId' => 'exetyp_id',
                            'setId'          => 'set_id',
                        ))
                        ->where('set_id = :setId')->bind(array(':setId' => $set->Id))
                        ->order('set_id ASC')
                        ->order('exe_id ASC')
        ;

        if ($rowset = $table->fetchAll($select) ) {
            return $this->_toObjects($rowset);
        }
        throw new Exception('Could not find record in table ' . $table->getInfo(Zend_Db_Table::NAME));
    }

    public function readFirstUncompletedWhereUser(Application_Model_User $user)
    {
        $table = $this->getDbTable();
        $tableSet  = new Application_Model_DbTable_Set();
        $tableUser = new Application_Model_DbTable_User();

        $select = $table->select()
                        ->setIntegrityCheck(false) // to allow join
                        ->from(       array('e' => $table    ->info(Zend_Db_Table::NAME)), array(
                                                                      'id'             => 'exe_id',
                                                                      'exerciseTypeId' => 'exetyp_id',
                                                                      'setId'          => 'set_id',
                        ))
                        ->joinNatural(array('s' => $tableSet ->info(Zend_Db_Table::NAME)), array())
                        ->joinNatural(array('u' => $tableUser->info(Zend_Db_Table::NAME)), array())
                        ->where('exe_timeend IS NULL')
                        ->where('u.usr_id = :userId')
                        ->bind(array(':userId' => $user->Id))
                        ->order('exe_id')
                        ->limit(1)
        ;

        if ($row = $table->fetchRow($select) ) {
            return new Application_Model_Exercise($row->toArray());
        }
        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param $id ExerciseId
     * @return bool success
     */
    public function delete($id)
    {
        $table = $this->getDbTable();

        return (bool) $table->delete(array('exe_id = ?' => $id));
    }


    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_Exercise|null
     */
    public function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        return empty($row) ? null : new Application_Model_Exercise($row->toArray());
    }
}
