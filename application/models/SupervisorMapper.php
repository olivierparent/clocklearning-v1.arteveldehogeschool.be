<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_SupervisorMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_Supervisor';
    }

    /**
     * @param Application_Model_Supervisor $supervisor
     */
    public function save(Application_Model_Supervisor $supervisor)
    {
        $table = $this->getDbTable();

        $data = array('sup_email'      => $supervisor->Email,
                      'sup_username'   => $supervisor->Username,
                      'sup_password'   => $supervisor->Password,
                      'sup_givenname'  => $supervisor->Givenname,
                      'sup_familyname' => $supervisor->Familyname,
                      'sup_role'       => $supervisor->Role,
                      'exeset_id'      => $supervisor->ExerciseSettingsId,
                      'lan_id'         => $supervisor->LanguageId,
                      'modset_id'      => $supervisor->ModuleSettingsId,

                );

        if ($supervisor->Id === null) {
            $supervisor->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('sup_id = ?' => $supervisor->Id));
        }
    }

    /**
     * @param int $id
     * @return Application_Model_Supervisor
     */
    public function read($id = 1)
    {
        $row = $this->readRow($id);

        return new Application_Model_Supervisor($row);
    }

    /**
     * @param int $id
     * @return array
     * @throws Exception
     */
    public function readRow($id = 1)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table,
                               array('id'                 => 'sup_id',
                                     'email'              => 'sup_email',
                                     'username'           => 'sup_username',
                                     'password'           => 'sup_password',
                                     'givenname'          => 'sup_givenname',
                                     'familyname'         => 'sup_familyname',
                                     'role'               => 'sup_role',
                                     'exerciseSettingsId' => 'exeset_id',
                                     'languageId'         => 'lan_id',
                                     'moduleSettingsId'   => 'modset_id',
                               )
                        )
                        ->where('sup_id = :id')
                        ->where('sup_active = TRUE')
                        ->where('sup_deleted = FALSE')
                        ->order('sup_timestamp DESC')
                        ->bind(array(':id' => $id))
        ;
        if ($row = $table->fetchRow($select) ) {
            $row = $row->toArray();

//            Zend_Debug::dump($row); exit;
            return $row;
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     */
    public function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        ;
    }
}
