<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_MessageRecipientMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_MessageRecipient';
    }

    /**
     * @return array
     */
    public function readAll()
    {
        $rowset = $this->getDbTable()->fetchAll();

        return $this->_toObjects($rowset);
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_MessageRecipient
     */
    protected function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        $messageRecipient = new Application_Model_MessageRecipient();

        if ($row) {
            $messageRecipient->Id    = (int) $row['msgrcp_id'   ];
            $messageRecipient->Email =       $row['msgrcp_email'];
            $messageRecipient->Name  =       $row['msgrcp_name' ];
        }

        return $messageRecipient;
    }
}
