<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_ExerciseAttemptMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_ExerciseAttempt';
    }

    /**
     * @param Application_Model_ExerciseAttempt $exerciseAttempt
     * @return int
     */
    public function save(Application_Model_ExerciseAttempt $exerciseAttempt)
    {
        $table = $this->getDbTable();

        $data = array(
            'exeatt_id'      => $exerciseAttempt->Id,
            'exeatt_answer'  => $exerciseAttempt->Answer,
            'exeatt_mode'    => $exerciseAttempt->Mode,
            'exeatt_correct' => $exerciseAttempt->Correct,
            'exeatt_timeend' => $exerciseAttempt->TimeEnd,
            'exe_id'         => $exerciseAttempt->ExerciseId,
        );

        if ($exerciseAttempt->Id === null) {
            return $exerciseAttempt->Id = $table->insert($data);
        } else {
            $table->update($data, array('exeatt_id = ?' => $exerciseAttempt->Id));
        }
    }

    /**
     * @param Application_Model_Exercise $exercise
     * @return array
     * @throws Exception
     */
    public function readAllWhereExercise(Application_Model_Exercise $exercise)
    {
        $table = $this->getDbTable();

        $select = $table->select()
            ->from($table, array(
                'id'         => 'exeatt_id',
                'answer'     => 'exeatt_answer',
                'mode'       => 'exeatt_mode',
                'correct'    => 'exeatt_correct',
                'timeEnd'    => 'exeatt_timeend',
                'exerciseId' => 'exe_id',
            ))
            ->where('exe_id = :exeId')
            ->bind(array(':exeId' => $exercise->Id))
            ->order('id ASC')
        ;

        if ($rowset = $table->fetchAll($select) ) {
            return $this->_toObjects($rowset);
        }

        throw new Exception('Could not find record in table ' . $table->info(Zend_Db_Table::NAME));
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_ExerciseAttempt|null
     */
    public function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        return empty($row) ? null : new Application_Model_ExerciseAttempt($row->toArray());
    }
}
