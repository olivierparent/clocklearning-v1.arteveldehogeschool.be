<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Model_SetMapper extends Ahs_Model_Mapper_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        parent::__construct($options);
        $this->DbTableClassName = 'Application_Model_DbTable_Set';
    }

    /**
     * @param Application_Model_Set $set
     * @return int
     */
    public function save(Application_Model_Set $set)
    {
        $table = $this->getDbTable();

        $data = array('set_id' => $set->Id,
                      'usr_id' => $set->userId,
                );

        if ($set->Id === null) {
            return $set->Id = (int) $table->insert($data);
        } else {
            $table->update($data, array('set_id = ?' => $set->Id));
        }
    }

    public function readWhereUser(Application_Model_User $user)
    {
        $table = $this->getDbTable();

        $select = $table->select()
                        ->from($table, array(
                            'id'     => 'set_id',
                            'userId' => 'usr_id',
                        ))
                        ->where('usr_id = :userId')->bind(array(':userId' => $user->Id))
                        ->order('set_id ASC')
        ;

        $rowset = $table->fetchAll($select);

        return $this->_toObjects($rowset);
    }

    /**
     * @param $id SetId
     * @return bool success
     */
    public function delete($id)
    {
        $table = $this->getDbTable();

        return (bool) $table->delete(array('set_id = ?' => $id));
    }


    /**
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Application_Model_Set|null
     */
    public function _toObject(Zend_Db_Table_Row_Abstract $row = null)
    {
        return empty($row) ? null : new Application_Model_Set($row->toArray());
    }
}
