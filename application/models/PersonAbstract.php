<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Application_Model_PersonAbstract extends Ahs_Model_Abstract
{
    const DEFAULT_ACTIVE  = true;
    const DEFAULT_DELETED = false;

	/**
     * @var integer
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_username;

    /**
     * @var string
     */
    protected $_password;

	/**
     * @var string
     */
    protected $_givenname;

    /**
     * @var string
     */
    protected $_familyname;

    /**
     * @var string.
     */
    protected $_role;

	/**
     * @var boolean
     */
    protected $_active = self::DEFAULT_ACTIVE;

    /**
     * @var boolean
     */
    protected $_deleted = self::DEFAULT_DELETED;

    /**
     * @var integer
     */
    protected $_exerciseSettingsId;

    /**
     * @var integer
     */
    protected $_moduleSettingsId;

    /**
     * @param array $options
     */
    public function __construct(array $options = null) {
        parent::__construct($options);
    }

    /**
     * @param string $password
     */
    public function setPasswordraw($password)
    {
        $this->Password = Ahs_Utility::hash($password);
    }

    /**
     * @param string $password
     */
    public function setPasswordrepeat($password)
    {
        // Do nothing.
    }
}
