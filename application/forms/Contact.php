<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Form_Contact extends Zend_Form
{
    public function init()
    {
        $this
//            ->setOptions(array(
//                'class' => 'ui-body ui-body-a ui-corner-all',
//            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getForm())
        ;

        $text_from = new Zend_Form_Element_Text('from');
        $text_from
            ->setOptions(array(
                'placeholder' => $this->getTranslator()->_('Email address')
            ))
            ->setRequired()
            ->setFilters(array(
                'StringTrim',
            ))
            ->setValidators(array(
                array('NotEmpty'    , true),
                array('EmailAddress', true),
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getElement())
        ;
        $text_subject = new Zend_Form_Element_Text('subject');
        $text_subject
            ->setOptions(array(
                'placeholder' => $this->getTranslator()->_('Subject')
            ))
            ->setRequired()
            ->setFilters(array(
                'StringTrim',
            ))
            ->setValidators(array(
                array('NotEmpty', true),
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getElement())
        ;

        $textarea_body = new Zend_Form_Element_Textarea('body');
        $textarea_body
            ->setOptions(array(
                'placeholder' => $this->getTranslator()->_('Message')
            ))
//            ->setAttrib('rows', '4')
            ->setRequired()
            ->setFilters(array(
                'StringTrim',
            ))
            ->setValidators(array(
                array('NotEmpty', true),
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getElement())
        ;

        $captcha = new Zend_Form_Element_Captcha('captcha', array(
            'captcha'        => 'Figlet',
            'captchaOptions' => array(
                'wordLen' =>   4,
                'timeout' => 300,
            ),
        ));
        $captcha
            ->setLabel('Please verify you are not a robot by entering the characters below:')
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getCaptcha())
        ;

        $submit = new Zend_Form_Element_Submit('send');
        $submit
            ->setLabel('Send')
            ->setOptions(array(
                'data-icon'   => 'mail',
                'data-inline' => 'true',
                'data-theme'  => 'b',
            ))
            ->setDecorators(array(
               'ViewHelper',
               array(
                   'HtmlTag',
                   array(
                       'tag' => 'div',
                   ),
               ),
            ))
        ;

//        $view = Zend_Layout::getMvcInstance()->getView();

        $this
            ->setMethod('post')
            ->setAction('')
            ->addElement($text_from)
            ->addElement($text_subject)
            ->addElement($textarea_body)
            ->addElement($captcha)
            ->addElement($submit)
        ;
    }
}
