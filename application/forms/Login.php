<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Application_Form_Login extends Zend_Form
{
    public function init()
    {
        $this
            ->setOptions(array(
//                'class' => 'ui-body ui-body-a ui-corner-all',
                'data-ajax' => 'false',
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getForm())
        ;

        $text_username = new Zend_Form_Element_Text('username');
        $text_username
            ->setOptions(array(
                'placeholder' => $this->getTranslator()->_('User name'),
            ))
            ->setRequired()
            ->setFilters(array(
                'StringTrim',
            ))
            ->setValidators(array(
                array('NotEmpty', true),
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getElement())
        ;

        $password_raw = new Zend_Form_Element_Password('passwordraw');
        $password_raw
            ->setOptions(array(
                'placeholder' => $this->getTranslator()->_('Password'),
            ))
            ->setRequired()
            ->setFilters(array(
                'StringTrim',
            ))
            ->setValidators(array(
                array('NotEmpty', true),
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getElement())
        ;

        $submit = new Zend_Form_Element_Submit('submit');
        $submit
            ->setLabel('Log in')
            ->setOptions(array(
                'data-icon'   => 'user',
                'data-inline' => 'true',
                'data-theme'  => 'b',
            ))
            ->setDecorators(Ahs_Form_Decorators_Frontoffice::getButton())
        ;

        $this
            ->setMethod('post')
            ->setAction('')
            ->addElement($text_username)
            ->addElement($password_raw)
            ->addElement($submit)
        ;
    }
}
