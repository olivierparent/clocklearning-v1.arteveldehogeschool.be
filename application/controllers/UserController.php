<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class UserController extends Zend_Controller_Action
{
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();
    }

    public function indexAction()
    {
        $this->redirect('user/login');
    }

    public function loginAction()
    {
        $form = new Application_Form_Login();

        $view = $this->view;
        $view->title = 'Login';

        $request = $this->getRequest();

        /**
         * If form is post
         */
        if ($request->isPost() ) {

            /**
             *  Validate form
             */
            if ($form->isValid($request->getPost() ) ) {
                $values = $form->getValues();

                $user = new Application_Model_User($values);

                $adapter = new Ahs_Auth_Adapter_User($user->Username,
                                                     $user->Password);

                $this->_auth->authenticate($adapter);

                if ($this->_auth->hasIdentity() ) {
                    // Authenticated
                    $id = $adapter->getResultRowObject()->usr_id;
                    $identity = new Ahs_Auth_Identity();
                    $identity->setUserId($id);

                    $this->_auth->getStorage()->write($identity);

                    return $this->redirect('/');
                } else {
                    // Unable to authenticate
                    $view->form = Ahs_Utility::getFrontofficeMessage('Login failed', 'Cannot log in with the provided credentials', Ahs_Utility::MESSAGE_ERROR);
                }
            } else {
                $view->form = Ahs_Utility::getFrontofficeMessage('Errors', 'Please correct the errors below', Ahs_Utility::MESSAGE_ERROR);
            }
        }
        $view->form .= $form;
    }

    public function logoutAction()
    {
        $this->_helper->layout()->disableLayout();       // Disable layout
        $this->_helper->viewRenderer->setNoRender(true); // Disable view renderer

        $this->_auth->clearIdentity();

        Ahs_Exercise_Settings::destroySession();

        $this->redirect(Ahs_History::getPreviousRequestUri(), array('prependBase' => false));
    }

    /**
     *
     */
    public function languageAction()
    {
        $session = new Ahs_Session(Ahs_Language::SESSION_NAMESPACE);

        $lang = strtolower($this->getRequest()->getParam('to', Ahs_Language::LANG_EN));

        switch ($lang) {
            case Ahs_Language::LANG_NL:
                $session->lang = Ahs_Language::LANG_NL;
                break;
            case Ahs_Language::LANG_EN:
            default:
                $session->lang = Ahs_Language::LANG_EN;
                break;
        }

        $this->redirect(Ahs_History::getPreviousRequestUri(), array('prependBase' => false));
    }
}
