<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class ContactController extends Zend_Controller_Action
{
    const MESSAGE_STATUS_SENT   = 'SENT';
    const MESSAGE_STATUS_FAILED = 'FAILED';

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity() ) {
            $this->view->assign('role', $auth->getIdentity()->Role);
        }
    }

    public function indexAction()
    {
        $form = new Application_Form_Contact();

        $view = $this->view;
        $view->assign('title', 'Contact');

        $request = $this->getRequest();

        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             *  Validate form
             */
            if ( $form->isValid( $request->getPost() ) ) {
                $values = $form->getValues();

                $message = new Application_Model_Message($values);
                $messageMapper = new Application_Model_MessageMapper();
                $messageMapper->save($message);

                $form = Ahs_Utility::getFrontofficeMessage('Info', 'Your message has been sent', Ahs_Utility::MESSAGE_SUCCESS);

                try {
                    $messageRecipientMapper = new Application_Model_MessageRecipientMapper();
                    $messageRecipients = $messageRecipientMapper->readAll();

                    $email = new Zend_Mail();
                    foreach ($messageRecipients as $messageRecipient) {
                        $email
                            ->addBcc($messageRecipient->email, $messageRecipient->name)
                        ;
                    }
                    $email
                        ->addTo(      $message->From   )
                        ->setFrom(    $message->From   )
                        ->setReplyTo( $message->From   )
                        ->setSubject( $message->Subject)
                        ->setBodyHtml($message->Body   )
                        ->setBodyText($message->Body   )
                        ->send()
                    ;
                    $message->Status = self::MESSAGE_STATUS_SENT;
                    $messageMapper->save($message);
                } catch (Zend_Mail_Transport_Exception $e) {
                    $message->Status = self::MESSAGE_STATUS_FAILED;
                    $messageMapper->save($message);
                }
            } else {
                $form = Ahs_Utility::getFrontofficeMessage('Errors', 'Please correct the errors below', Ahs_Utility::MESSAGE_ERROR)
                      . $form;
            }
        }
        $view->assign('form', $form);
    }
}
