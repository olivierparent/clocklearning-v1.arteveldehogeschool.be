<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class ExerciseController extends Zend_Controller_Action
{
    protected $_person = null;

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        $type = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        $view = $this->_getView();
        $view->assign('type', $type);
        if ($auth->hasIdentity() ) {
            $identity = $auth->getIdentity();
            if ($identity->isUser() ) {
                $userMapper = new Application_Model_UserMapper();
                $this->_person = $userMapper->read($identity->UserId);
            } elseif ($identity->isSupervisor() ) {
                $supervisorMapper = new Application_Model_SupervisorMapper();
                $this->_person = $supervisorMapper->read($identity->SupervisorId);
            }
            $role = $auth->getIdentity()->Role;
            $urlNext = $view->baseUrl('exercise/' . ($role === Ahs_Acl::ROLE_USER ? 'user/set/next' : $type));

            $view->assign('role'   , $role   )
                 ->assign('urlNext', $urlNext)
            ;
        } else {
            $view->assign('urlNext', $type);
        }

        $moduleSettings   = Ahs_Exercise_Settings::getSessionModuleSettings($this->_person);
        $exerciseSettings = Ahs_Exercise_Settings::getSessionExerciseSettings($this->_person);

        $assistance = array();
        switch ($exerciseSettings->Assistance) {
            case Application_Model_ExerciseSettings::ASSISTANCE_MODELLING;
                $assistance[] = 'modelling';
            case Application_Model_ExerciseSettings::ASSISTANCE_VISUAL;
                $assistance[] = 'visual';
            case Application_Model_ExerciseSettings::ASSISTANCE_VERBAL;
            default:
                $assistance[] = 'verbal';
                break;
        }


        if (!in_array($type, array('index', 'user'))) {

            $vars = array(
                'colour'     => ($moduleSettings->Colour       ? ' colour'        : ''             )
                              . ($moduleSettings->ColourSwitch ? ' colour-switch' : ''             ),
                'twentyfour' => ($moduleSettings->Twentyfour   ? ' twentyfour'    : ''             ),
                'sound'      => ($moduleSettings->Sound        ? ''               : ' muted'       ),
                'mascot'     =>  $moduleSettings->Mascot,
                'assistance' =>  implode(' ', $assistance),
                'subtitles'  => ($exerciseSettings->Subtitles  ? ' subtitles'     : ' no-subtitles'),
            );

            $view->assign($vars);
            $view->inlineScript()
                 ->appendFile($view->placeholder('baseScripts') . 'User/Exercise/main.js')
            ;
            Ahs_Exercise_Audio_NL::setBaseUrl($view->placeholder('baseAudio'));
            Ahs_Exercise_Video::   setBaseUrl($view->placeholder('baseVideo'));
        }
    }

    public function indexAction()
    {
        // Not logged in user and supervisor.
    }

    /**
     * Recognizing: analogue clock → 4 notations
     */
    public function type01Action()
    {
    }

    /**
     * Recognizing: digital clock → 4 notations
     */
    public function type02Action()
    {
    }

    /**
     * Recognizing: notation → 4 analogue clocks
     */
    public function type03Action()
    {
    }

    /**
     * Recognizing: notation → 4 digital clocks
     */
    public function type04Action()
    {
    }

    /**
     * Combining: 4 notations → 4 analogue clocks
     */
    public function type05Action()
    {
    }

    /**
     * Combining: 4 notations → 4 digital clocks
     */
    public function type06Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type07Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type08Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type09Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type10Action()
    {
    }

    /**
     * Clock setting: notation → analogue clock
     */
    public function type11Action()
    {
    }

    /**
     * Clock setting: notation → digital clock
     */
    public function type12Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type13Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type14Action()
    {
    }

    /**
     * Recognizing: analogue clock → 4 digital clocks
     */
    public function type15Action()
    {
    }

    /**
     * Recognizing: digital clock → 4 analogue clocks
     */
    public function type16Action()
    {
    }

    /**
     * Combining: 4 analogue clocks → 4 digital clocks
     */
    public function type17Action()
    {
    }

    /**
     * Clock setting: analogue clock → set digital clock
     */
    public function type18Action()
    {
    }

    /**
     * Clock setting: digital clock → set analogue clock
     */
    public function type19Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type20Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type21Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type22Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type23Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type24Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type25Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type26Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type27Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type28Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type29Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type30Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type31Action()
    {
    }

    /**
     * @todo Implement
     */
    public function type32Action()
    {
    }

    /**
     *
     */
    public function userAction()
    {
        if ($this->view->role === Ahs_Acl::ROLE_USER) {
            $user = $this->_person;
            $exerciseMapper = new Application_Model_ExerciseMapper();
            switch (strtolower($this->getRequest()->getParam('set') ) ) {
                case 'start':
                    try {
                        $exercise = $exerciseMapper->readFirstUncompletedWhereUser($user);
                        $this->view->assign('exercise', 'exercise/type' . sprintf('%1$02d', $exercise->ExerciseTypeId));
                    } catch (Exception $e) {
                        $this->redirect('exercise/user/set/end');
                    }
                    break;
                case 'next':
                    try {
                        $exercise = $exerciseMapper->readFirstUncompletedWhereUser($user);
                        $this->redirect('exercise/type' . sprintf('%1$02d', $exercise->ExerciseTypeId));
                    } catch (Exception $e) {
                        $this->redirect('exercise/user/set/end');
                    }
                    break;
                case 'end':
                default:
                    break;
            }
        } else {
            $this->redirect('exercise');
        }
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $view = $this->view;

        return $view;
    }
}
