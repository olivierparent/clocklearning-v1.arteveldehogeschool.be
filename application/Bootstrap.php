<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Initialize Resources
     */
    protected function _initResources()
    {
        $this->bootstrap('layout'); // Make a _initLayout()
    }

    /**
     * Initialize Locale
     */
    protected function _initLocale()
    {
        try {
            $session = new Ahs_Session('Language');
            if ( isset ($session->lang) ) {
                $locale = new Zend_Locale($session->lang);
            } else {
                $locale = new Zend_Locale();
                switch ($locale->getLanguage()) {
                    case 'nl':
                    case 'nl_BE':
                    case 'nl_NL':
                        $locale = new Zend_Locale(Ahs_Language::LANG_NL);
                        break;
                    default:
                        $locale = new Zend_Locale(Ahs_Language::LANG_EN);
                        break;
                }
                $session->lang = $locale;
            }
        } catch (Zend_Locale_Exception $e) {
            $locale = new Zend_Locale(Ahs_Language::LANG_EN);
        }

        Zend_Registry::set('Zend_Locale', $locale);
    }

    /**
     * Initialize Translations
     */
    protected function _initTranslate()
    {
        $resources = $this->getOption('resources');
//        Zend_Debug::dump(realpath($resources['translate']['content'])); exit;
        try {
            $translate = new Zend_Translate(
                array(
                     'adapter' => 'xliff',
                     'content' => realpath($resources['translate']['content']),
                     'locale'  => Zend_Registry::get('Zend_Locale')->getLanguage(), // 'auto'
                     'scan'    => Zend_Translate::LOCALE_DIRECTORY,
                )
            );
            Zend_Registry::set('Zend_Translate', $translate);
        } catch (Zend_Translate_Exception $e) {
            // Do nothing.
        }
    }

    /**
     * Initialize language of form validation
     */
    protected function _initValidate()
    {
        $resources = $this->getOption('resources');
//        Zend_Debug::dump(realpath($resources['translate']['validation'])); exit;
        try {
            $translate = new Zend_Translate(
                array(
                    'adapter' => 'array',
                    'content' => realpath($resources['translate']['validation']), // Full version of Zend Framework 1!
                    'locale'  => Zend_Registry::get('Zend_Locale')->getLanguage(), // 'auto'
                    'scan'    => Zend_Translate::LOCALE_DIRECTORY,
                )
            );
            Zend_Validate::setDefaultTranslator($translate);
        } catch (Zend_Translate_Exception $e) {
            // Do nothing.
        }
    }

    /**
     * Initialize ViewHelpers
     */
    protected function _initViewHelpers()
    {
        /**
         * For Module specific settings: see library/Ahs/Controller/Plugin/ folder for additional Bootstraps!
         */

        // To make $view->baseUrl() available in this bootstrap
        $front = $this->getResource('frontController');
        $front->setRequest(new Zend_Controller_Request_Http());

        $view = $this->_getView();
        $view->addHelperPath('Ahs/View/Helper', 'Ahs_View_Helper'); // Support for HTML5 form elements
        $view->placeholder('baseImages'   )->set($view->baseUrl('/assets/images/') );
        $view->placeholder('baseStyles'   )->set($view->baseUrl('/assets/css/'   ) );
        $view->placeholder('baseStylesLib')->set($view->baseUrl('/libraries/css/') );
        $view->placeholder('baseScripts'  )->set($view->baseUrl('/assets/js/'    ) );
        $view->placeholder('baseVendor'   )->set($view->baseUrl('/components/' ) );
        $view->placeholder('baseVideo'    )->set($view->baseUrl('/assets/video/' ) );
        $view->placeholder('baseAudio'    )->set($view->baseUrl('/assets/audio/' ) );

        $translate = Zend_Registry::get('Zend_Translate');
        $lang = Zend_Registry::get('Zend_Locale')->getLanguage();

        $view->title = $translate->_('Clock learning with Rika-Tika');
        $view->doctype('HTML5'); // http://framework.zend.com/manual/en/zend.view.helpers.html#zend.view.helpers.initial.doctype
        $view
            ->headMeta()
            ->setCharset('utf-8')
            ->appendName('author'     , 'Olivier Parent, Veerle Van Vooren, Tom Neuttiens')
//            ->appendName('copyright'  , 'Copyright (c) 2011-' . date('Y') . ' ' . $translate->_('Artevelde University College Ghent'), array('lang' => $lang))
            ->appendName('google'     , 'notranslate')
            ->appendName('description', $translate->_('Open source web-based educational computer assistant software application for clock reading'), array('lang' => $lang))
            ->appendName('keywords'   , 'klokleren, kloklezen, klokzetten, Arteveldehogeschool', array('lang' => $lang))
            ->appendName('viewport'   , 'width=device-width, initial-scale=1')
//            ->appendName('dcterms.rightsHolder', 'Olivier Parent')
//            ->appendName('dcterms.rights', 'Copyright (c) 2011-' . date('Y') . ' ' . $translate->_('Artevelde University College Ghent'), array('lang' => $lang))
//            ->appendName('dcterms.dateCopyrighted', date('Y'))
        ;

        $view->headTitle($view->title, 'PREPEND') // Zend_View_Helper_HeadTitle
             ->setDefaultAttachOrder('PREPEND')
             ->setSeparator(' ← ')
        ;

        // Head Links
        $view
            ->headLink() // Zend_View_Helper_HeadLink
            ->headLink(array(
                'rel'   => 'shortcut icon',
                'type'  => 'image/x-icon',
                'href'  => $view->placeholder('baseImages') . 'favicon.ico',
            ), 'PREPEND')
            ->headLink(array(
                'rel'   => 'apple-touch-icon',
                'sizes' => '57x57',
                'href'  => $view->placeholder('baseImages') . 'touch-icon-iphone.png',
            ), 'APPEND')
            ->headLink(array(
                'rel'   => 'apple-touch-icon',
                'sizes' => '72x72',
                'href'  => $view->placeholder('baseImages') . 'touch-icon-ipad.png',
            ), 'APPEND')
            ->headLink(array(
                'rel'   => 'apple-touch-icon',
                'sizes' => '114x114',
                'href'  => $view->placeholder('baseImages') . 'touch-icon-iphone-retina.png',
            ), 'APPEND')
            ->headLink(array(
                'rel'   => 'apple-touch-icon',
                'sizes' => '144x144',
                'href'  => $view->placeholder('baseImages') . 'touch-icon-ipad-retina.png',
            ), 'APPEND')
            ->headLink(array(
                'rel'   => 'author',
                'href'  => $view->baseUrl('humans.txt'),
            ))
            ->appendStylesheet('//cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.2/jquery.mobile.min.css')
            ->headLink(array(
                'media' => 'screen',
                'rel'   => 'stylesheet/less',
                'type'  => 'text/css',
                'href'  => $view->placeholder('baseStyles') . 'User/main.less',
            ))
//            ->headLink(array(
//                'rel'  => 'schema.dcterms',
//                'href' => 'http://purl.org/dc/terms/',
//            ))
        ;

        // Head Scripts
        $view
            ->headScript()
            ->appendFile('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js')
            ->appendFile('//cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.2/jquery.mobile.js')
            ->appendFile('//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js')
        ;

        // Inline Scripts
        $view
            ->inlineScript()
            ->appendFile('//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.11/require.min.js')
            ->appendFile($view->placeholder('baseScripts') . 'main.js')
        ;
    }

    protected function _initViewHelperNavigation()
    {
        $yaml = APPLICATION_PATH . '/configs/navigation/default.yml';
        $pages = new Zend_Config_Yaml($yaml);
        $navigation = new Zend_Navigation($pages);
        $auth = Zend_Auth::getInstance();
        $role = $auth->hasIdentity() ? $auth->getIdentity()->Role : Ahs_Acl::ROLE_GUEST;

        $view = $this->_getView();
        $view->navigation($navigation)
             ->setAcl(new Ahs_Acl())
             ->setRole($role)
        ;
    }

    // See http://framework.zend.com/manual/1.12/en/zend.controller.router.html#zend.controller.router.routes.rest
    protected function _initRestRoutes()
    {
        $front     = Zend_Controller_Front::getInstance();
        $restRoute = new Zend_Rest_Route($front, array(), array(
            // module
            'service' => array(
               // controllers
               'attempt',
               'exercise',
               'group',
               'module',
               'set',
               'user',
            ),
        ));
        $front->getRouter()->addRoute('rest', $restRoute);
    }

    /**
     * ZFDebug DEBUG TOOLBAR https://github.com/jokkedk/ZFDebug
     *
     * Disable temporarily with ?ZFDEBUG_DISABLE
     */
    protected function _initZFDebug()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZFDebug');

        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');

        // Only enable zfdebug if options have been specified for it
        if ($this->hasOption('zfdebug'))
        {
            $zfdebug = new ZFDebug_Controller_Plugin_Debug($this->getOption('zfdebug'));

            $this->bootstrap('db');
            $db = array($this->getPluginResource('db')->getDbAdapter());
            $zfdebug->registerPlugin(new ZFDebug_Controller_Plugin_Debug_Plugin_Database($db));
            $front->registerPlugin($zfdebug);
        }
    }

    protected function _initAhsDebug()
    {
        Ahs_Debug::$enabled = $this->getOption('ahsdebug');
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        return $view;
    }
}
