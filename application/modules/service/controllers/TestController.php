<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Service_TestController extends Zend_Controller_Action
{
    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Analogue_Answers;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Analogue_Answers_Test;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Digital_Answers;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Digital_Answers_Test;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Feedback_Analogue;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Feedback_Digital;

    /**
     * @var string
     */
    protected $_Ahs_Time;

    /**
     * @var Zend_Translate
     */
    protected $_translate;

    public function init()
    {
        $this->getHelper('layout')->disableLayout();         // Disable layout
        $this->getHelper('viewRenderer')->setNoRender(true); // Disable view renderer

        $this->_translate = Zend_Registry::get('Zend_Translate');

        $lang = strtoupper(Zend_Registry::get('Zend_Locale')->getLanguage());
        $this->_Ahs_Exercise_Data_Analogue_Answers      = "Ahs_Exercise_Data_Analogue_Answers_{$lang}";
        $this->_Ahs_Exercise_Data_Analogue_Answers_Test = "Ahs_Exercise_Data_Analogue_Answers_Test_{$lang}";
        $this->_Ahs_Exercise_Data_Digital_Answers       = "Ahs_Exercise_Data_Digital_Answers_{$lang}";
        $this->_Ahs_Exercise_Data_Digital_Answers_Test  = "Ahs_Exercise_Data_Digital_Answers_Test_{$lang}";
        $this->_Ahs_Exercise_Feedback_Analogue          = "Ahs_Exercise_Feedback_Analogue_{$lang}";
        $this->_Ahs_Exercise_Feedback_Digital           = "Ahs_Exercise_Feedback_Digital_{$lang}";
        $this->_Ahs_Time                                = "Ahs_Time_{$lang}";

        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        try {
            $moduleSettingsMapper = new Application_Model_ModuleSettingsMapper();
            $moduleSettings = $moduleSettingsMapper->read();

//            Zend_Debug::dump($moduleSettings); exit;

            if ($moduleSettings->Twentyfour) {
                $Ahs_Time::checkTwentyFour();
            } else {
                $Ahs_Time::uncheckTwentyFour();
            }

            /**
             * Official clock system or alternative
             */
            if ($moduleSettings->Quadrants === 'OFFICIAL') {
                // Official clock system
                $Ahs_Time::checkQuadrants();
            } else {
                // Alternative clock sytem
                $Ahs_Time::uncheckQuadrants();
            }

            $exerciseSettingsMapper = new Application_Model_ExerciseSettingsMapper();
            $exerciseSettings = $exerciseSettingsMapper->read();
//            Zend_Debug::dump($exerciseSettings); exit;

            if ($exerciseSettings->Hour) {
                $Ahs_Time::checkHour();
            } else {
                $Ahs_Time::uncheckHour();
            }
            if ($exerciseSettings->HalfHour) {
                $Ahs_Time::checkHalfHour();
            } else {
                $Ahs_Time::uncheckHalfHour();
            }
            if ($exerciseSettings->QuarterPast) {
                $Ahs_Time::checkQuarterPast();
            } else {
                $Ahs_Time::uncheckQuarterPast();
            }
            if ($exerciseSettings->QuarterTo) {
                $Ahs_Time::checkQuarterTo();
            } else {
                $Ahs_Time::uncheckQuarterTo();
            }
        } catch (Exception $e) {
            Zend_Debug::dump($e); exit;
        }
    }

    public function indexAction()
    {
        echo '<pre>';
        Zend_Debug::dump( json_decode( new $this->_Ahs_Exercise_Data_Analogue_Answers() ) );
    }

    /**
     * Test page Answers (analogue)
     *
     * @test
     */
    public function answersanalogueAction()
    {
        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        $h_max = $Ahs_Time::getTwentyFour() ? 24 : 12;
        $clock_system = $Ahs_Time::getQuadrants() ? 'Official' : 'Alternative';
        echo '<pre>', PHP_EOL,
             "<h2>{$h_max}-hour clock (", $this->_translate->_("{$clock_system} clock system"), ")</h2>", PHP_EOL;

//        $minutes = array();
//        $minutes[] = 0;
//        $minutes[] = 5;
//        $minutes[] = 10;
//        $minutes[] = 15;
//        $minutes[] = 20;
//        $minutes[] = 25;
//        $minutes[] = 30;
//        $minutes[] = 35;
//        $minutes[] = 40;
//        $minutes[] = 45;
//        $minutes[] = 50;
//        $minutes[] = 55;

//        foreach ($minutes as $m) {
        for ($m = 0; $m < 60; $m++) {
            echo '<h3>' , $m , ' ' , $this->_translate->_($m === 1 ? 'minute' : 'minutes'), '</h3>', PHP_EOL,
                 '<table>';
            for ($h = 0; $h < $h_max; $h++) {
                echo new $this->_Ahs_Exercise_Data_Analogue_Answers_Test($h, $m);
            }
            echo '</table><br><br>';
        }
    }

    /**
     * Test page Answers (digital)
     *
     * @test
     */
    public function answersdigitalAction()
    {
        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        $h_max = $Ahs_Time::getTwentyFour() ? 24 : 12;
        $clock_system = $Ahs_Time::getQuadrants() ? 'official' : 'alternative';
        echo '<pre>', PHP_EOL,
             "<h2>{$h_max}-hour clock ({$clock_system} clock system)</h2>", PHP_EOL;

        for ($m = 0; $m < 60; $m++) {
            echo "<h3>{$m} minutes</h3>", PHP_EOL,
                 '<table>';
            for ($h = 0; $h < $h_max; $h++) {
                echo new $this->_Ahs_Exercise_Data_Digital_Answers_Test($h, $m);
            }
            echo '</table><br><br>';
        }
    }

    /**
     * Test page Clocks (analogue)
     *
     * @test
     */
    public function clocksanalogueAction()
    {
        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        $this->getHelper('layout')->enableLayout();           // Enable layout
        $this->getHelper('viewRenderer')->setNoRender(false); // Enable view renderer

        $view = $this->view;

        $auth = Zend_Auth::getInstance();
        $person = null;
        if ($auth->hasIdentity() ) {
            $identity = $auth->getIdentity();
            if ($identity->isUser() ) {
                $userMapper = new Application_Model_UserMapper();
                $person = $userMapper->read($identity->UserId);
            } elseif ($identity->isSupervisor() ) {
                $supervisorMapper = new Application_Model_SupervisorMapper();
                $person = $supervisorMapper->read($identity->SupervisorId);
            }
        }

        $moduleSettings = Ahs_Exercise_Settings::getSessionModuleSettings($person);

        $vars = array(
            'colour'     => ($moduleSettings->Colour       ? ' colour'        : ''      )
                          . ($moduleSettings->ColourSwitch ? ' colour-switch' : ''      ),
            'twentyfour' => ($moduleSettings->Twentyfour   ? ' twentyfour'    : ''      ),
            'sound'      => ($moduleSettings->Sound        ? ''               : ' muted'),
            'official'   => $Ahs_Time::getQuadrants(),
        );
        $view->assign($vars);

        foreach($this->_request->getParams() as $key => $value) {
            switch ($key) {
                case 'module':
                case 'controller':
                case 'action':
                    break;
                case 'h':
                    $h = (int) $value;
                    break;
                case 'm':
                    $m = (int) $value;
                    break;
                default:
                    break 2; // break out of foreach as well
            }
        }

        $answers = new $this->_Ahs_Exercise_Data_Analogue_Answers($h, $m);
        $view->answers = array(
            array_merge(explode(':', (string) $answers->Time        ), array($answers->Time        ->toText(Ahs_Time_Abstract::TEXT_ANALOGUE), $this->_translate->_($answers->Time        ->Mode))),
            array_merge(explode(':', (string) $answers->TimeWrong[0]), array($answers->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE), $this->_translate->_($answers->TimeWrong[0]->Mode))),
            array_merge(explode(':', (string) $answers->TimeWrong[1]), array($answers->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE), $this->_translate->_($answers->TimeWrong[1]->Mode))),
            array_merge(explode(':', (string) $answers->TimeWrong[2]), array($answers->TimeWrong[2]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE), $this->_translate->_($answers->TimeWrong[2]->Mode))),
        );

        $view->inlineScript()
             ->appendFile($view->placeholder('baseScripts') . 'User/Exercise/main.js')
        ;
    }

    /**
     * Test page Clocks (digital)
     *
     * @test
     */
    public function clocksdigitalAction()
    {
        $this->getHelper('layout')->enableLayout();           // Enable layout
        $this->getHelper('viewRenderer')->setNoRender(false); // Enable view renderer

        $view = $this->view;

        $auth = Zend_Auth::getInstance();
        $person = null;
        if ($auth->hasIdentity() ) {
            $identity = $auth->getIdentity();
            if ($identity->isUser() ) {
                $userMapper = new Application_Model_UserMapper();
                $person = $userMapper->read($identity->UserId);
            } elseif ($identity->isSupervisor() ) {
                $supervisorMapper = new Application_Model_SupervisorMapper();
                $person = $supervisorMapper->read($identity->SupervisorId);
            }
        }

        $moduleSettings = Ahs_Exercise_Settings::getSessionModuleSettings($person);

        $vars = array(
            'colour'     => ($moduleSettings->Colour       ? ' colour'        : ''      )
                          . ($moduleSettings->ColourSwitch ? ' colour-switch' : ''      ),
            'twentyfour' => ($moduleSettings->Twentyfour   ? ' twentyfour'    : ''      ),
            'sound'      => ($moduleSettings->Sound        ? ''               : ' muted'),
        );
        $view->assign($vars);

        foreach($this->_request->getParams() as $key => $value) {
            switch ($key) {
                case 'module':
                case 'controller':
                case 'action':
                    break;
                case 'h':
                    $h = (int) $value;
                    break;
                case 'm':
                    $m = (int) $value;
                    break;
                default:
                    break 2; // break out of foreach as well
            }
        }

        $answers = new $this->_Ahs_Exercise_Data_Digital_Answers($h, $m);

        switch ($m) {
                case  0:
                    $view->answers = array(
                        array_merge(explode(':', (string) $answers->Time        ), array($answers->Time        ->toText(Ahs_Time_Abstract::TEXT_DIGITAL         ), $this->_translate->_($answers->Time        ->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[0]), array($answers->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_DIGITAL_M00_AS_0), $this->_translate->_($answers->TimeWrong[0]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[1]), array($answers->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_DIGITAL_H×10    ), $this->_translate->_($answers->TimeWrong[1]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[2]), array($answers->TimeWrong[2]->toText(Ahs_Time_Abstract::TEXT_DIGITAL_H×100   ), $this->_translate->_($answers->TimeWrong[2]->Mode))),
                    );
                    break;
                case 15:
                    $view->answers = array(
                        array_merge(explode(':', (string) $answers->Time        ), array($answers->Time        ->toText(Ahs_Time_Abstract::TEXT_ANALOGUE                   ), $this->_translate->_($answers->Time        ->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[0]), array($answers->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE_QUARTERS_AS_NUMBER), $this->_translate->_($answers->TimeWrong[0]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[1]), array($answers->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE                   ), $this->_translate->_($answers->TimeWrong[1]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[2]), array($answers->TimeWrong[2]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE_QUARTERS_AS_NUMBER), $this->_translate->_($answers->TimeWrong[2]->Mode))),
                    );
                    break;
                case 30:
                    $view->answers = array(
                        array_merge(explode(':', (string) $answers->Time        ), array($answers->Time        ->toText(Ahs_Time_Abstract::TEXT_ANALOGUE                                    ), $this->_translate->_($answers->Time        ->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[0]), array($answers->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_DIGITAL_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS), $this->_translate->_($answers->TimeWrong[0]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[1]), array($answers->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE                                    ), $this->_translate->_($answers->TimeWrong[1]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[2]), array($answers->TimeWrong[2]->toText(
                            ($answers->TimeWrong[2]->Hours !== 3 ? Ahs_Time_Abstract::TEXT_DIGITAL_REVERSE_M_DIGITS : Ahs_Time_Abstract::TEXT_DIGITAL_SWITCH_H_AND_M)
                        ), $this->_translate->_($answers->TimeWrong[2]->Mode))),
                    );
                    break;
                case 45:
                    $view->answers = array(
                        array_merge(explode(':', (string) $answers->Time        ), array($answers->Time        ->toText(Ahs_Time_Abstract::TEXT_ANALOGUE   ), $this->_translate->_($answers->Time        ->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[0]), array($answers->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE_TO), $this->_translate->_($answers->TimeWrong[0]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[1]), array($answers->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE   ), $this->_translate->_($answers->TimeWrong[1]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[2]), array($answers->TimeWrong[2]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE   ), $this->_translate->_($answers->TimeWrong[2]->Mode))),
                    );
                    break;
                default:
                    $view->answers = array(
                        array_merge(explode(':', (string) $answers->Time        ), array($answers->Time        ->toText(Ahs_Time_Abstract::TEXT_ANALOGUE_PAST                    ), $this->_translate->_($answers->Time        ->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[0]), array($answers->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_DIGITAL_SWITCH_H_AND_M           ), $this->_translate->_($answers->TimeWrong[0]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[1]), array($answers->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE_TO                      ), $this->_translate->_($answers->TimeWrong[1]->Mode))),
                        array_merge(explode(':', (string) $answers->TimeWrong[2]), array($answers->TimeWrong[2]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE_SWITCH_H_AND_M_THEN_PAST), $this->_translate->_($answers->TimeWrong[2]->Mode))),
                    );
                    break;
        }

        $view->inlineScript()
             ->appendFile($view->placeholder('baseScripts') . 'User/Exercise/main.js')
        ;
    }

    /**
     * Test page Feedback (analogue)
     *
     * @test
     */
    public function feedbackanalogueAction()
    {
        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        foreach($this->_request->getParams() as $key => $value) {
            switch ($key) {
                case 'module':
                case 'controller':
                case 'action':
                    break;
                case 'h':
                    $h = (int) $value;
                    break;
                case 'm':
                    $m = (int) $value;
                    break;
                default:
                    break 2; // break out of foreach as well
            }
        }

        echo '<pre>';
        var_dump(new $this->_Ahs_Exercise_Feedback_Analogue(new $Ahs_Time($h, $m)));
    }

    /**
     * Test page Feedback (digital)
     *
     * @test
     */
    public function feedbackdigitalAction()
    {
        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        foreach($this->_request->getParams() as $key => $value) {
            switch ($key) {
                case 'module':
                case 'controller':
                case 'action':
                    break;
                case 'h':
                    $h = (int) $value;
                    break;
                case 'm':
                    $m = (int) $value;
                    break;
                default:
                    break 2; // break out of foreach as well
            }
        }

        echo '<pre>';
        var_dump(new $this->_Ahs_Exercise_Feedback_Digital(new $Ahs_Time($h, $m)));
    }

    /**
     * Test pages Time to Text
     *
     * @test
     */
    public function timesAction()
    {
        $lang = $this->_getParam('lang');
        switch ($lang) {
            case 'nl':
                {
                    $Ahs_Time = 'Ahs_Time_NL';

                    echo '<pre>';

                    $Ahs_Time::checkTwentyFour();
                    $i = 0;
                    for ($h = 0; $h < 24; $h++) {
                        echo '<table>',
                             '<tr style="background-color: lightgrey">',
                                '<td colspan="2" style="text-align: right">Kloksysteem:</td>',
                                '<th style="text-align: left">Analoog (officieel)</th>',
                                '<th style="text-align: left">Analoog (alternatief)</th>',
                                '<th style="text-align: left">Digitaal</th>',
                                '<th style="text-align: left">Digitaal M 0</th>',
                                '<th style="text-align: left">Digitaal H ×10</th>',
                                '<th style="text-align: left">Digitaal H ×100</th>';
                        for ($m = 0; $m < 60; $m++) {
                            $time = new $Ahs_Time($h, $m);
                            echo '<tr>',
                                 '<td width="50" style="text-align: right">' , ++$i , '.</td>',
                                 "<th width=\"100\">{$time}</th>";
                            $Ahs_Time::checkQuadrants();
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_ANALOGUE)      , '</td>';
                            $Ahs_Time::uncheckQuadrants();
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_ANALOGUE)      , '</td>';
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_DIGITAL)       , '</td>';
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_DIGITAL_M00_AS_0)   , '</td>';
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_DIGITAL_H×10) , '</td>';
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_DIGITAL_H×100), '</td>';
                            echo '</tr>';

                        }
                        echo '</table>';
                    }
                }
                break;
            case 'en':
                {
                    $Ahs_Time = 'Ahs_Time_EN';

                    echo '<pre>';

                    $Ahs_Time::checkTwentyFour();
                    $i = 0;
                    for ($h = 0; $h < 24; $h++) {
                        echo '<table>',
                             '<tr style="background-color: lightgrey">',
                                '<td colspan="2" style="text-align: right">Type:</td>',
                                '<th style="text-align: left">Analogue</th>',
                                '<th style="text-align: left">Digital</th>';
                        for ($m = 0; $m < 60; $m++) {
                            $time = new $Ahs_Time($h, $m);
                            echo '<tr>',
                                 '<td width="50" style="text-align: right">' , ++$i , '.</td>',
                                 "<th width=\"100\">{$time}</th>";
                            $Ahs_Time::checkQuadrants();
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_ANALOGUE), '</td>';
                            echo '<td width="250">' , $time->toText(Ahs_Time_Abstract::TEXT_DIGITAL) , '</td>';
                            echo '</tr>';

                        }
                        echo '</table>';
                    }
                }
                break;
            default:
                break;
        }
    }
}
