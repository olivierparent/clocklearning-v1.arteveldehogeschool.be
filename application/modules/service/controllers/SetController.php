<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Service_SetController extends Zend_Rest_Controller
{
    // Remember to add it to _initRestRoutes() in application/Bootstrap.php

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();

        $this->getHelper('layout')->disableLayout();         // Disable layout
        $this->getHelper('viewRenderer')->setNoRender(true); // Disable view renderer
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $setMapper = new Application_Model_SetMapper();

        $setId = (int) $this->getParam('id');

        if ($setMapper->delete($setId)) {
            $this
                ->getResponse()
                ->setBody('deleteAction')
                ->setHttpResponseCode(Ahs_Response::HTTP_OK)
            ;
        } else {
            $this->getResponse()->setHttpResponseCode(Ahs_Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this->getResponse()->setBody('getAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {

    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $this->getResponse()->setBody('indexAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $data = json_decode($this->getRequest()->getRawBody());

        try {
            $identity = $this->_auth->getIdentity();
            $supervisorMapper = new Application_Model_SupervisorMapper();
            $supervisor = $supervisorMapper->read($identity->SupervisorId);

            $groupMapper = new Application_Model_GroupMapper();
            $group = $groupMapper->readWhereSupervisor($supervisor, $data->group);

            $userMapper = new Application_Model_UserMapper();
            $users = $userMapper->readWhereGroup($group);

            $setMapper = new Application_Model_SetMapper();
            $exerciseMapper = new Application_Model_ExerciseMapper();

            $db = Zend_Db_Table::getDefaultAdapter();
            $db->beginTransaction();
            foreach ($users as $user) {
                $set = new Application_Model_Set(array('userId' => $user->Id) );
                $setMapper->save($set);

                foreach ($data->modules as $module) {
                    foreach ($module->exerciseTypes as $exerciseType) {
                        for ($i = 0; $i < $module->amount; $i++) {
                            $exercise = new Application_Model_Exercise(array(
                                'exerciseTypeId' => $exerciseType->id,
                                'setId'          => $set->Id,
                            ));
                            $exerciseMapper->save($exercise);
                        }
                    }
                }
            }
            $db->commit();
            $this->getResponse()
                 ->setHttpResponseCode(Ahs_Response::HTTP_OK);
        } catch (Exception $e) {
            $db->rollBack();
            $this->getResponse()
                 ->setBody($e)
                 ->setHttpResponseCode(Ahs_Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $this->getResponse()->setBody('putAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

}
