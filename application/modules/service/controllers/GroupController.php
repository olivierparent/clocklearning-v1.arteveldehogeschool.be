<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Service_GroupController extends Zend_Rest_Controller
{
    // Remember to add it to _initRestRoutes() in application/Bootstrap.php

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();

        $this->getHelper('layout')->disableLayout();         // Disable layout
        $this->getHelper('viewRenderer')->setNoRender(true); // Disable view renderer
    }

    public function deleteAction()
    {
        $this->getResponse()->setBody('deleteAction')
                            ->setHttpResponseCode(200);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this->getResponse()->setBody('getAction')
                            ->setHttpResponseCode(200);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {

    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $translate = Zend_Registry::get('Zend_Translate');

        $identity = $this->_auth->getIdentity();
        $supervisorMapper = new Application_Model_SupervisorMapper();
        $supervisor = $supervisorMapper->read($identity->SupervisorId);

        $groupMapper = new Application_Model_GroupMapper();
        $groups = $groupMapper->readWhereSupervisor($supervisor);

        $response = array( 'groups' => array());
        foreach ($groups as $group) {
            if ($group->Name === Application_Model_Group::DEFAULT_NAME) {
                $group->Name = $translate->_('Default group');
            }
            $response['groups'][] = $group->toArray();
        }
        //Zend_Debug::dump($groups);
        $this->getResponse()->setHeader('Content-Type', 'application/json')
                            ->setBody(json_encode($response))
                            ->setHttpResponseCode(200);
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $this->getResponse()->setBody('postAction')
                            ->setHttpResponseCode(200);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $this->getResponse()->setBody('putAction')
                            ->setHttpResponseCode(200);
    }

}
