<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Service_AttemptController extends Zend_Rest_Controller
{
    // Remember to add it to _initRestRoutes() in application/Bootstrap.php

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();

        $this
            ->getHelper('layout')
            ->disableLayout();         // Disable layout
        $this
            ->getHelper('viewRenderer')
            ->setNoRender(true); // Disable view renderer
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $this
            ->getResponse()
            ->setBody('deleteAction')
            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this
            ->getResponse()
            ->setBody('getAction')
            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {

    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $this
            ->getResponse()
            ->setBody('indexAction')
            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $this
            ->getResponse()
            ->setBody('postAction')
            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $data = json_decode($this->getRequest()->getRawBody());
//        Zend_Debug::dump($data); exit;

        /**
         * Return id of last updated Exercise record?
         */

        try {
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->beginTransaction();

            $exerciseMapper = new Application_Model_ExerciseMapper();
            if (isset($data->id)) {
                $exercise = $exerciseMapper->read($data->id);
            } else {
                $identity = $this->_auth->getIdentity();
                $userMapper = new Application_Model_UserMapper();
                $user = $userMapper->read($identity->UserId);

                $exercise = $exerciseMapper->readFirstUncompletedWhereUser($user);
            }

            $exercise->Questions = json_encode($data->questions);
            $exercise->Answers   = json_encode($data->answers);
            $exercise->Modes     = json_encode($data->modes);
            $exercise->Solution  = json_encode($data->solution);
            $exercise->Correct   = $data->correct;
            $exercise->TimeStart = $data->timeStart;
            $exercise->TimeEnd   = $data->timeEnd;

            $exerciseMapper->save($exercise);

            $attemptValue = end($data->attempts);
            $attempt = json_encode($attemptValue);
            $exerciseAttempt = new Application_Model_ExerciseAttempt();
            $exerciseAttempt->Answer     = is_int($attemptValue) ? $data->answers[$attempt] : 'Answer: ' . $attemptValue;
            $exerciseAttempt->Mode       = is_int($attemptValue) ? $data->modes[$attempt]   : 'Answer: ' . $attemptValue;
            $exerciseAttempt->Correct    = $data->correct;
            $exerciseAttempt->TimeEnd    = $exercise->TimeEnd;
            $exerciseAttempt->ExerciseId = $exercise->Id;

            $exerciseAttemptMapper = new Application_Model_ExerciseAttemptMapper();
            $exerciseAttemptMapper->save($exerciseAttempt);
            $db->commit();
            $this
                ->getResponse()
                ->setBody(json_encode(array('id' => $exercise->Id)))
                ->setHttpResponseCode(Ahs_Response::HTTP_OK);
        } catch (Exception $e) {
            $db->rollBack();
            $this
                ->getResponse()
                ->setBody($e)
                ->setHttpResponseCode(Ahs_Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
