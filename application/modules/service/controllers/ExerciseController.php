<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Service_ExerciseController extends Zend_Rest_Controller
{
    // Remember to add it to _initRestRoutes() in application/Bootstrap.php

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Analogue_Answers;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Analogue_Answers_Digital;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Analogue_Answers_Reverse;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Analogue_Answers_Test;
    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Combinations;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Combinations_Conversion;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Digital_Answers;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Settings;

    /**
     * @var string
     */
    protected $_Ahs_Exercise_Data_Settings_Conversion;


    /**
     * @var string
     */
    protected $_Ahs_Exercise_Feedback_Analogue;

    /**
     * @var string
     */
    protected $_Ahs_Time;

    public function init()
    {
        $this->getHelper('layout')->disableLayout();         // Disable layout
        $this->getHelper('viewRenderer')->setNoRender(true); // Disable view renderer

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
            switch ($identity->Role) {
                case Ahs_Acl::ROLE_USER;
                    $userMapper = new Application_Model_UserMapper();
                    $person = $userMapper->read($identity->UserId);
                    break;
                case Ahs_Acl::ROLE_SUPERVISOR:
                    $supervisorMapper = new Application_Model_SupervisorMapper();
                    $person = $supervisorMapper->read($identity->SupervisorId);
                    break;
                default:
                    $person = null;
                    break;
            }
        } else {
            $person = null;
        }
//        Zend_Debug::dump($person); exit;

        $lang = strtoupper(Zend_Registry::get('Zend_Locale')->getLanguage());
        $this->_Ahs_Exercise_Data_Analogue_Answers         = "Ahs_Exercise_Data_Analogue_Answers_{$lang}";
        $this->_Ahs_Exercise_Data_Analogue_Answers_Digital = "Ahs_Exercise_Data_Analogue_Answers_Digital_{$lang}";
        $this->_Ahs_Exercise_Data_Analogue_Answers_Reverse = "Ahs_Exercise_Data_Analogue_Answers_Reverse_{$lang}";
        $this->_Ahs_Exercise_Data_Analogue_Answers_Test    = "Ahs_Exercise_Data_Analogue_Answers_Test_{$lang}";
        $this->_Ahs_Exercise_Data_Combinations             = "Ahs_Exercise_Data_Combinations_{$lang}";
        $this->_Ahs_Exercise_Data_Combinations_Conversion  = "Ahs_Exercise_Data_Combinations_Conversion_{$lang}";
        $this->_Ahs_Exercise_Data_Digital_Answers          = "Ahs_Exercise_Data_Digital_Answers_{$lang}";
        $this->_Ahs_Exercise_Data_Settings                 = "Ahs_Exercise_Data_Settings_{$lang}";
        $this->_Ahs_Exercise_Data_Settings_Conversion      = "Ahs_Exercise_Data_Settings_Conversion_{$lang}";
        $this->_Ahs_Exercise_Feedback_Analogue             = "Ahs_Exercise_Feedback_Analogue_{$lang}";
        $this->_Ahs_Time                                   = "Ahs_Time_{$lang}";

        // Dynamic class name
        $Ahs_Time = $this->_Ahs_Time;

        try {
            $exerciseSettings = Ahs_Exercise_Settings::getSessionExerciseSettings($person);

            if ($exerciseSettings->Hour) {
                $Ahs_Time::checkHour();
            } else {
                $Ahs_Time::uncheckHour();
            }

            if ($exerciseSettings->HalfHour) {
                $Ahs_Time::checkHalfHour();
            } else {
                $Ahs_Time::uncheckHalfHour();
            }

            if ($exerciseSettings->QuarterPast) {
                $Ahs_Time::checkQuarterPast();
            } else {
                $Ahs_Time::uncheckQuarterPast();
            }

            if ($exerciseSettings->QuarterTo) {
                $Ahs_Time::checkQuarterTo();
            } else {
                $Ahs_Time::uncheckQuarterTo();
            }

//            Zend_Debug::dump($exerciseSettings->MinutesIncrement); exit;
            switch ($exerciseSettings->MinutesIncrement) {
                case Application_Model_ExerciseSettings::MINUTES_INCREMENT_01:
                    $Ahs_Time::selectIncrementMinutes();
                    break;
                case Application_Model_ExerciseSettings::MINUTES_INCREMENT_05:
                    $Ahs_Time::selectIncrementFives();
                    break;
                case Application_Model_ExerciseSettings::MINUTES_INCREMENT_10:
                    $Ahs_Time::selectIncrementTens();
                    break;
                default:
                    break;
            }
        } catch (Exception $e) {
            // Do nothing.
        }

        try {
            $moduleSettings = Ahs_Exercise_Settings::getSessionModuleSettings($person);

            if ($moduleSettings->Quadrants == Application_Model_ModuleSettings::QUADRANTS_OFFICIAL) {
                $Ahs_Time::checkQuadrants();
            } elseif ($moduleSettings->Quadrants == Application_Model_ModuleSettings::QUADRANTS_ALTERNATIVE) {
                $Ahs_Time::uncheckQuadrants();
            }

            if ($moduleSettings->Twentyfour) {
                $Ahs_Time::checkTwentyFour();
            } else {
                $Ahs_Time::uncheckTwentyFour();
            }
        } catch (Exception $e) {
            // Do nothing.
        }
    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $this->getResponse()->setBody('Rika-Tika RESTful service')
                            ->setHttpResponseCode(200);
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $exerciseMapper = new Application_Model_ExerciseMapper();

        $exerciseId = (int) $this->getParam('id');

        if ($exerciseMapper->delete($exerciseId)) {
            $this
                ->getResponse()
                ->setBody('deleteAction')
                ->setHttpResponseCode(Ahs_Response::HTTP_OK)
            ;
        } else {
            $this->getResponse()->setHttpResponseCode(Ahs_Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json')
                            ->setHttpResponseCode(200);

        switch ($this->_getParam('id')) {
            case 'type01':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Analogue_Answers()
                );
                break;
            case 'type02':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Digital_Answers()
                );
                break;
            case 'type03':
            case 'type04':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Analogue_Answers_Reverse()
                );
                break;
            case 'type05':
            case 'type06':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Combinations()
                );
                break;
            case 'type11':
            case 'type12':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Settings()
                );
                break;
            case 'type15':
            case 'type16':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Analogue_Answers_Digital()
                );
                break;
            case 'type17':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Combinations_Conversion()
                );
                break;
            case 'type18':
            case 'type19':
                $this->getResponse()->setBody(
                    new $this->_Ahs_Exercise_Data_Settings_Conversion()
                );
                break;
            default:
                $this->getResponse()->setHeader('Content-Type', 'text/html')
                                    ->setBody('getAction')
                                    ->setHttpResponseCode(500);
                break;
        }
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {

    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $this->getResponse()->setBody('postAction')
                            ->setHttpResponseCode(200);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $this->getResponse()->setBody('putAction')
                            ->setHttpResponseCode(200);
    }

}
