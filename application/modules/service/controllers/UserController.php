<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Service_UserController extends Zend_Rest_Controller
{
    // Remember to add it to _initRestRoutes() in application/Bootstrap.php

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();

        $this->getHelper('layout')->disableLayout();         // Disable layout
        $this->getHelper('viewRenderer')->setNoRender(true); // Disable view renderer
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $userMapper = new Application_Model_UserMapper();

        $userId = (int) $this->getParam('id');

        if ($userMapper->delete($userId)) {
            $this
                ->getResponse()
                ->setBody('deleteAction')
                ->setHttpResponseCode(Ahs_Response::HTTP_OK)
            ;
        } else {
            $this->getResponse()->setHttpResponseCode(Ahs_Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        try {
            $identity = $this->_auth->getIdentity();
            $supervisorMapper = new Application_Model_SupervisorMapper();
            $supervisor = $supervisorMapper->read($identity->SupervisorId);

            $groupMapper = new Application_Model_GroupMapper();
            $group = $groupMapper->readWhereSupervisor($supervisor, (int) $this->getParam('group'));

            $userMapper = new Application_Model_UserMapper();
            $users = $userMapper->readWhereGroup($group);

            $response = array('users' => array());
            foreach ($users as $user) {
                $response['users'][] = $user->toArray();
            }
            //Zend_Debug::dump($users);
            $this->getResponse()->setHeader('Content-Type', 'application/json')
                                ->setBody(json_encode($response))
                                ->setHttpResponseCode(Ahs_Response::HTTP_OK);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(Ahs_Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {

    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {
        $identity = $this->_auth->getIdentity();
        $supervisorMapper = new Application_Model_SupervisorMapper();
        $supervisor = $supervisorMapper->read($identity->SupervisorId);

        $groupMapper = new Application_Model_GroupMapper();
        $group = $groupMapper->readDefaultWhereSupervisor($supervisor);

        $userMapper = new Application_Model_UserMapper();
        $users = $userMapper->readWhereGroup($group);

        $response = array('users' => array());
        foreach ($users as $user) {
            $response['users'][] = $user->toArray();
        }
        //Zend_Debug::dump($users);
        $this->getResponse()->setHeader('Content-Type', 'application/json')
                            ->setBody(json_encode($response))
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $this->getResponse()->setBody('postAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        try {
            $identity = $this->_auth->getIdentity();
            $supervisorMapper = new Application_Model_SupervisorMapper();
            $supervisor = $supervisorMapper->read($identity->SupervisorId);
//        Zend_Debug::dump($supervisor);
//        Zend_Debug::dump((int) $this->getParam('id'));
//        exit;

            $groupMapper = new Application_Model_GroupMapper();
            if ($this->getParam('group')) {
                $group = $groupMapper->readWhereSupervisor($supervisor, (int) $this->getParam('group'));
            } else {
                $group = $groupMapper->readDefaultWhereSupervisor($supervisor);
            }

//        Zend_Debug::dump($group); exit;

            $userMapper = new Application_Model_UserMapper();
            $user = $userMapper->readWhereSupervisor($supervisor, (int) $this->getParam('id'));
//            Zend_Debug::dump($user);
//            Zend_Debug::dump($group); exit;
            $user->GroupId = $group->Id;
            $userMapper->save($user);

            $this->getResponse()->setBody('Updated')
                ->setHttpResponseCode(Ahs_Response::HTTP_OK);
        } catch (Exception $e) {
            $this->getResponse()
                 ->setBody($e)
                 ->setHttpResponseCode(Ahs_Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
