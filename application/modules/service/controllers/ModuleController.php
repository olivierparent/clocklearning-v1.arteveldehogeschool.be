<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Service_ModuleController extends Zend_Rest_Controller
{
    // Remember to add it to _initRestRoutes() in application/Bootstrap.php

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();

        $this->getHelper('layout')->disableLayout();         // Disable layout
        $this->getHelper('viewRenderer')->setNoRender(true); // Disable view renderer
    }

    /**
     * The delete action handles DELETE requests and receives an 'id'
     * parameter; it should update the server resource state of the resource
     * identified by the 'id' value.
     */
    public function deleteAction()
    {
        $this->getResponse()->setBody('deleteAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The get action handles GET requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function getAction()
    {
        $this->getResponse()->setBody('getAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {

    }

    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */
    public function indexAction()
    {

        $identity = $this->_auth->getIdentity();
        $supervisorMapper = new Application_Model_SupervisorMapper();
        $supervisor = $supervisorMapper->read($identity->SupervisorId);
        $exerciseSettings = Ahs_Exercise_Settings::getSessionExerciseSettings($supervisor);

        $translate = Zend_Registry::get('Zend_Translate');
        try {
            $moduleMapper = new Application_Model_ModuleMapper;
            $modules = $moduleMapper->readAllActive();

            $exerciseTypeMapper = new Application_Model_ExerciseTypeMapper();
            foreach ($modules as $module_index => $module) {
                $exerciseTypes = $exerciseTypeMapper->readAllActiveWhereModule($module);
                foreach ($exerciseTypes as $exercise_index => $exerciseType) {
                    $exerciseType->Name = $translate->_($exerciseType->Name);
                    $exerciseTypes[$exercise_index] = $exerciseType->toArray();
                }

                $module->Name = $translate->_($module->Name);
                $module->ExerciseTypes = $exerciseTypes;
                $modules[$module_index] = array_merge($module->toArray(), array('exerciseAmount' => $exerciseSettings->Amount));
            }

            $response = $modules;
//            Zend_Debug::dump($modules); exit;
            $this->getResponse()->setHeader('Content-Type', 'application/json')
                                ->setBody(json_encode($response))
                                ->setHttpResponseCode(Ahs_Response::HTTP_OK);
        } catch (Exception $e) {
            var_dump($e); exit;
            $this->getResponse()->setHttpResponseCode(Ahs_Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * The post action handles POST requests; it should accept and digest a
     * POSTed resource representation and persist the resource state.
     */
    public function postAction()
    {
        $this->getResponse()->setBody('postAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

    /**
     * The put action handles PUT requests and receives an 'id' parameter; it
     * should update the server resource state of the resource identified by
     * the 'id' value.
     */
    public function putAction()
    {
        $this->getResponse()->setBody('putAction')
                            ->setHttpResponseCode(Ahs_Response::HTTP_OK);
    }

}
