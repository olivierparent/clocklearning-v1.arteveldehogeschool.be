<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Account_Login extends Ahs_Form_Backoffice
{
    /**
     *
     */
    public function init()
    {
        $text_username = new Zend_Form_Element_Text('username');
        $text_username
            ->setLabel('User name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_username->getLabel())
            ->setRequired()
            ->addFilter('StringTrim') // Zend/Filter/StringTrim.php
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $password_raw = new Zend_Form_Element_Password('passwordraw');
        $password_raw
            ->setLabel('Password')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $password_raw->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $submit = new Zend_Form_Element_Submit('submit');
        $submit
            ->setLabel('Log in')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButtonOpen())
        ;

        $view = Zend_Layout::getMvcInstance()->getView();

        $register = new Zend_Form_Element_Button('register');
        $register
            ->setDescription('Register')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButtonClose($view->baseUrl('supervisor/account/register')))
        ;

        $this
            ->addElement($text_username)
            ->addElement($password_raw )
            ->addElement($submit       )
            ->addElement($register     )
        ;
    }
}
