<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Account_Register extends Ahs_Form_Backoffice
{
    public function init()
    {
        $email = new Zend_Form_Element_Text('email');
        $email
            ->setLabel('Email address')
            ->setAttrib('type', 'email')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $email->getLabel())
            ->setRequired()
            ->addValidator('EmailAddress', true)
            ->addValidator('Db_NoRecordExists', true, array(
                'table' => 'Supervisor',
                'field' => 'sup_email',
            ))
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;
        $text_givenname = new Zend_Form_Element_Text('givenname');
        $text_givenname
            ->setLabel('Given name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_givenname->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $text_familyname = new Zend_Form_Element_Text('familyname');
        $text_familyname
            ->setLabel('Family name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_familyname->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $radio_role = new Ahs_Form_Element_Radio_Backoffice('role');
        $radio_role
            ->setLabel('Role')
//            ->setAttrib('class', 'form-control')
            ->setRequired()
            ->addMultiOption(Application_Model_Supervisor::ROLE_TEACHER  , 'Teacher'  )
            ->addMultiOption(Application_Model_Supervisor::ROLE_THERAPIST, 'Therapist')
//            ->setValue('therapist')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $select_language = new Zend_Form_Element_Select('language');
        $select_language
            ->setLabel('Language')
            ->setAttrib('class', 'form-control')
            ->setValue('NL')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        // Get languages from database
        $languageMapper = new Application_Model_LanguageMapper();
        $languages = $languageMapper->readAll();
        foreach ($languages as $language) {
            $select_language->addMultiOption($language->Id, $language->Name);
        }

        $text_username = new Zend_Form_Element_Text('username');
        $text_username
            ->setLabel('User name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_username->getLabel())
            ->setRequired()
            ->addFilter('StringTrim') // Zend/Filter/StringTrim.php
            ->addValidator('Db_NoRecordExists', true, array(
                'table' => 'Supervisor',
                'field' => 'sup_username'))
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $password_raw = new Zend_Form_Element_Password('passwordraw');
        $password_raw
            ->setLabel('Password')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $password_raw->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $password_repeat = new Zend_Form_Element_Password('passwordrepeat');
        $password_repeat
            ->setLabel('Repeat password')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $password_repeat->getLabel())
            ->setRequired()
            ->addValidator('Identical', true, array('passwordraw')) // Zend/Validate/Identical.php
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $submit = new Zend_Form_Element_Submit('submit');
        $submit
            ->setLabel('Register')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButtonOpen())
        ;

        $view = Zend_Layout::getMvcInstance()->getView();

        $login = new Zend_Form_Element_Button('login');
        $login
            ->setDescription('Log in')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButtonClose($view->baseUrl('supervisor/account/login')))
        ;

        $this->addElement($email          )
             ->addElement($text_username  )
             ->addElement($password_raw   )
             ->addElement($password_repeat)
             ->addElement($text_givenname )
             ->addElement($text_familyname)
             ->addElement($radio_role     )
             ->addElement($select_language)
             ->addElement($submit         )
             ->addElement($login          )
        ;
    }
}
