<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Settings_Exercise extends Ahs_Form_Backoffice
{
    public function init()
    {
        $amount     = new Supervisor_Form_Settings_Exercise_Amount();
        $assistance = new Supervisor_Form_Settings_Exercise_Assistance();
        $level      = new Supervisor_Form_Settings_Exercise_Level();
        $mode       = new Supervisor_Form_Settings_Exercise_Mode();

        $this->addSubForms(array(
            'amountForm'     => $amount,
            'levelForm'      => $level,
            'modeForm'       => $mode,
            'assistanceForm' => $assistance,
        ));

        // Prevent CSRF
//        $hash = new Zend_Form_Element_Hash('no-csrf-settings');
//        $hash->setSalt('My Unique Salt');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit
            ->setLabel('Save')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButton())
        ;

        $this->addElements(array(
                $submit,
//                $hash,
        ));
    }
}
