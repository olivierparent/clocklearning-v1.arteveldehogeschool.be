<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Settings_Exercise_Level extends Zend_Form_SubForm
{
    public function init()
    {
        $this->setLegend('Level');

        $check_halfHour = new Ahs_Form_Element_Checkbox_Backoffice('halfHour');
        $check_halfHour->setLabel('Half hour');

        $check_hour = new Ahs_Form_Element_Checkbox_Backoffice('hour');
        $check_hour->setLabel('Hour');

        $check_quarterPast = new Ahs_Form_Element_Checkbox_Backoffice('quarterPast');
        $check_quarterPast->setLabel('Quarter past');

        $check_quarterTo = new Ahs_Form_Element_Checkbox_Backoffice('quarterTo');
        $check_quarterTo->setLabel('Quarter to');

        $this->addDisplayGroup(
                 array(
                     $check_hour,
                     $check_halfHour,
                     $check_quarterPast,
                     $check_quarterTo,
                 ),
                 'clocklevel-quarters',
                 array(
                     'legend' => 'Quarter hours',
                 )
             )
             ->setDisplayGroupDecorators(Ahs_Form_Decorators_Backoffice::getDisplayGroup());

        $check_minutes = new Ahs_Form_Element_Checkbox_Backoffice('minutes');
        $check_minutes->setLabel('Minutes');

        $radio_increments = new Ahs_Form_Element_Radio_Backoffice('minutesIncrement');
//        $radio_increments->setAttrib('disabled', true);
        $translation = explode('|', $this->getTranslator()->translate('minute|minutes'));
        foreach (array(Application_Model_ExerciseSettings::MINUTES_INCREMENT_10, Application_Model_ExerciseSettings::MINUTES_INCREMENT_05, Application_Model_ExerciseSettings::MINUTES_INCREMENT_01) as $value) {
            $radio_increments->addMultiOption($value, "{$value} " . $translation[( (int) $value > 1 )]); // Plurals are not supported for XLIFF
        }
//                         ->setValue('10')

        $this->addDisplayGroup(
                 array($check_minutes,
                       $radio_increments,
                 ),
                 'clocklevel-minutes',
                 array(
                     'legend' => 'Minutes',
                 )
             )
             ->setDisplayGroupDecorators(Ahs_Form_Decorators_Backoffice::getDisplayGroup());
        ;

        $check_seconds = new Ahs_Form_Element_Checkbox_Backoffice('seconds');
        $check_seconds->setLabel('Seconds');

//        $this->addDisplayGroup(
//                 array(
//                     $check_seconds,
//                 ),
//                 'clocklevel-seconds',
//                 array(
//                     'legend' => 'Seconds',
//                 )
//             )
//             ->setDisplayGroupDecorators(Ahs_Form_Decorators_Backoffice::getDisplayGroup());
//        ;
    }
}
