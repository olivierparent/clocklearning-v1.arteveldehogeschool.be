<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Settings_Exercise_Assistance extends Zend_Form_SubForm
{
    public function init()
    {
        $radio_increments = new Ahs_Form_Element_Radio_Backoffice('assistance');
        $radio_increments
            ->addMultiOption(Application_Model_ExerciseSettings::ASSISTANCE_MODELLING, 'Modelling')
            ->addMultiOption(Application_Model_ExerciseSettings::ASSISTANCE_VISUAL   , 'Visual'   )
            ->addMultiOption(Application_Model_ExerciseSettings::ASSISTANCE_VERBAL   , 'Verbal'   )
//            ->setValue(Application_Model_Settings_Exercise::ASSISTANCE_MODELLING)
        ;

        $check_textual = new Ahs_Form_Element_Checkbox_Backoffice('subtitles');
        $check_textual
            ->setLabel('Subtitles')
//            ->setValue(true)
        ;

        $this
            ->setLegend('Assistance')
            ->addElement($radio_increments)
            ->addElement($check_textual)
        ;

    }
}
