<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Settings_Exercise_Amount extends Zend_Form_SubForm
{
    public function init()
    {
        $text_amount = new Zend_Form_Element_Text('amount');
		$text_amount
            ->setLabel('Amount of exercises')
            ->setAttrib('class', 'form-control')
            ->setRequired()
            ->setAttrib('type', 'number')
            ->setAttrib('min' ,  5)
            ->setAttrib('max' , 50)
            ->setAttrib('step',  5)
            ->addValidator('GreaterThan', true, array('min' =>   1))
            ->addValidator('LessThan'   , true, array('max' => 100))
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $this->setLegend('Amount')
             ->addElement($text_amount)
        ;
    }
}
