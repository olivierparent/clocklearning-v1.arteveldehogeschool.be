<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Users_Add extends Ahs_Form_Backoffice
{
    public function init()
    {
        //$this->setDecorators(Ahs_Form_Decorators_Backoffice::getForm());

        $text_givenname = new Zend_Form_Element_Text('givenname');
        $text_givenname
            ->setLabel('Given name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_givenname->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $text_familyname = new Zend_Form_Element_Text('familyname');
        $text_familyname
            ->setLabel('Family name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_familyname->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $text_username = new Zend_Form_Element_Text('username');
        $text_username
            ->setLabel('User name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_username->getLabel())
            ->setRequired()
            ->addFilter('StringTrim')                                 // Zend/Filter/StringTrim.php
            ->addValidator('Db_NoRecordExists', true, array('table' => 'User',
                                                          'field' => 'usr_username'))
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $password_raw = new Zend_Form_Element_Password('passwordraw');
        $password_raw
            ->setLabel('Password')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $password_raw->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $password_repeat = new Zend_Form_Element_Password('passwordrepeat');
        $password_repeat
            ->setLabel('Repeat password')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $password_repeat->getLabel())
            ->setRequired()
            ->addValidator('Identical', true, array('passwordraw')) // Zend/Validate/Identical.php
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $date_bday = new Zend_Form_Element_Text('birthday');
        $date_bday
            ->setLabel('Date of birth')
            ->setAttrib('class', 'form-control')
            ->setAttrib('type', 'date')
            ->addValidator('Date', true, array('yyyy-mm-dd'))             // Zend/Validate/Date.php
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $submit = new Zend_Form_Element_Submit('submit');
        $submit
            ->setLabel('Add')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButton())
        ;

        $this
            ->setMethod('post')
            ->setAction('')
            ->addElement($text_givenname)
            ->addElement($text_familyname)
            ->addElement($text_username)
            ->addElement($password_raw)
            ->addElement($password_repeat)
            ->addElement($date_bday)
            ->addElement($submit)
        ;
    }
}
