<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_Form_Groups_Add extends Ahs_Form_Backoffice
{
    public function init()
    {
        //$this->setDecorators(Ahs_Form_Decorators_Backoffice::getForm());

        $text_name = new Zend_Form_Element_Text('name');
        $text_name
            ->setLabel('Group name')
            ->setAttrib('class', 'form-control')
            ->setAttrib('placeholder', $text_name->getLabel())
            ->setRequired()
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getElement())
        ;

        $submit = new Zend_Form_Element_Submit('submit');
        $submit
            ->setLabel('Add')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(Ahs_Form_Decorators_Backoffice::getButton())
        ;

        $this
            ->setMethod('post')
            ->setAction('')
            ->addElement($text_name)
            ->addElement($submit)
        ;
    }
}
