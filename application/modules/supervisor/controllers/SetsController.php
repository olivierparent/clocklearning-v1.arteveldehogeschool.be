<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_SetsController extends Zend_Controller_Action
{
    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();
    }

    public function indexAction()
    {
        $this->redirect('/supervisor/sets/list');
    }

    /**
     * Results page
     */
    public function listAction()
    {
        if ($this->_auth->hasIdentity() ) {
            $identity = $this->_auth->getIdentity();

            $supervisorMapper      = new Application_Model_SupervisorMapper();
            $groupMapper           = new Application_Model_GroupMapper();
            $userMapper            = new Application_Model_UserMapper();
            $setMapper             = new Application_Model_SetMapper();
            $exerciseMapper        = new Application_Model_ExerciseMapper();
            $exerciseAttemptMapper = new Application_Model_ExerciseAttemptMapper();
            $exerciseTypeMapper    = new Application_Model_ExerciseTypeMapper();

            $supervisor = $supervisorMapper->read($identity->SupervisorId);
            $groups     = $groupMapper->readWhereSupervisor($supervisor);

            foreach ($groups as $group) {
                $group->Users = $userMapper->readWhereGroup($group);
                foreach ($group->Users as $user) {
                    $user->Sets = $setMapper->readWhereUser($user);
                    foreach ($user->Sets as $set) {
                        $set->Exercises = $exerciseMapper->readWhereSet($set);
                        foreach ($set->Exercises as $exercise) {
                            $exercise->Type     = $exerciseTypeMapper->read($exercise->exerciseTypeId);
                            $exercise->Attempts = $exerciseAttemptMapper->readAllWhereExercise($exercise);
                        }
                    }
                }
            }

            $view = $this->_getView();
            $view
                ->assign('title' , 'Supervisor')
                ->assign('groups', $groups     )
                ->inlineScript()
                    ->appendFile($view->placeholder('baseScripts') . 'Supervisor/Sets/list.js')
            ;

        }
    }

    /**
     * Select page
     */
    public function addAction()
    {
        if ($this->_auth->hasIdentity() ) {
            $identity = $this->_auth->getIdentity();

            $supervisorMapper = new Application_Model_SupervisorMapper();
            $supervisor = $supervisorMapper->read($identity->SupervisorId);

            $groupMapper = new Application_Model_GroupMapper();
            $groups = $groupMapper->readWhereSupervisor($supervisor);

            $exerciseTypeMapper = new Application_Model_ExerciseTypeMapper();
            $moduleMapper = new Application_Model_ModuleMapper();
            $modules = $moduleMapper->readAllActive();
            foreach ($modules as $module) {
                $module->ExerciseTypes = $exerciseTypeMapper->readAllActiveWhereModule($module);
            }

            $view = $this->_getView();
            $view
                ->assign('title'  , ''      )
                ->assign('groups' , $groups )
                ->assign('modules', $modules)
                ->inlineScript()
                    ->appendFile($view->placeholder('baseScripts') . 'Supervisor/Sets/add.js')
            ;
        }
    }

    public function editAction()
    {
        // @todo Implement editAction()
    }

    public function deleteAction()
    {
        // @todo Implement deleteAction()
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $view = $this->view;

        return $view;
    }
}
