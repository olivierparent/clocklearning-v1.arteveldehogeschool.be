<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */

class Supervisor_SettingsController extends Zend_Controller_Action
{
    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();
        if ($this->_auth->hasIdentity() ) {
            $identity = $this->_auth->getIdentity();
            if (!$identity->isSupervisor()) {
                return $this->redirect('/supervisor/account/login');
            }
        }
    }

    public function indexAction()
    {
        $this->redirect('/supervisor/settings/exercise/');
    }

    public function exerciseAction()
    {
        $view = $this->_getView();
        $identity = $this->_auth->getIdentity();
        $supervisorMapper = new Application_Model_SupervisorMapper();
        $supervisor = $supervisorMapper->read($identity->SupervisorId);
        $id = $supervisor->ExerciseSettingsId;
        $translate = Zend_Registry::get('Zend_Translate');

        $params = $this->getAllParams();
        if (isset($params['group']) ) {
            try {
                $groupMapper = new Application_Model_GroupMapper();
                $group = $groupMapper->readWhereSupervisor($supervisor, (int) $params['group']);
                $view->assign('heading', sprintf($translate->_('Exercise settings for %s'), $group->Name === Application_Model_Group::DEFAULT_NAME ? $translate->_('Default group') : $group->Name) );

                if ($group->ExerciseSettingsId !== null) {
                    $id = $group->ExerciseSettingsId;
                }

            } catch (Exception $e) {
                throw new Exception('Group could not be found');
            }
        } elseif (isset($params['user'])) {
            try {
                $userMapper = new Application_Model_UserMapper();
                $user = $userMapper->readWhereSupervisor($supervisor, (int) $params['user']);
                $view->assign('heading', sprintf($translate->_('Exercise settings for %s'), "{$user->Givenname} {$user->Familyname}") );

                if ($user->ExerciseSettingsId !== null) {
                    $id = $user->ExerciseSettingsId;
                }

            } catch (Exception $e) {
                throw new Exception('User could not be found');
            }
        } else {
            $view->assign('heading', sprintf($translate->_('Exercise settings for %s'), $translate->_('Supervisor')) );
        }

        $form = new Supervisor_Form_Settings_Exercise();

        $view->assign('title', 'Supervisor');
        $view->form  = '';

        $request = $this->getRequest();
        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             *  Validate form
             */
            if ($form->isValid($request->getPost() ) ) {
                $values = $form->getValues();
                try {
                    $settingsMapper = new Application_Model_ExerciseSettingsMapper();
                    $settings = new Application_Model_ExerciseSettings($values);
                    if       (isset($user ) && $user->ExerciseSettingsId  === null) {
                        $user->ExerciseSettingsId = $settingsMapper->save($settings);
                        $userMapper->save($user);
                    } elseif (isset($group) && $group->ExerciseSettingsId === null) {
                        $group->ExerciseSettingsId = $settingsMapper->save($settings);
                        $groupMapper->save($group);
                    } else {
                        $settings->Id = $id;
                        $settingsMapper->save($settings);
                    }
                    $view->form .= Ahs_Utility::getBackofficeMessage('Success', 'Your changes have been saved', Ahs_Utility::MESSAGE_SUCCESS);
                } catch (PDOException $e) {
                    // Do nothing.
                }
            } else {
                $view->form .= Ahs_Utility::getBackofficeMessage('Errors', 'Please correct the errors below', Ahs_Utility::MESSAGE_ERROR);
            }
        } else {
            try {
                $settingsMapper = new Application_Model_ExerciseSettingsMapper();
                $settings = $settingsMapper->readRow($id);
                $form->populate($settings);
            } catch (PDOException $e) {
                // Do nothing.
            }
        }
        $view->form .= $form;
        $view->inlineScript()
             ->appendFile($view->placeholder('baseScripts') . 'Supervisor/Settings/exercise.js');
    }

    public function moduleAction()
    {
        $view = $this->_getView();
        $identity = $this->_auth->getIdentity();
        $supervisorMapper = new Application_Model_SupervisorMapper();
        $supervisor = $supervisorMapper->read($identity->SupervisorId);
        $id = $supervisor->ModuleSettingsId;

        $translate = Zend_Registry::get('Zend_Translate');

        $params = $this->getAllParams();
        if (isset($params['group']) ) {
            try {
                $groupMapper = new Application_Model_GroupMapper();
                $group = $groupMapper->readWhereSupervisor($supervisor, $params['group']);
                $view->assign('heading', sprintf($translate->_('Module settings for %s'), $group->Name === Application_Model_Group::DEFAULT_NAME ? $translate->_('Default group') : $group->Name) );

                if ($group->ModuleSettingsId !== null) {
                    $id = $group->ModuleSettingsId;
                }

            } catch (Exception $e) {
                throw new Exception('Group could not be found');
            }
        } elseif (isset($params['user'])) {
            try {
                $userMapper = new Application_Model_UserMapper();
                $user = $userMapper->readWhereSupervisor($supervisor, $params['user']);
                $view->assign('heading', sprintf($translate->_('Module settings for %s'), "{$user->Givenname} {$user->Familyname}") );

                if ($user->ModuleSettingsId !== null) {
                    $id = $user->ModuleSettingsId;
                }

            } catch (Exception $e) {
                throw new Exception('User could not be found');
            }
        } else {
            $view->assign('heading', sprintf($translate->_('Module settings for %s'), $translate->_('Supervisor')) );
        }

        $form = new Supervisor_Form_Settings_Module();

        $request = $this->getRequest();

        $view->assign('title', 'Supervisor');
        $view->form  = '';

        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             *  Validate form
             */
            if ($form->isValid($request->getPost() ) ) {
                $values = $form->getValues();
                try {
                    $settingsMapper = new Application_Model_ModuleSettingsMapper();
                    $settings = new Application_Model_ModuleSettings($values);
                    if       (isset($user ) && $user->ModuleSettingsId  === null) {
                        $user->ModuleSettingsId = $settingsMapper->save($settings);
                        $userMapper->save($user);
                    } elseif (isset($group) && $group->ModuleSettingsId === null) {
                        $group->ModuleSettingsId = $settingsMapper->save($settings);
                        $groupMapper->save($group);
                    } else {
                        $settings->Id = $id;
                        $settingsMapper->save($settings);
                    }
                    $view->form .= Ahs_Utility::getBackofficeMessage('Success', 'Your changes have been saved', Ahs_Utility::MESSAGE_SUCCESS);
                } catch (PDOException $e) {
                    // Do nothing.
                }
            } else {
                $view->form .= Ahs_Utility::getBackofficeMessage('Errors', 'Please correct the errors below', Ahs_Utility::MESSAGE_ERROR);
            }
        } else {
            try {
                $settingsMapper = new Application_Model_ModuleSettingsMapper();
                $settings = $settingsMapper->readRow($id);
                $form->populate($settings);
            } catch (Exception $e) {
//                // Do nothing.
            }
        }
        $view->form .= $form;
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $view = $this->view;

        return $view;
    }
}
