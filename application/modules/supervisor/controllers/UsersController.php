<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_UsersController extends Zend_Controller_Action
{
    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();
    }

    public function indexAction()
    {
        $this->redirect('/supervisor/users/list');
    }

    public function listAction()
    {
        if ($this->_auth->hasIdentity() ) {
            $identity = $this->_auth->getIdentity();

            $supervisorMapper = new Application_Model_SupervisorMapper();
            $supervisor = $supervisorMapper->read($identity->SupervisorId);

            $userMapper = new Application_Model_UserMapper();
            $users = $userMapper->readWhereSupervisor($supervisor);
//            Zend_Debug::dump($users); exit;

            $view = $this->_getView();
            $view
                ->assign('title', 'Supervisor')
                ->assign('users', $users      )
                ->inlineScript()
                    ->appendFile($view->placeholder('baseScripts') . 'Supervisor/Users/list.js')
            ;
        }
    }

    public function addAction()
    {
        $form = new Supervisor_Form_Users_Add();

        $view = $this->_getView();
        $view->assign('title', 'Supervisor');
        $view->assign('form' , $form);

        $request = $this->getRequest();
        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             *  Validate form
             */
            if ($form->isValid($request->getPost() ) ) {
                $identity = $this->_auth->getIdentity();

                $supervisorMapper = new Application_Model_SupervisorMapper();
                $supervisor = $supervisorMapper->read($identity->SupervisorId);

                $groupMapper = new Application_Model_GroupMapper();
                $group = $groupMapper->readDefaultWhereSupervisor($supervisor);

                $user = new Application_Model_User($form->getValues() );
                $user->GroupId = $group->Id;
                $user->Role    = ($supervisor->Role == Application_Model_Supervisor::ROLE_THERAPIST) ? Application_Model_User::ROLE_CLIENT : Application_Model_User::ROLE_PUPIL;

                $userMapper = new Application_Model_UserMapper();
                $userMapper->save($user);

//                Zend_Debug::dump($supervisor);
//                Zend_Debug::dump($group);
//                Zend_Debug::dump($user);
//                exit;

                $this->redirect('supervisor/users/list');
            }
        }
    }

    public function editAction()
    {
        // @todo Implement editAction()
    }

    public function deleteAction()
    {
        // @todo Implement deleteAction()
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $view = $this->view;

        return $view;
    }
}
