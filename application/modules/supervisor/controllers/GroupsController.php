<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_GroupsController extends Zend_Controller_Action
{
    const USER_ACTION_ADD    = 'add';
    const USER_ACTION_REMOVE = 'remove';

    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();
    }

    public function indexAction()
    {
        return $this->redirect('/supervisor/groups/list');
    }

    public function listAction()
    {
        if ($this->_auth->hasIdentity() ) {
            $view = $this->_getView();
            $view->assign('title', 'Supervisor')
                 ->inlineScript()
                 ->appendFile($view->placeholder('baseScripts') . 'Supervisor/Groups/list.js')
            ;
        }
    }

    public function addAction()
    {
        $form = new Supervisor_Form_Groups_Add();

        $view = $this->_getView();
        $view->assign('title', 'Supervisor')
             ->assign('form' , $form)
        ;

        $request = $this->getRequest();
        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             *  Validate form
             */
            if ($form->isValid($request->getPost() ) ) {
                $identity = $this->_auth->getIdentity();

                $supervisorMapper = new Application_Model_SupervisorMapper();
                $supervisor = $supervisorMapper->read($identity->SupervisorId);

                $group = new Application_Model_Group($form->getValues() );
                $group->SupervisorId = $supervisor->Id;

                $groupMapper = new Application_Model_GroupMapper();
                $groupMapper->save($group);

                $this->redirect('supervisor/groups/list');
            }
        }
    }

    public function editAction()
    {
        // @todo Implement editAction()
    }

    public function deleteAction()
    {
        // @todo Implement deleteAction()
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $view = $this->view;

        return $view;
    }
}
