<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Supervisor_AccountController extends Zend_Controller_Action
{
    /**
     * @var Zend_Auth
     */
    protected $_auth;

    public function init()
    {
        $this->_auth = Zend_Auth::getInstance();
    }

    public function indexAction()
    {
        if ($this->_auth->hasIdentity() ) {
            $identity = $this->_auth->getIdentity();
            if ($identity->isSupervisor() ) {
                return $this->redirect('supervisor/account/logout');
            }
        } else {
            return $this->redirect('supervisor/account/login');
        }
    }

    /**
     * Supervisor login
     */
    public function loginAction()
    {
        $form = new Supervisor_Form_Account_Login();

        $view = $this->_getView();
        $view->assign('title', 'Supervisor');

        $request = $this->getRequest();

        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             *  Validate form
             */
            if ($form->isValid( $request->getPost() ) ) {
                $values = $form->getValues();

                $supervisor = new Application_Model_Supervisor($values);

                $adapter = new Ahs_Auth_Adapter_Supervisor($supervisor->Username,
                                                           $supervisor->Password);

                $this->_auth->authenticate($adapter);

                if ($this->_auth->hasIdentity() ) {
                    // Authenticated
                    $data = new Ahs_Auth_Identity();
                    $data->Role = Ahs_Acl::ROLE_SUPERVISOR;
                    $data->Id = (int) $adapter->getResultRowObject()->sup_id;

                    $this->_auth->getStorage()->write($data);

                    return $this->redirect('supervisor');
                } else {
                    // Unable to authenticate
                    $view->form = Ahs_Utility::getBackofficeMessage('Login failed', 'Cannot login with the provided credentials', Ahs_Utility::MESSAGE_ERROR);
                }
            } else {
                $view->form = Ahs_Utility::getBackofficeMessage('Errors', 'Please correct the errors below', Ahs_Utility::MESSAGE_ERROR);
            }
        }
        $view->form .= $form;
    }

    /**
     * Supervisor logout
     */
    public function logoutAction()
    {
        $form = new Supervisor_Form_Account_Logout();

        $view = $this->_getView();
        $view->form = '';

        if ($this->_auth->hasIdentity() ) {
            $identity = $this->_auth->getIdentity();
            if ($identity->Role === Ahs_Acl::ROLE_SUPERVISOR) {
                $supervisorMapper = new Application_Model_SupervisorMapper();

                $supervisor = $supervisorMapper->read($identity->SupervisorId);

                $view->assign('name', "{$supervisor->Givenname} {$supervisor->Familyname}");
            }
        }
        $view->form  .= $form;

        $request = $this->getRequest();

        /**
         * If form is post
         */
        if ($request->isPost() ) {
            $this->_auth->clearIdentity();

            Ahs_Exercise_Settings::destroySession();

            $this->redirect('supervisor');
        }
    }

    /**
     * Supervisor registration page
     */
    public function registerAction()
    {
        $form = new Supervisor_Form_Account_Register();

        $view = $this->_getView();
        $view->assign('title', 'Supervisor');
        $view->assign('form', $form);

        $request = $this->getRequest();

        /**
         * If form is post
         */
        if ($request->isPost() ) {
            /**
             * Validate form
             */
            if ( !$form->isValid( $request->getPost() ) ) {
                $view->form = $form;
                $this->render('register'); // Render 'index' view script
            } else {
//                $db = Zend_Db_Table::getDefaultAdapter();
//                $db->beginTransaction();
//                try {

                    // Create supervisor
                    $values = $form->getValues();
                    $supervisor = new Application_Model_Supervisor($values);

                    // Create and save Exercise Settings;
                    $exerciseSettings = new Application_Model_ExerciseSettings();
                    $exerciseSettingsMapper = new Application_Model_ExerciseSettingsMapper();
                    $supervisor->ExerciseSettingsId = $exerciseSettingsMapper->save($exerciseSettings);

                    // Create and save Module Settings;
                    $moduleSettings = new Application_Model_ModuleSettings();
                    $moduleSettingsMapper = new Application_Model_ModuleSettingsMapper();
                    $supervisor->ModuleSettingsId = $moduleSettingsMapper->save($moduleSettings);

                    $supervisorMapper = new Application_Model_SupervisorMapper();
                    $supervisorMapper->save($supervisor);

                                        // Create and save 'Default' Group;
                    $group = new Application_Model_Group();
                    $group->SupervisorId = $supervisor->Id;
                    $groupMapper = new Application_Model_GroupMapper();
                    $groupMapper->save($group);

//                    Zend_Debug::dump($exerciseSettings);
//                    Zend_Debug::dump($moduleSettings);
//                    Zend_Debug::dump($group);
//                    Zend_Debug::dump($supervisor);
//                    exit;

//                    $db->commit();
//                } catch (Zend_Db_Exception $e) {
//                    $db->rollBack();
//                    echo 'rollback';
//                    echo $e->getMessage();
//                } catch (Exception $e) {
//                    echo $e->getMessage();
//                }

                $this->redirect('supervisor/account');
            }
        }
    }

    /**
     * @return Zend_View
     */
    protected function _getView()
    {
        $view = $this->view;

        return $view;
    }
}
