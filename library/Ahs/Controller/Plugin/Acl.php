<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * Access Controll List plugin for Front Controller.
 *
 * Add this to application.ini:
 * resources.frontController.plugins.acl = "Ahs_Controller_Plugin_Acl"
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    const SESSION_NAMESPACE = 'Acl';

    /**
     * @var Zend_Acl
     */
    protected $_acl;

    public function __construct()
    {
        $session = new Ahs_Session(self::SESSION_NAMESPACE);

        if ( isset($session->acl) ) {
            $this->_acl = $session->acl;
        } else {
            $session->acl = $this->_acl = new Ahs_Acl();
        }
    }

    /**
     * @param Zend_Controller_Request_Abstract $request
     * @return boolean
     * @throws Zend_Exception
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        parent::preDispatch($request);

        $request = $this->getRequest();

        $auth = Zend_Auth::getInstance();

        $role = $auth->hasIdentity() ? $auth->getIdentity()->Role : Ahs_Acl::ROLE_GUEST;
        $resource  = Ahs_Acl::getResource( $request->getControllerName(), $request->getModuleName());
        $privilege = Ahs_Acl::getPrivilege($request->getActionName()                               );

        $acl = $this->_acl;
        if ( $acl->isAllowed($role, $resource, $privilege) ) {
            return true;
        }

        throw new Zend_Exception("Access violation for Role '{$role}': no access to Resource '{$resource}' for Privilege '{$privilege}'");
    }
}
