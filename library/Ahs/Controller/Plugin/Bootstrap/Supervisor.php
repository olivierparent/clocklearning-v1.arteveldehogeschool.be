<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * Front Controller plugin to bootstrap the Supervisor module.
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Controller_Plugin_Bootstrap_Supervisor extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() != 'supervisor') {
            // If not in this module, return early
            return;
        }

        Zend_Layout::getMvcInstance()->setLayout('backoffice');

        $viewRenderer = Zend_Controller_Action_HelperBroker::getExistingHelper('ViewRenderer');
        $viewRenderer->initView();
        $view = $viewRenderer->view;

        // Head Links
        $view
            ->headLink()
            ->exchangeArray(array()); // Reset settings from Bootstrap.php
        $view
            ->headLink()
            ->headLink(array('rel'  => 'shortcut icon',
                             'type' => 'image/x-icon',
                             'href' => $view->placeholder('baseImages') . 'favicon.ico'), 'PREPEND')
            ->appendStylesheet('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/css/bootstrap.min.css')
            ->headLink(array('media' => 'screen',
                             'rel'   => 'stylesheet/less',
                             'type'  => 'text/css',
                             'href'  => $view->placeholder('baseStyles') . 'Supervisor/main.less'))
        ;

        // Head Scripts
        $view
            ->headScript()
            ->exchangeArray(array()); // Reset settings from Bootstrap.php
//        $view
//            ->headScript()
//        ;

        // Inline Scripts
        $view
            ->inlineScript()
            ->exchangeArray(array()); // Reset settings from Bootstrap.php
        $view
            ->inlineScript()
            ->appendFile('//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.11/require.min.js')
            ->appendFile($view->placeholder('baseScripts') . 'main.js')
            ->appendFile($view->placeholder('baseScripts') . 'Supervisor/main.js')
        ;

        // @todo Should be in cache
        $session = new Ahs_Session('pages');
        if (isset($session->pages)) {
            $pages = $session->pages;
        } else {
            $yaml = APPLICATION_PATH . '/configs/navigation/supervisor.yml';
            $session->pages = $pages = new Zend_Config_Yaml($yaml);
        }
        $navigation = new Zend_Navigation($pages);

        $auth = Zend_Auth::getInstance();
        $role = $auth->hasIdentity() ? $auth->getIdentity()->Role : Ahs_Acl::ROLE_GUEST;

        $view
            ->navigation($navigation)
            ->setAcl(new Ahs_Acl())
            ->setRole($role)
        ;
        $view
            ->navigation()
                ->breadcrumbs()
                ->setPartial('partials/breadcrumbs.phtml')
        ;
        $view
            ->navigation()
            ->menu()
            ->setRenderParents(true)
            ->setPartial('partials/menu.phtml')
            ->setOnlyActiveBranch(true)
            ->setUlClass('nav navbar-nav')
        ;
    }
}
