<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Exercise_Audio_Abstract
{
    const FILE          = '__FILE__';
    const FILES         = '__FILES__';
    const TRANSCRIPTION = '__TRANSCRIPTION__';

    protected static $_baseUrl = '';

    /**
     * @param string $key
     */
    public static function echoFile($key = null, $extension = 'mp3')
    {
        echo self::getFile($key) . ".{$extension}";
    }

    /**
     * @param string $key
     * @return string
     * @throws Exception
     */
    public static function getFile($key = null)
    {
        if (!empty($key) && isset(static::$_files[$key]) ) {
            return self::$_baseUrl . static::LANG . '/' . static::$_files[$key];
        }
        throw new Exception("Audio for '{$key}' does not exist.");
    }



    /**
     * Get audio file and transcription for a number.
     *
     * @param string $number
     * @return array
     * @throws Exception
     */
    public static function getNumber($number = null)
    {
        if ($number == 0) {
            $number = 12;
        }
        if (!empty($number) && 0 < $number && $number <= 60) {
            $file = sprintf('numbers_1-60/number_%02d', $number);

            return self::_getAudio($file, "$number");
        }
        throw new Exception("Audio for '{$number}' does not exist.");
    }

    /**
     * Get audio file and transcription for a (partial) phrase.
     *
     * @param string $key
     * @return array
     * @throws Exception
     */
    public static function getPhrase($key = null)
    {
        if (!empty($key) && isset(static::$_files[$key]) ) {
            return self::_getAudio(static::$_files[$key], $key);
        }
        throw new Exception("Audio for '{$key}' does not exist.");
    }

    /**
     * Get audio files and transcription for a phrase.
     *
     * @param string $key
     * @return array
     * @throws Exception
     */
    public static function getPhraseComposition()
    {
        $args = func_get_args();
        $audio = array(
            self::TRANSCRIPTION => array(),
            self::FILES         => array(),
        );

        foreach ($args as $arg) {
            $phrase = (is_int($arg)) ? self::getNumber($arg) : self::getPhrase($arg);
            $audio[self::TRANSCRIPTION][] = $phrase[self::TRANSCRIPTION];
            $audio[self::FILES][]         = $phrase[self::FILE];
        }
        $audio[self::TRANSCRIPTION] = implode(' ', $audio[self::TRANSCRIPTION]);

        return $audio;
    }

    /**
     * @param string $url
     */
    public static function setBaseUrl($url)
    {
        self::$_baseUrl = $url;
    }

    /**
     * @param string $file
     * @param string $transcription
     * @return array
     */
    protected static function _getAudio($file, $transcription)
    {
        return array(
            self::TRANSCRIPTION => $transcription,
            self::FILE          => $file,
        );
    }

}
