<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Audio_NL extends Ahs_Exercise_Audio_Abstract
{
    const LANG = 'nl';

    /**
     * @todo Complete the list of translations.
     *
     * @var array
     * @static
     */
    protected static $_files = array(
        'Dat is fout!'
            => 'negative_a/dat_is_fout',

        'Dat is niet goed!'
            => 'negative_a/dat_is_niet_goed',

        'Jammer.'
            => 'negative_a/jammer',

        'Deze staat op'
            => 'modelling/deze_staat_op_[c]',

        'Goed gedaan!'
            => 'positive_a/goed_gedaan',

        'Goed gewerkt!'
            => 'positive_a/goed_gewerkt',

        'Het is dus'
            => 'modelling/[x]_het_is_dus_[x]',

        'Hij staat bovenaan op het uur.'
            => 'modelling/hij_staat_bovenaan_op_het_uur',

        'Hoe laat is het al geweest?'
            => 'help/analogue/hoe_laat_is_het_al_geweest',

        'Hoe laat moet het nog worden?'
            => 'help/analogue/hoe_laat_moet_het_nog_worden',

        'Hoe laat wordt het?'
            => 'help/analogue/hoe_laat_wordt_het',

        'Hoeveel minuten nog tot half?'
            => 'help/analogue/hoeveel_minuten_nog_tot_half',

        'Hoeveel minuten nog tot het uur?'
            => 'help/analogue/hoeveel_minuten_nog_tot_het_uur',

        'Hoeveel minuten zijn er al voorbij?'
            => 'help/analogue/hoeveel_minuten_zijn_er_al_voorbij',

        'Is de wijzer al voorbij half?'
            => 'help/analogue/is_de_wijzer_al_voorbij_half',

        'Is de wijzer al voorbij het uur?'
            => 'help/analogue/is_de_wijzer_al_voorbij_het_uur',

        'Is de wijzer al voorbij het uur of niet?'
            => 'help/analogue/is_de_wijzer_al_voorbij_het_uur_of_niet',

        'Kijk eens goed waar de <b class="minutes">grote wijzer</b> staat.'
            => 'help/analogue/kijk_eens_goed_waar_de_grote_wijzer_staat',

        'Op naar de volgende klok!'
            => 'positive_b/op_naar_de_volgende_klok',

        'Probeer het eens met hulp!'
            => 'help/probeer_het_eens_met_hulp',

        'Probeer het nog eens!'
            => '2nd_chance_a/probeer_het_nog_eens',

        'uur.'
            => 'modelling/[c]_uur',

        'Waar staat de <b class="hours">kleine wijzer</b>?'
            => 'help/analogue/waar_staat_de_kleine_wijzer',

        'Wat wil dit zeggen?'
            => 'help/analogue/wat_wil_dit_zeggen',

        'We gaan naar de volgende klok.'
            => 'positive_b/we_gaan_naar_de_volgende_klok',
    );
}
