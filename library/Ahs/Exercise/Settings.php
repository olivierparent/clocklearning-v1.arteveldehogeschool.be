<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Settings
{
    const SESSION_NAMESPACE = 'Settings';

    /**
     * @param Application_Model_PersonAbstract $person
     * @return Application_Model_ExerciseSettings
     */
    public static function getSessionExerciseSettings(Application_Model_PersonAbstract $person = null)
    {
        $session = new Ahs_Session(self::SESSION_NAMESPACE);
// @todo Enble sessions.
//        if (!isset($session->exerciseSettings) ) {
            self::setSessionExerciseSettings($person);
//        }

//        Zend_Debug::dump($session->exerciseSettings); exit;
        return $session->exerciseSettings;
    }

    /**
     * @param Application_Model_PersonAbstract $person
     * @return Application_Model_ModuleSettings
     */
    public static function getSessionModuleSettings(Application_Model_PersonAbstract $person = null)
    {
        $session = new Ahs_Session(self::SESSION_NAMESPACE);
// @todo Enble sessions.
//        if (!isset($session->exerciseSettings) ) {
            self::setSessionModuleSettings($person);
//        }

        return $session->moduleSettings;
    }

    /**
     * @param Application_Model_PersonAbstract $person
     */
    public static function setSessionSettings(Application_Model_PersonAbstract $person = null)
    {
        self::setSessionExerciseSettings($person);
        self::setSessionModuleSettings($person);
    }

    /**
     * @param Application_Model_PersonAbstract $person
     * @return Application_Model_ExerciseSettings
     */
    public static function setSessionExerciseSettings(Application_Model_PersonAbstract $person = null)
    {
        $session = new Ahs_Session(self::SESSION_NAMESPACE);

        /**
         * 1. Role has ExerciseSettings?
         *    YES: use Role ExerciseSettings.
         *    NO : continue.
         * 2. Role is User?
         *    YES: continue.
         *    NO : use default ExerciseSettings
         * 3. User has Group?
         *    YES: continue.
         *    NO : goto 5.
         * 4. Group has ExerciseSettings?
         *    YES: use Group ExerciseSettings.
         *    NO : continue.
         * 5. Supervisor has ExerciseSettings?
         *    YES: use Supervisor ExerciseSettings.
         *    NO : default ExerciseSettings.
         */
//        Zend_Debug::dump($person); exit;
        if ($person) {
            if ($person instanceof Application_Model_PersonAbstract && !$person instanceof Application_Model_Admin) {
                if ($person->ModuleSettingsId) {
                    $exerciseSettingsMapper = new Application_Model_ExerciseSettingsMapper();

                    return $session->exerciseSettings = $exerciseSettingsMapper->read($person->ExerciseSettingsId);
                } elseif ($person instanceof Application_Model_User) {
                    $groupMapper = new Application_Model_GroupMapper();
                    $group = $groupMapper->read($person->GroupId);

                    $exerciseSettingsMapper = new Application_Model_ExerciseSettingsMapper();
                    if ($group->ExerciseSettingsId) {

                        return $session->exerciseSettings = $exerciseSettingsMapper->read($group->ExerciseSettingsId);
                    } else {
                        $supervisorMapper = new Application_Model_SupervisorMapper();
                        $supervisor = $supervisorMapper->read($group->SupervisorId);

                        return $session->exerciseSettings = $exerciseSettingsMapper->read($supervisor->ExerciseSettingsId);
                    }
                }
            }
        }

        return $session->exerciseSettings = new Application_Model_ExerciseSettings();
    }

    /**
     * @param Application_Model_PersonAbstract $person
     * @return Application_Model_ModuleSettings
     */
    public static function setSessionModuleSettings(Application_Model_PersonAbstract $person = null)
    {
        $session = new Ahs_Session(self::SESSION_NAMESPACE);

        /**
         * 1. Role has ModuleSettings?
         *    YES: use Role ModuleSettings.
         *    NO : continue.
         * 2. Role is User?
         *    YES: continue.
         *    NO : use default ModuleSettings
         * 3. User has Group?
         *    YES: continue.
         *    NO : goto 5.
         * 4. Group has ModuleSettings?
         *    YES: use Group ModuleSettings.
         *    NO : continue.
         * 5. Supervisor has ModuleSettings?
         *    YES: use Supervisor ModuleSettings.
         *    NO : default ModuleSettings.
         */
        if ($person) {
            if ($person instanceof Application_Model_PersonAbstract && !$person instanceof Application_Model_Admin) {
                if ($person->ModuleSettingsId) {
                    $moduleSettingsMapper = new Application_Model_ModuleSettingsMapper();

                    return $session->moduleSettings = $moduleSettingsMapper->read($person->ModuleSettingsId);
                } elseif ($person instanceof Application_Model_User) {
                    $groupMapper = new Application_Model_GroupMapper();
                    $group = $groupMapper->read($person->GroupId);

                    $moduleSettingsMapper = new Application_Model_ModuleSettingsMapper();
                    if ($group->ModuleSettingsId) {

                        return $session->moduleSettings = $moduleSettingsMapper->read($group->ModuleSettingsId);
                    } else {
                        $supervisorMapper = new Application_Model_SupervisorMapper();
                        $supervisor = $supervisorMapper->read($group->SupervisorId);

                        return $session->moduleSettings = $moduleSettingsMapper->read($supervisor->ModuleSettingsId);
                    }
                }
            }
        }

        return $session->moduleSettings = new Application_Model_ModuleSettings();
    }

    public static function destroySession() {
        $session = new Ahs_Session(self::SESSION_NAMESPACE);
        $session->unsetAll();
    }
}
