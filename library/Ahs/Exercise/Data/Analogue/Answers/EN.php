<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * NL analogue clock: Generate 4 likely answers
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Analogue_Answers_EN extends Ahs_Exercise_Data_Analogue_Answers_Abstract
{
    /**
     * Generate 4 times of which 3 are likely wrong answers.
     *
     * @param int $hours
     * @param int $minutes
     */
    public function __construct($hours = null, $minutes = null)
    {
        $this->Time = new Ahs_Time_EN($hours, $minutes);
        $this->Time->setMode(Ahs_Time_Abstract::CORRECT_ANALOGUE);

        switch ($this->Time->Minutes) {
            case  0:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_HOURS_HOUR_EARLIER)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_HOURS_HOUR_LATER)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_H_00_12_M_00)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_HOURS_MIRROR_Z)
                ;
                break;
            // Quarter past
            case 15:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_MIRROR_Y)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_READ_AS_FULL_HOURS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_H_03_15_M_15)
                ;
                break;
            // Half past
            case 30:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_HOURS_HALF_HOUR_LATER)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_HOURS_HOUR_EARLIER)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_H_05_17_M_30)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_H_00_12_M_00)
                ;
                break;
            // Quarter to
            case 45:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_MIRROR_Y)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_READ_AS_FULL_HOURS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_H_09_21_M_45)
                ;
                break;
            // Tens
            case 10:
            case 20:
            case 40:
            case 50:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_MIRROR_Y)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_FULL_HOURS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS_WITH_PAST_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS_NEAREST_HOUR)
                ;
                break;
            // Fives
            case  5:
            case 55:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_MIRROR_Y)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_FULL_HOURS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS)
                ;
                break;
            case 25:
            case 35:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_MIRROR_Y)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_05M_DIFFERENCE)
                ;
                break;
            default:
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_01M_DIFFERENCE)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_MINUTES_READ_AS_ADDITION_TO_HOURS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_ANALOGUE_SWITCH_HANDS_NEAREST_HOUR)
                ;
                break;
        }

        $this->_generateWrongAnswers();
    }

    /**
     * Convert to string.
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $questions = array();
            $answers   = array();
            $modes     = array();
            $solution  = array();

            $questions[] = $this->Time->toTime();

            $times = array_merge(array($this->Time), $this->TimeWrong);
            shuffle($times);

            foreach ($times as $index => $time) {
                $answers[] = $time->toText(Ahs_Time_Abstract::TEXT_ANALOGUE);
                $modes[]   = $time->Mode;
                if ($time->Mode === $this->Time->Mode) {
                    $solution[] = $index;
                }
            }

            $feedback = new Ahs_Exercise_Feedback_Analogue_EN($this->Time);

            $response = new Ahs_Exercise_Data_Response($this);
            return $response
                ->setQuestions($questions)
                ->setAnswers($answers)
                ->setModes($modes)
                ->setSolution($solution)
                ->setFeedback($feedback)
                ->toJSON();
        } catch (Exception $e) {
            return "$e";
        }
    }

    /**
     * @param string $possibleError
     * @return $this
     */
    protected function _pushPossibleError($possibleError)
    {
        array_push($this->_possibleErrors, $possibleError);

        return $this;
    }

    /**
     * @return string
     */
    protected function _shiftPossibleError()
    {
        return array_shift($this->_possibleErrors);
    }

    /**
     * @param Ahs_Time_Abstract $time
     * @return bool
     */
    protected function _isUniquePossibleError($time)
    {
        if ($this->_isEqualTime($time, $this->Time) ) {
            return false;
        } else {
            foreach($this->_timeWrong as $timeWrong) {
                if ($this->_isEqualTime($time, $timeWrong) ) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param Ahs_Time_Abstract $timeA
     * @param Ahs_Time_Abstract $timeB
     * @return bool
     */
    protected function _isEqualTime(Ahs_Time_Abstract $timeA, Ahs_Time_Abstract $timeB)
    {
        return $timeA->Hours   === $timeB->Hours &&
        $timeA->Minutes === $timeB->Minutes;
    }

    /**
     * Generate likely wrong answers for a given time.
     *
     * @throws ErrorException
     */
    protected function _generateWrongAnswers()
    {
        while (count($this->TimeWrong) < 3) {
            $time = clone $this->Time;

            $possibleError = $this->_shiftPossibleError();

            if ($possibleError) {
                if ($time->{$possibleError}() ) {
                    if ($this->_isUniquePossibleError($time)) {
                        $this->_timeWrong[] = $time;
                    }
                }
            } else {
//                echo "Ran out of options for {$time}";
//                var_dump(array_merge(array($this->Time), $this->TimeWrong));
//                exit;

                throw new ErrorException("Ran out of options for {$time}");
            }
        }
    }

}
