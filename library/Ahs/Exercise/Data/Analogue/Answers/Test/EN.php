<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Analogue_Answers_Test_EN extends Ahs_Exercise_Data_Analogue_Answers_EN
{
    static protected $_i = 0;

    public function __toString()
    {
        $translate = Zend_Registry::get('Zend_Translate');

        return '<tr>' . PHP_EOL
             . '<th style="text-align:right">' . ++self::$_i . '.</th>' . PHP_EOL
             . '<td><span style="color: lightgray">(A)</span> <b style="color: #0C0" title="' . $translate->_($this->Time        ->Mode) . '">' . $this->Time         . '</b></td><td>'  . $this->Time        ->toText(Ahs_Time_Abstract::TEXT_ANALOGUE) . '</td>' . PHP_EOL
             . '<td><span style="color: lightgray">(B)</span> <b style="color: #F00" title="' . $translate->_($this->TimeWrong[0]->Mode) . '">' . $this->TimeWrong[0] . '</b></td><td>'  . $this->TimeWrong[0]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE) . '</td>' . PHP_EOL
             . '<td><span style="color: lightgray">(C)</span> <b style="color: #F00" title="' . $translate->_($this->TimeWrong[1]->Mode) . '">' . $this->TimeWrong[1] . '</b></td><td>'  . $this->TimeWrong[1]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE) . '</td>' . PHP_EOL
             . '<td><span style="color: lightgray">(D)</span> <b style="color: #F00" title="' . $translate->_($this->TimeWrong[2]->Mode) . '">' . $this->TimeWrong[2] . '</b></td><td>'  . $this->TimeWrong[2]->toText(Ahs_Time_Abstract::TEXT_ANALOGUE) . '</td>' . PHP_EOL
             . '<td><a href="clocksanalogue/h/' . $this->Time->Hours . '/m/' . $this->Time->Minutes . '" style="color: lightgray">' . $translate->_('Clocks for') . ' '. $this->Time . '</a></td>' . PHP_EOL
             . '</tr>' . PHP_EOL;
    }
}
