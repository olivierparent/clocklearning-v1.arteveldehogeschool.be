<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Analogue_Answers_Digital_NL extends Ahs_Exercise_Data_Analogue_Answers_NL
{
    /**
     * toString
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $questions = array();
            $answers   = array();
            $modes     = array();
            $solution  = array();

            $questions[] = $this->Time->toTime();

            $times = array_merge(array($this->Time), $this->TimeWrong);
            shuffle($times);

            foreach ($times as $index => $time) {
                $answers[] = $time->toTime();
                $modes[]   = $time->Mode;
                if ($time->Mode === $this->Time->Mode) {
                    $solution[] = $index;
                }
            }

            $feedback = new Ahs_Exercise_Feedback_Analogue_NL($this->Time);

            $response = new Ahs_Exercise_Data_Response($this);
            return $response
                ->setQuestions($questions)
                ->setAnswers($answers)
                ->setModes($modes)
                ->setSolution($solution)
                ->setFeedback($feedback)
                ->toJSON();
        } catch (Exception $e) {
            return "$e";
        }
    }
}
