<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * NL digital clock: Generate 4 likely answers
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Digital_Answers_EN extends Ahs_Exercise_Data_Digital_Answers_Abstract
{
    /**
     * @var Ahs_Time_EN
     */
    protected $_time;

    /**
     * @var array Ahs_Time_EN
     */
    protected $_timeWrong = array();

    /**
     * Array of possible errors.
     *
     * @var array
     */
    protected $_possibleErrors = array();

    /**
     * Generate 4 times of which 3 are likely wrong answers.
     *
     * @param int $hours
     * @param int $minutes
     */
    public function __construct($hours = null, $minutes = null)
    {
        $this->Time = new Ahs_Time_EN($hours, $minutes);

        switch ($this->Time->Minutes) {
            case  0:
                $this->Time->setMode(Ahs_Time_Abstract::CORRECT_DIGITAL);
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_M00_AS_0)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_H×10)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_H×100)
                ;
                break;
            case 15: // Quarter past
                $this->Time->setMode(Ahs_Time_Abstract::CORRECT_DIGITAL_READ_ANALOGUE); // For added difficulty.
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_QUARTERS_AS_NUMBER)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_SWITCH_PAST_AND_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_SWITCH_PAST_AND_TO_THEN_READ_QUARTERS_AS_NUMBER)
                ;
                break;
            case 30: // Half past
                $this->Time->setMode(Ahs_Time_Abstract::CORRECT_DIGITAL_READ_ANALOGUE); // For added difficulty.
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_EARLIER_BY_30M)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_REVERSE_M_DIGITS)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_SWITCH_H_AND_M)
                ;
                break;
            case 45: // Quarter to
                $this->Time->setMode(Ahs_Time_Abstract::CORRECT_DIGITAL_READ_ANALOGUE); // For added difficulty.
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_EARLIER_BY_30M)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_EARLIER_BY_60M)
                ;
                break;
            default:
                $this->Time->setMode(Ahs_Time_Abstract::CORRECT_DIGITAL_READ_ANALOGUE_PAST); // For added difficulty.
                $this
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_HOUR)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_TO)
                    ->_pushPossibleError(Ahs_Time_Abstract::ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_PAST)
                ;
                break;
        }
        $this->_generateWrongAnswers();
    }

    /**
     * Convert to string.
     *
     * @return string
     */
    public function __toString()
    {
        try {
            $questions = array();
            $answers   = array();
            $modes     = array();
            $solution  = array();

            $questions[] = $this->Time->toTime();

            $times = array_merge(array($this->Time), $this->TimeWrong);
            shuffle($times);

            foreach ($times as $index => $time) {
                switch ($time->Mode) {
                    case Ahs_Time_Abstract::CORRECT_DIGITAL_READ_ANALOGUE:
                    case Ahs_Time_Abstract::ERROR_DIGITAL_EARLIER_BY_30M:
                    case Ahs_Time_Abstract::ERROR_DIGITAL_EARLIER_BY_60M:
                    case Ahs_Time_Abstract::ERROR_DIGITAL_SWITCH_PAST_AND_TO:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_ANALOGUE);
                        break;
                    case Ahs_Time_Abstract::CORRECT_DIGITAL_READ_ANALOGUE_PAST;
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_ANALOGUE_PAST);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_H×10:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL_H×10);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_H×100:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL_H×100);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_M00_AS_0:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL_M00_AS_0);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_REVERSE_M_DIGITS:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL_REVERSE_M_DIGITS);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_QUARTERS_AS_NUMBER:
                    case Ahs_Time_Abstract::ERROR_DIGITAL_SWITCH_PAST_AND_TO_THEN_READ_QUARTERS_AS_NUMBER:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_ANALOGUE_QUARTERS_AS_NUMBER);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_SWITCH_H_AND_M;
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL_SWITCH_H_AND_M);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_PAST;
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_ANALOGUE_SWITCH_H_AND_M_THEN_PAST);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS;
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS);
                        break;
                    case Ahs_Time_Abstract::ERROR_DIGITAL_READ_TO;
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_ANALOGUE_TO);
                        break;
                    default:
                        $answers[] = $time->toText(Ahs_Time_EN::TEXT_DIGITAL);
                        break;
                }
                $modes[] = $time->Mode;
                if ($time->Mode === $this->Time->Mode) {
                    $solution[] = $index;
                }
            }

            // @todo Change to Ahs_Exercise_Feedback_Digital_EN
            $feedback = new Ahs_Exercise_Feedback_Analogue_EN($this->Time);

            $response = new Ahs_Exercise_Data_Response($this);
            return $response
                ->setQuestions($questions)
                ->setAnswers($answers)
                ->setModes($modes)
                ->setSolution($solution)
                ->setFeedback($feedback)
                ->toJSON();
        } catch (Exception $e) {
            return "$e";
        }
    }

    /**
     * @param Ahs_Time_Abstract $time
     * @return bool
     */
    protected function _isUniquePossibleError($time)
    {
        if ($time->Mode === Ahs_Time_Abstract::ERROR_DIGITAL_READ_REVERSE_M_DIGITS) {
            if ($time->Hours === 3) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $possibleError
     * @return $this
     */
    protected function _pushPossibleError($possibleError)
    {
        array_push($this->_possibleErrors, $possibleError);

        return $this;
    }

    /**
     * @return string
     */
    protected function _shiftPossibleError()
    {
        return array_shift($this->_possibleErrors);
    }

    /**
     * Generate likely wrong answers for a given time.
     *
     * @throws ErrorException
     */
    protected function _generateWrongAnswers()
    {
        while (count($this->TimeWrong) < 3) {
            $time = clone $this->Time;

            $possibleError = $this->_shiftPossibleError();

            if ($possibleError) {
                if ($time->{$possibleError}() ) {
                    if ($this->_isUniquePossibleError($time)) {
                        $time->Mode = $possibleError;
                        $this->_timeWrong[] = $time;
                    }
                }
            } else {
                throw new ErrorException("Ran out of options for {$time}");
            }
        }
    }

}
