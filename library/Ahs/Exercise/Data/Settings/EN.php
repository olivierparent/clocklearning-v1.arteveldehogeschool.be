<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Settings_EN extends Ahs_Exercise_Data_Settings_Abstract
{
	/**
	 * @var Ahs_Time_EN
	 */
	protected $_time;

	public function __construct()
	{
        Ahs_Time_EN::uncheckTwentyFour();
		$h = Ahs_Time_EN::getHoursAvailable();
		shuffle($h);

        $m = Ahs_Time_EN::getMinutesAvailable();
        shuffle($m);

        $this->Time = new Ahs_Time_EN($h[0], $m[0]);
	}

	/**
	 * Convert to string.
	 *
	 * @return string
	 */
	public function __toString()
	{
        try {
            $questions = array();
            $answers   = array();
            $modes     = array();
            $solution  = array();

            $questions[] = $this->Time->toText(Ahs_Time_Abstract::TEXT_ANALOGUE);
            $answers[]   = $this->Time->toTime();
            $modes[]     = $this->Time->Mode;
            $solution[]  = array_search($this->Time->toTime(), $answers);

            $feedback = new Ahs_Exercise_Feedback_Analogue_EN($this->Time);

            $response = new Ahs_Exercise_Data_Response($this);
            return $response
                ->setQuestions($questions)
                ->setAnswers($answers)
                ->setModes($modes)
                ->setSolution($solution)
                ->setFeedback($feedback)
                ->toJSON();
        } catch (Exception $e) {
            return "$e";
        }
	}
}
