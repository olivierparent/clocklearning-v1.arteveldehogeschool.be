<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Combinations_NL extends Ahs_Exercise_Data_Combinations_Abstract
{
	/**
     *
	 */
	public function __construct()
	{
		$h = Ahs_Time_NL::getHoursAvailable();
		shuffle($h);

        $m = Ahs_Time_NL::getMinutesAvailable();
        shuffle($m);

        for ($i = 0; $i < 4; $i++) {
            $this->_time[$i] = new Ahs_Time_NL($h[$i], $m[$i]);
        }
	}

	/**
	 * Convert to string.
	 *
	 * @return string
	 */
	public function __toString()
	{
        try {
            $questions = array();
            $answers   = array();
            $solution  = array();

            foreach ($this->_time as $time) {
                $questions[] = $time->toText(Ahs_Time_Abstract::TEXT_ANALOGUE);
                $answers[]   = $time->toTime();
            }

            shuffle($answers);

            foreach ($this->_time as $time) {
                $solution[] = array_search($time->toTime(), $answers);
            }

//            $feedback = new Ahs_Exercise_Feedback_Analogue_NL($this->Time1);

            $response = new Ahs_Exercise_Data_Response($this);
            return $response
                ->setQuestions($questions)
                ->setAnswers($answers)
                ->setSolution($solution)
                ->toJSON();
        } catch (Exception $e) {
            return "$e";
        }
	}
}
