<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Data_Response extends Ahs_Object_Abstract
{
    protected $_class     = null;

    protected $_questions = array();

    protected $_answers   = array();

    protected $_modes     = array();

    protected $_solution  = array();

    protected $_feedback  = null;

    public function __construct($object)
    {
        $this->_class = get_class($object);
    }

    public function setQuestions($questions)
    {
        $this->_questions = $questions;

        return $this;
    }

    public function setAnswers($answers)
    {
        $this->_answers = $answers;

        return $this;
    }

    public function setModes($modes)
    {
        $this->_modes = $modes;

        return $this;
    }

    public function setSolution($solution)
    {
        $this->_solution = $solution;

        return $this;
    }

    public function setFeedback($feedback)
    {
        $this->_feedback = $feedback;

        return $this;
    }

    public function toJSON()
    {
        $exercise = array(
            'data' => array(
                'type'      => $this->_class,
                'questions' => $this->_questions,
                'answers'   => $this->_answers,
                'modes'     => $this->_modes,
                'solution'  => $this->_solution,
                'attempts'  => array(),
                'correct'   => null,
                'timeStart' => 0,
                'timeEnd'   => 0,
            ),
        );

        if ($this->_feedback !== null) {
            $exercise['feedback'] = $this->_feedback->toArray();
        }

        return json_encode($exercise);
    }
}
