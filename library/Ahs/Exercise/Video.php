<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Exercise_Video
{
    protected static $_baseUrl = '';

    /**
     * @todo Complete the list of videos.
     *
     * @var array
     * @static
     */
    protected static $_files = array(
        'awaking'
            => 'rika-tika/awaking',
        'dance'
            => 'rika-tika/dance',
        'jump'
            => 'rika-tika/jump',
        'placeholder[1]'
            => '_film1',
        'placeholder[2]'
            => '_film2',
        'placeholder[3]'
            => '_film3',
        'placeholder[4]'
            => '_film4',
        'sad[1]'
            => 'rika-tika/sad_01',
        'sad[2]'
            => 'rika-tika/sad_02',
    );

    /**
     * @param string|null $key
     * @param string $extension
     */
    public static function echoFile($key = null, $extension = 'mp4')
    {
        echo self::getFile($key) . ".{$extension}";
    }

    /**
     * @param string|null $key
     * @return string
     * @throws Exception
     */
    public static function getFile($key = null)
    {
        if (!empty($key) && isset(self::$_files[$key]) ) {
            return self::$_baseUrl . self::$_files[$key];
        }
        throw new Exception("Video for '{$key}' does not exist.");
    }

    /**
     * @param string $url
     */
    public static function setBaseUrl($url)
    {
        self::$_baseUrl = $url;
    }

}
