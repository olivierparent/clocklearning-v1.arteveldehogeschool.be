<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Feedback_Abstract extends Ahs_Object_Abstract
{

    /**
     * @var array
     */
    protected $_verbalVisual = array();
    /**
     * @var array
     */
    protected $_modelling    = array();

    protected $_index = null;

    /**
     * @return array
     */
    public function toArray()
    {
        $feedback = array();

        if (count($this->VerbalVisual) ) {
            $feedback['verbalVisual'] = $this->VerbalVisual;
        }

        if (count($this->Modelling) ) {
            $feedback['modelling']    = $this->Modelling;
        }

        return $feedback;
    }

    protected function _generateFeedback()
    {
        /**
         * @todo Make dependent on settings from database
         */
        $this->_generateFeedbackVerbalVisual();
        $this->_generateFeedbackModelling();
    }

    /**
     * @param array $item
     */
    protected function _addItemModelling($item)
    {
        $this->Index = null;
        $this->_modelling[] = array($item);
    }

    /**
     * @param array $item
     */
    protected function _addPartialItemModelling($item)
    {
        if ($this->Index === null) {
            $this->Index = count($this->_modelling);
            $this->_modelling[$this->Index] = array();
            $this->_modelling[$this->Index][] = $item;
        } else {
            $this->_modelling[$this->Index][] = $item;
        }
    }

    /**
     * @param array $item
     */
    protected function _addItemVerbalVisual($item)
    {
        $this->Index = null;
        $this->_verbalVisual[] = array($item);
    }

    /**
     * @param array $item
     */
    protected function _addPartialItemVerbalVisual($item)
    {
        if ($this->Index === null) {
            $this->Index = count($this->_verbalVisual);
            $this->_verbalVisual[$this->Index] = array();
            $this->_verbalVisual[$this->Index][] = $item;
        } else {
            $this->_verbalVisual[$this->Index][] = $item;
        }
    }

    /**
     *
     */
    protected function _addFeedbackModelling()
    {

    }
}
