<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Exercise_Feedback_Digital_NL extends Ahs_Exercise_Feedback_Abstract
{
    /**
     * @var Ahs_Time_NL
     */
    protected $_time;

    /**
     * @param Ahs_Time_NL $time
     */
    public function __construct(Ahs_Time_NL $time)
	{
		$this->Time = $time;
        $this->_generateFeedback();
	}

    /**
     * Verbal Visual
     */
    protected function _generateFeedbackVerbalVisual()
    {
        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity() ) {
            $identity = $auth->getIdentity();
            if ($identity->isUser() ) {
                $userMapper = new Application_Model_UserMapper();
                $person = $userMapper->read($identity->UserId);
            } elseif ($identity->isSupervisor() ) {
                $supervisorMapper = new Application_Model_SupervisorMapper();
                $person = $supervisorMapper->read($identity->SupervisorId);
            }
            $moduleSettings = Ahs_Exercise_Settings::getSessionModuleSettings($person);
        } else {
            $moduleSettings = new Application_Model_ModuleSettings();
        }

        $add        = '_addItemVerbalVisual';
        $addPartial = '_addPartialItemVerbalVisual';
        /**
         * Minutes
         */
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightMinutesOn() );
        $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Kijk eens goed waar de <b class="minutes">grote wijzer</b> staat.') );
        $this->$add( Ahs_Exercise_Feedback_Pause::long() );
        switch ($this->Time->Minutes) {
            case  0:
            case 15:
            case 30:
            case 45:
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Wat wil dit zeggen?') );
                $this->$add( Ahs_Exercise_Feedback_Pause::short() );
                break;
            default:
                if ($moduleSettings->Quadrants === Application_Model_ModuleSettings::QUADRANTS_OFFICIAL) {
                    $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Is de wijzer al voorbij half?') );
                } else {
                    $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Is de wijzer al voorbij het uur?') );
//                    $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Is de wijzer al voorbij het uur of niet?') );
                }
                $this->$add( Ahs_Exercise_Feedback_Pause::short() );
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Wat wil dit zeggen?') );
                break;
        }
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightMinutesOff() );
        /**
         * Hours
         */
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightHoursOn() );
        //$this->$add( Ahs_Exercise_Audio_NL::getPhrase('Waar staat de <b class="hours">kleine wijzer</b>?') );

        switch ($this->Time->Minutes) {
            case  0:
            case 30;
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Waar staat de <b class="hours">kleine wijzer</b>?') );
                $this->$add( Ahs_Exercise_Feedback_Pause::long() );
                break;
            case 15:
            case 45:
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Is de wijzer al voorbij het uur?') );
                $this->$add( Ahs_Exercise_Feedback_Pause::short() );

                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Wat wil dit zeggen?') );
                $this->$add( Ahs_Exercise_Feedback_Pause::short() );
                break;
            default:
                if ($this->Time->Minutes < 30) {
                    $this->$add( Ahs_Exercise_Audio_NL::getPhrase('Hoeveel minuten zijn er al voorbij?') );
                    $this->$add( Ahs_Exercise_Feedback_Pause::short() );
                } else {
                    $this->$add( Ahs_Exercise_Audio_NL::getPhrase('Hoeveel minuten nog tot het uur?') );
                    $this->$add( Ahs_Exercise_Feedback_Pause::short() );
                    // @todo Implement verbal visual feedback
//                    $this->_addItemVerbalVisual( Ahs_Exercise_Audio_NL::getPhrase('Hoeveel minuten nog tot half?') );
                }
                break;
        }
        if ($this->Time->Minutes != 0 && $this->Time->Minutes != 30) {
            $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Waar staat de <b class="hours">kleine wijzer</b>?') );
            $this->$add( Ahs_Exercise_Feedback_Pause::long() );
            if ($this->Time->Minutes < 30) {
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Hoe laat is het al geweest?') );
            } else {
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Hoe laat wordt het?') );
            }
        }
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightHoursOff() );
    }

    /**
     * Modelling
     */
    protected function _generateFeedbackModelling()
    {
        $add        = '_addItemModelling';
        $addPartial = '_addPartialItemModelling';
        /**
         * Minutes
         */
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightMinutesOn() );
        switch ($this->Time->Minutes) {
            case  0:
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Kijk eens goed waar de <b class="minutes">grote wijzer</b> staat.') );
                $this->$add( Ahs_Exercise_Feedback_Pause::long() );
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Hij staat bovenaan op het uur.') );
                break;
            default:
                break;
        }
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightMinutesOff() );
        /**
         * Hours
         */
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightHoursOn() );
        switch ($this->Time->Minutes) {
            case  0:
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Waar staat de <b class="hours">kleine wijzer</b>?') );
                $this->$add( Ahs_Exercise_Feedback_Pause::long() );
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhraseComposition('Deze staat op', $this->Time->Hours, 'uur.') );
                break;
            default:
                break;
        }
        $this->$add( Ahs_Exercise_Feedback_Modelling::highlightHoursOff() );
        /**
         * Time
         */
        switch ($this->Time->Minutes) {
            case  0:
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhraseComposition('Het is dus', $this->Time->Hours, 'uur.') );
                $this->$add( Ahs_Exercise_Feedback_Pause::long() );
                $this->$addPartial( Ahs_Exercise_Audio_NL::getPhrase('Probeer het eens met hulp!') );
                break;
            default:
                break;
        }
    }
}
