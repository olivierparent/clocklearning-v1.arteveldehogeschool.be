<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Exercise
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Exercise_Feedback_Modelling
{
    const ACTION                = '__ACTION__';
    const HIGHLIGHT_HOURS_ON    = '__HIGHLIGHT_HOURS_ON__';
    const HIGHLIGHT_HOURS_OFF   = '__HIGHLIGHT_HOURS_OFF__';
    const HIGHLIGHT_MINUTES_ON  = '__HIGHLIGHT_MINUTES_ON__';
    const HIGHLIGHT_MINUTES_OFF = '__HIGHLIGHT_MINUTES_OFF__';

    /**
     * @return array
     */
    public static function highlightHoursOn()
    {
        return array(self::ACTION => self::HIGHLIGHT_HOURS_ON);
    }

    /**
     * @return array
     */
    public static function highlightHoursOff()
    {
        return array(self::ACTION => self::HIGHLIGHT_HOURS_OFF);
    }

    /**
     * @return array
     */
    public static function highlightMinutesOn()
    {
        return array(self::ACTION => self::HIGHLIGHT_MINUTES_ON);
    }

    /**
     * @return array
     */
    public static function highlightMinutesOff()
    {
        return array(self::ACTION => self::HIGHLIGHT_MINUTES_OFF);
    }
}
