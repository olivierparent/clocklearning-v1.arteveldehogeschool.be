<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Form_Backoffice extends Zend_Form
{
    /**
     * @param type $options
     */
    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->setAttrib('class', 'form-horizontal')
             ->setDecorators(            Ahs_Form_Decorators_Backoffice::getForm()        )
             ->setSubFormDecorators(     Ahs_Form_Decorators_Backoffice::getSubForm()     )
             ->setDisplayGroupDecorators(Ahs_Form_Decorators_Backoffice::getDisplayGroup())
             ->setMethod('post')
             ->setAction('')
        ;
    }

    /**
     * @param type $data
     * @return type
     */
    public function isValid($data)
    {
        $valid = parent::isValid($data);

        foreach ($this->getElements() as $element) {
            $decorator = $element->getDecorator('outer');
            $class = $decorator->getOption('class');

            if ($element->hasErrors()) {
                $decorator->setOption('class', $class . ' has-error');
                // @todo SubForm Decorators
            } elseif($element->getValue() == false) {
                $decorator->setOption('class', $class . ' has-warning');
            } else {
                $decorator->setOption('class', $class . ' has-success');
            }
        }

        return $valid;
    }
}
