<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Form_Decorators_Backoffice
{
    public static function getElement()
    {
        return array(
            'ViewHelper',
            array('Errors',
                array ('class' => 'help-block')
            ),
            array(
                array('inner' => 'HtmlTag'),
                array('tag'   => 'div',
                      'class' => 'col-md-4')
            ),
            array('Label',
                array('placement' => 'PREPEND',
                      'class'     => 'col-md-2 control-label')
            ),
            array(
                array('outer' => 'HtmlTag'),
                array('tag'   => 'div',
                      'class' => 'form-group')
            ),
        );
    }

    public static function getRadio()
    {
        return array(
            'ViewHelper',
            array('Errors',
                array ('class' => 'help-block')
            ),
            array(
                array('inner' => 'HtmlTag'),
                array('tag'   => 'div',
                      'class' => 'col-md-offset-2 col-md-10')
            ),
            array('Label',
                array('placement' => 'PREPEND',
                      'class'     => 'col-md-2 control-label')
            ),
            array(
                array('outer' => 'HtmlTag'),
                array('tag'   => 'div',
                      'class' => 'form-group')
            ),
        );
    }

    public static function getCheckbox()
    {
        return array(
            'ViewHelper',
            array('Label',
                array('placement' => 'IMPLICIT_APPEND',
                      'class'     => 'checkbox')
            ),
            array('Errors',
                array ('class' => 'help-block')
            ),
            array(
                array('inner' => 'HtmlTag'),
                array('tag'   => 'div',
                      'class' => 'col-md-offset-2 col-md-10')
            ),

            array(
                array('outer' => 'HtmlTag'),
                array('tag'   => 'div',
                      'class' => 'form-group')
            ),
        );
    }

    public static function getForm()
    {
        return array(
            'FormElements',
            'Form',
        );
    }

    public static function getSubForm()
    {
        return array(
            'FormElements',
            'Fieldset',
        );
    }
    public static function getDisplayGroup()
    {
        return array(
            'FormElements',
            'Fieldset',
            array(
                'HtmlTag',
                array('tag'   => 'div',
                      'class' => 'well')
            ),
        );
    }

    public static function getButton()
    {
        return array(
            'ViewHelper',
            array(
                array('inner'    => 'HtmlTag'),
                array('tag'      => 'div',
                      'class'    => 'col-md-offset-2 col-md-10'
                ),
            ),
            array(
                array('outer'    => 'HtmlTag'),
                array('tag'      => 'div',
                      'class'    => 'form-group'
                ),
            ),
        );
    }

    public static function getButtonOpen()
    {
        return array(
            'ViewHelper',
            array(
                array('inner'    => 'HtmlTag'),
                array('tag'      => 'div',
                      'openOnly' =>  true,
                      'class'    => 'col-md-offset-2 col-md-10'
                ),
            ),
            array(
                array('outer'    => 'HtmlTag'),
                array('tag'      => 'div',
                      'openOnly' =>  true,
                      'class'    => 'form-group'
                ),
            ),
        );
    }

    public static function getButtonClose($href = '/')
    {
        return array(
            array('Description', array('tag'  => 'a',
                                       'href'  => $href,
                                       'class' => 'btn btn-link')
            ),
            array(
                array('closeInner' => 'HtmlTag'),
                array('tag'        => 'div',
                      'closeOnly'  =>  true)
            ),
            array(
                array('closeOuter' => 'HtmlTag'),
                array('tag'        => 'div',
                      'closeOnly'  =>  true)
            ),
        );
    }
}
