<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Form_Decorators_Frontoffice
{
    public static function getElement()
    {
        return array(
            'ViewHelper',
            'Errors',
        );
    }

    public static function getCaptcha()
    {
        return array(
            'Captcha',
            'Label',
            array('HtmlTag',
//                array('tag'  => 'div',),
            ),
            array('Errors',
                array ('class' => 'errors'),
            ),
        );
    }

//    public static function getRadio()
//    {
//        return array(
//            'ViewHelper',
//            array('Errors',
//                array ('class' => 'help-inline')
//            ),
//            array(
//                array('inner' => 'HtmlTag'),
//                array('tag'   => 'div',
//                      'class' => 'controls')
//            ),
//            array('Label',
//                array('placement' => 'PREPEND',
//                      'class'     => 'errors')
//            ),
//            array(
//                array('outer' => 'HtmlTag'),
//                array('tag'   => 'div',
//                      'class' => 'control-group')
//            ),
//        );
//    }
//
//    public static function getCheckbox()
//    {
//        return array(
//            'ViewHelper',
//            array('Label',
//                array('placement' => 'IMPLICIT_APPEND',
//                      'class'     => 'checkbox')
//            ),
//            array('Errors',
//                array ('class' => 'errors')
//            ),
//            array(
//                array('inner' => 'HtmlTag'),
//                array('tag'   => 'div',
//                      'class' => 'controls')
//            ),
//
//            array(
//                array('outer' => 'HtmlTag'),
//                array('tag'   => 'div',
//                      'class' => 'control-group')
//            ),
//        );
//    }
//
    public static function getForm()
    {
        return array(
            'FormElements',
            'Form',
        );
    }
//
//    public static function getSubForm()
//    {
//        return array(
//            'FormElements',
//            'Fieldset',
//        );
//    }
//    public static function getDisplayGroup()
//    {
//        return array(
//            'FormElements',
//            'Fieldset',
//            array(
//                'HtmlTag',
//                array('tag'   => 'div',
//                      'class' => 'well')
//            ),
//        );
//    }
//
    public static function getButton()
    {
        return array('ViewHelper',
                   array('HtmlTag',
                       array('tag'      => 'div'),
                   ),
               );
    }

    public static function getButtonOpen()
    {
        return array(
            'ViewHelper',
            array('HtmlTag',
                  array('tag'      => 'div',
                        'openOnly' => true,
                  )
            ),
        );
    }

    public static function getButtonClose($href = '/')
    {
        return array(
           array('Description', array('tag'         => 'a',
                                      'href'        => $href,
                                      'data-inline' => 'true',
                                      'data-role'   => 'button',
           )),
           array('HtmlTag'    , array('tag'         => 'div',
                                      'closeOnly'   => true,
           )),
        );
    }
}
