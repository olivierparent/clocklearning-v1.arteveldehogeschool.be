<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * Abstract class for an object.
 *
 * @category   Ahs
 * @package    Ahs_Object
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Object_Abstract
{
	/**
	 * Generic property getter.
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		$method = 'get' . ucfirst($name);
		if (method_exists($this, $method) ) {
			return $this->{$method}();
		} else {
			$property = '_' . lcfirst($name);
			if (property_exists($this, $property) ) {
				return $this->{$property};
			} else {
				$this->_throwError($property);
			}
		}

	}

	/**
	 * Generic property setter.
	 *
	 * @param string $name
	 * @param mixed  $value
	 */
	public function __set($name, $value)
	{
		$method = 'set' . ucfirst($name);
		if (method_exists($this, $method) ) {
			$this->{$method}($value);
		} else {
			$property = '_' . lcfirst($name);
			if (property_exists($this, $property) ) {
				$this->{$property} = $value;
			} else {
				$this->_throwError($property);
			}
		}
	}

	/**
	 * Throws an ErrorException 'property does not exist in class'.
	 * klasse.
	 *
	 * @param string $property
	 * @throws ErrorException
	 */
	protected function _throwError($property)
	{
		throw new ErrorException("Property <strong>{$property}</strong> does not exist in class <strong>" . get_called_class() . '<strong>');
	}
}
