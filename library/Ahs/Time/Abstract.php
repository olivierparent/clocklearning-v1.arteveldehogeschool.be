<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Time
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Time_Abstract extends Ahs_Object_Abstract
{
    const TEXT_ANALOGUE                                     = 'TEXT_ANALOGUE';
    const TEXT_ANALOGUE_PAST                                = 'TEXT_ANALOGUE_PAST';
    const TEXT_ANALOGUE_TO                                  = 'TEXT_ANALOGUE_TO';
    const TEXT_ANALOGUE_QUARTERS_AS_NUMBER                  = 'TEXT_ANALOGUE_QUARTERS_AS_NUMBER';
    const TEXT_ANALOGUE_SWITCH_H_AND_M                      = 'TEXT_ANALOGUE_SWITCH_H_AND_M';
    const TEXT_ANALOGUE_SWITCH_H_AND_M_THEN_PAST            = 'TEXT_ANALOGUE_SWITCH_H_AND_M_THEN_PAST';
    const TEXT_DIGITAL                                      = 'TEXT_DIGITAL';
    const TEXT_DIGITAL_H×10                                 = 'TEXT_DIGITAL_H×10';
    const TEXT_DIGITAL_H×100                                = 'TEXT_DIGITAL_H×100';
    const TEXT_DIGITAL_M00_AS_0                             = 'TEXT_DIGITAL_M00_AS_0';
    const TEXT_DIGITAL_REVERSE_M_DIGITS                     = 'TEXT_DIGITAL_REVERSE_M_DIGITS';
    const TEXT_DIGITAL_SWITCH_H_AND_M                       = 'TEXT_DIGITAL_SWITCH_H_AND_M';
    const TEXT_DIGITAL_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS = 'TEXT_DIGITAL_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS';

    const CORRECT_ANALOGUE                                              = 'correctAnalogue';
    const ERROR_ANALOGUE_H_00_12_M_00                                   = 'errorAnalogueH0012M00';
    const ERROR_ANALOGUE_H_03_15_M_15                                   = 'errorAnalogueH0315M15';
    const ERROR_ANALOGUE_H_05_17_M_30                                   = 'errorAnalogueH0517M30';
    const ERROR_ANALOGUE_H_09_21_M_45                                   = 'errorAnalogueH0921M45';
    const ERROR_ANALOGUE_HOURS_HALF_HOUR_EARLIER                        = 'errorAnalogueHoursHalfHourEarlier';
    const ERROR_ANALOGUE_HOURS_HALF_HOUR_LATER                          = 'errorAnalogueHoursHalfHourLater';
    const ERROR_ANALOGUE_HOURS_HOUR_EARLIER                             = 'errorAnalogueHoursHourEarlier';
    const ERROR_ANALOGUE_HOURS_HOUR_LATER                               = 'errorAnalogueHoursHourLater';
    const ERROR_ANALOGUE_HOURS_MIRROR_Y                                 = 'errorAnalogueHoursMirrorY';
    const ERROR_ANALOGUE_HOURS_MIRROR_Z                                 = 'errorAnalogueHoursMirrorZ';
    const ERROR_ANALOGUE_MINUTES_01M_EARLIER                            = 'errorAnalogueMinutes01mEarlier';
    const ERROR_ANALOGUE_MINUTES_01M_DIFFERENCE                         = 'errorAnalogueMinutes01mDifference';
    const ERROR_ANALOGUE_MINUTES_01M_LATER                              = 'errorAnalogueMinutes01mLater';
    const ERROR_ANALOGUE_MINUTES_05M_EARLIER                            = 'errorAnalogueMinutes05mEarlier';
    const ERROR_ANALOGUE_MINUTES_05M_DIFFERENCE                         = 'errorAnalogueMinutes05mDifference';
    const ERROR_ANALOGUE_MINUTES_05M_LATER                              = 'errorAnalogueMinutes05mLater';
    const ERROR_ANALOGUE_MINUTES_MIRROR_Y                               = 'errorAnalogueMinutesMirrorY';
    const ERROR_ANALOGUE_MINUTES_MIRROR_Z                               = 'errorAnalogueMinutesMirrorZ';
    const ERROR_ANALOGUE_MINUTES_READ_AS_ADDITION_TO_HOURS              = 'errorAnalogueMinutesReadAsAdditionToHours';
    const ERROR_ANALOGUE_MINUTES_READ_AS_HOURS                          = 'errorAnalogueMinutesReadAsHours';
    const ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO             = 'errorAnalogueMinutesReadAsHoursWithPastTo';
    const ERROR_ANALOGUE_READ_AS_FULL_HOURS                             = 'errorAnalogueReadAsFullHours';
    const ERROR_ANALOGUE_SWITCH_HANDS                                   = 'errorAnalogueSwitchHands';
    const ERROR_ANALOGUE_SWITCH_HANDS_NEAREST_HOUR                      = 'errorAnalogueSwitchHandsNearestHour';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_FULL_HOURS                = 'errorAnalogueSwitchHandsReadAsFullHours';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS                     = 'errorAnalogueSwitchHandsReadAsHours';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS_WITH_PAST_TO        = 'errorAnalogueSwitchHandsReadAsHoursWithPastTo';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_MINUTES_WITH_PAST_TO      = 'errorAnalogueSwitchHandsReadAsMinutesWithPastTo';

    const CORRECT_DIGITAL                                               = 'correctDigital';
    const CORRECT_DIGITAL_READ_ANALOGUE                                 = 'correctDigitalReadAnalogue';
    const CORRECT_DIGITAL_READ_ANALOGUE_PAST                            = 'correctDigitalReadAnaloguePast';
    const ERROR_DIGITAL_EARLIER_BY_30M                                  = 'errorDigitalEarlierBy30m';
    const ERROR_DIGITAL_EARLIER_BY_60M                                  = 'errorDigitalEarlierBy60m';
    const ERROR_DIGITAL_READ_H×10                                       = 'errorDigitalReadH×10';
    const ERROR_DIGITAL_READ_H×100                                      = 'errorDigitalReadH×100';
    const ERROR_DIGITAL_READ_HOUR                                       = 'errorDigitalReadHour';
    const ERROR_DIGITAL_READ_M00_AS_0                                   = 'errorDigitalReadM00As0';
    const ERROR_DIGITAL_READ_REVERSE_M_DIGITS                           = 'errorDigitalReadReverseMDigits';
    const ERROR_DIGITAL_READ_QUARTERS_AS_NUMBER                         = 'errorDigitalReadQuartersAsNumber';
    const ERROR_DIGITAL_READ_SWITCH_H_AND_M                             = 'errorDigitalReadSwitchHAndM';
    const ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_PAST                   = 'errorDigitalReadSwitchHAndMThenPast';
    const ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS       = 'errorDigitalReadSwitchHAndMThenReverseHDigits';
    const ERROR_DIGITAL_READ_TO                                         = 'errorDigitalReadTo';
    const ERROR_DIGITAL_SWITCH_PAST_AND_TO                              = 'errorDigitalSwitchPastAndTo';
    const ERROR_DIGITAL_SWITCH_PAST_AND_TO_THEN_READ_QUARTERS_AS_NUMBER = 'errorDigitalSwitchPastAndToThenReadQuartersAsNumber';

    protected static $_checkQuadrants  = true; // TRUE: NL official (10 voor half) | FALSE: NL alternative (20 over)
	protected static $_checkTwentyFour = false;

	protected static $_checkHalfHour    = true;
	protected static $_checkHour        = true;
    protected static $_checkImaginary   = false;
	protected static $_checkQuarterPast = true;
	protected static $_checkQuarterTo   = true;

	protected static $_radioMinutes = false;
	protected static $_radioFives   = false;
	protected static $_radioTens    = false;

	protected $_hours;
	protected $_minutes;
    protected $_mode;

	/**
	 * @param int $hours
	 * @param int $minutes
	 */
    public function __construct($hours = null, $minutes = null)
	{
		$this->setTime($hours, $minutes);
	}

	/**
	 * @param int $hours
	 * @param int $minutes
	 */
	public function setTime($hours = null, $minutes = null)
	{
		$this->Hours   = $hours;
		$this->Minutes = $minutes;
	}

    /**
     * @param string $mode
     * @return bool
     */
    public function setMode($mode)
    {
        $this->_mode = $mode;

        return true;
    }

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->toTime();
	}

    /**
     * Increment or decrement the time by a number of hours.
     *
     * @param int $hoursDelta
     */
    public function deltaHours($hoursDelta = 1)
	{
		$max = (self::$_checkTwentyFour ? 23 : 11);

		$hoursDelta %= $max;

		$hours  = $this->Hours;
		$hours += $hoursDelta;

		if ($hours < 0) {
			$hours += $max + 1;
		} elseif ($max < $hours) {
			$hours -= $max + 1;
		}

		$this->Hours = $hours;
	}

    public function multiplyHours($factor = 1)
    {
        $this->Hours *= $factor;
    }

    /**
     * Increment or decrement the time by a number of minutes.
     *
     * @param int $minutesDelta
     * @todo check results
     */
    public function deltaMinutes($minutesDelta = 1)
	{
		$max = (self::$_checkTwentyFour ? 1439 : 719);

		$minutesDelta %= $max;

		$minutes  = $this->Hours * 60 + $this->Minutes;
		$minutes += $minutesDelta;

		if ($minutes < 0) {
			$minutes += $max + 1;
		} elseif ($max < $minutes) {
			$minutes -= $max + 1;
		}

        $this->Minutes = $minutes % 60;
		$this->Hours   = (int) ($minutes - $this->Minutes) / 60;
	}

    abstract public function toText();

	/**
	 * Convert time in hours and minutes to a formatted time string.
	 *
	 * @return string
	 */
	public function toTime()
	{
		return $this->toHours() . ':' . $this->toMinutes();
	}

    /**
     * Convert the hours component of time to a formatted string.
     *
     * @return string
     */
    public function toHours()
    {
        return sprintf('%02d', $this->Hours);
    }

    /**
     * Convert the minutes component of time to a formatted string.
     *
     * @return string
     */
    public function toMinutes()
    {
        return sprintf('%02d', $this->Minutes);
    }

	/**
	 * Generate a random hour based on constraints.
	 *
	 * @return int
	 */
	protected static function _getHoursRandom()
	{
		return self::_getRandomArrayItem(self::getHoursAvailable() );
	}

	/**
	 * Generate a random minutes based on constraints.
	 *
	 * @return int
	 */
	protected static function _getMinutesRandom()
	{
		return self::_getRandomArrayItem(self::getMinutesAvailable() );
	}

    /**
     * @param array $a
     * @return int
     */
    protected static function _getRandomArrayItem(array $a)
	{
		$max = count($a);

		return (0 < $max) ? $a[mt_rand(0, --$max)] : 0;
	}

	/**
	 * Use 24-hour clock.
	 */
	public static function checkTwentyFour()
	{
		self::$_checkTwentyFour = true;
	}

    /**
     * @return bool
     */
    public static function getTwentyFour()
    {
        return self::$_checkTwentyFour;
    }


    /**
	 * Use 12-hour clock.
	 */
	public static function uncheckTwentyFour()
	{
		self::$_checkTwentyFour = false;
	}

	/**
	 * Use quadrants in lieu of halves.
	 */
	public static function checkQuadrants()
	{
		self::$_checkQuadrants = true;
	}

    /**
     * @return bool
     */
    public static function getQuadrants()
    {
        return self::$_checkQuadrants;
    }

	/**
	 * Use halves in lieu of quadrants.
	 */
	public static function uncheckQuadrants()
	{
		self::$_checkQuadrants = false;
	}

	/**
	 * Select 1 minute increments and deselect others.
	 */
	public static function selectIncrementMinutes()
	{
		self::$_radioMinutes = true;
		self::_deselectIncrementFives();
		self::_deselectIncrementTens();
	}

	/**
	 * Deselect 1 minute increments.
	 */
	protected static function _deselectIncrementMinutes()
	{
		self::$_radioMinutes = false;
	}

	/**
	 * Select 5 minute increments and deselect others.
	 */
	public static function selectIncrementFives()
	{
		self::_deselectIncrementMinutes();
		self::$_radioFives = true;
		self::_deselectIncrementTens();
	}

	/**
	 * Deselect 5 minute increments.
	 */
	protected static function _deselectIncrementFives()
	{
		self::$_radioFives = false;
	}

	/**
	 * Select 10 minute increments and deselect others.
	 */
	public static function selectIncrementTens()
	{
		self::_deselectIncrementMinutes();
		self::_deselectIncrementFives();
		self::$_radioTens = true;
	}

	/**
	 * Deselect 10 minute increments.
	 */
	protected static function _deselectIncrementTens()
	{
		self::$_radioTens = false;
	}

	/**
	 * Deselect all minute increments
	 */
	public static function deselectIncrement()
	{
		self::_deselectIncrementMinutes();
		self::_deselectIncrementFives();
		self::_deselectIncrementTens();
	}

	/**
	 *
	 */
	public static function checkQuarterPast()
	{
		self::$_checkQuarterPast = true;
	}

	/**
	 *
	 */
	public static function uncheckQuarterPast()
	{
		self::$_checkQuarterPast = false;
	}

	/**
	 *
	 */
	public static function checkQuarterTo()
	{
		self::$_checkQuarterTo = true;
	}

	/**
	 *
	 */
	public static function uncheckQuarterTo()
	{
		self::$_checkQuarterTo = false;
	}

	/**
	 *
	 */
	public static function checkHalfHour()
	{
		self::$_checkHalfHour = true;
	}

	/**
	 *
	 */
	public static function uncheckHalfHour()
	{
		self::$_checkHalfHour = false;
	}

	/**
	 *
	 */
	public static function checkHour()
	{
		self::$_checkHour = true;
	}

	/**
	 *
	 */
	public static function uncheckHour()
	{
		self::$_checkHour = false;
	}

    /**
     *
     */
    public static function checkImaginary()
	{
		self::$_checkImaginary = true;
	}

    /**
     *
     */
    public static function uncheckImaginary()
	{
		self::$_checkImaginary = false;
	}

	/**
	 * @return array
	 */
	public static function getHoursAvailable()
	{
		for ($h = array(), $i = 0; $i <= (self::$_checkTwentyFour ? 23 : 11); $i++) {
			$h[] = $i;
		}
		return $h;
	}

	/**
	 *
	 * @static
	 * @return array
	 */
	public static function getMinutesAvailable()
	{
		$m = array();

        if (self::$_checkHour) {
            $m[] = 0;
        }

        if (self::$_checkQuarterPast) {
            $m[] = 15;
        }

        if (self::$_checkHalfHour   ) {
            $m[] = 30;
        }

        if (self::$_checkQuarterTo  ) {
            $m[] = 45;
        }

        if (self::$_radioMinutes || self::$_radioFives || self::$_radioTens) {
            if (self::$_radioMinutes) {
                $step = 1;
            } elseif (self::$_radioFives) {
                $step = 5;
            } else {
                $step = 10;
            }
            $m = array_unique(array_merge(range(0, 59, $step), $m ) );
            sort($m);
        }

		return $m;
	}

	/**
	 * @param int $hours
	 * @throws ErrorException
	 */
	public function setHours($hours = null)
	{
		if ($hours === null) {
			$this->_hours = self::_getHoursRandom();
		} elseif (in_array($hours, self::getHoursAvailable() ) ) {
			$this->_hours = $hours;
		} else {
			throw new ErrorException("Illegal value <strong>{$hours}</strong> for hours in class <strong>" . get_called_class() . '</strong>');
		}
	}

    /**
     * @param int $minutes
     * @throws ErrorException
     */
    public function setMinutes($minutes = null)
	{
		if ($minutes === null) {
			$this->_minutes = self::_getMinutesRandom();
		} elseif (0 <= $minutes && $minutes < 60) {
			$this->_minutes = $minutes;
		} else {
			throw new ErrorException("Illegal value <strong>{$minutes}</strong> for minutes in class <strong>" . get_called_class() . '</strong>');
		}
	}

    /**
     * ERROR_ANALOGUE_H_00_12_M_00
     *
     * @return bool
     */
    public function errorAnalogueH0012M00()
    {
        if (self::$_checkTwentyFour) {
            $this->Hours = $this->Hours < 12 ? 0 : 12;
        } else {
            $this->Hours = 0;
        }

        $this->Minutes = 0;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_H_03_15_M_15
     *
     * @return bool
     */
    public function errorAnalogueH0315M15()
    {
        if (self::$_checkTwentyFour) {
            $this->Hours = $this->Hours < 12 ? 3 : 15;
        } else {
            $this->Hours = 3;
        }

        $this->Minutes = 15;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_H_05_17_M_30
     *
     * @return bool
     */
    public function errorAnalogueH0517M30()
    {
        if (self::$_checkTwentyFour) {
            $this->Hours = $this->Hours < 12 ? 5 : 17;
        } else {
            $this->Hours = 5;
        }

        $this->Minutes = 30;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_H_09_21_M_45
     *
     * @return bool
     */
    public function errorAnalogueH0921M45()
    {
        if (self::$_checkTwentyFour) {
            $this->Hours = $this->Hours < 12 ? 9 : 21;
        } else {
            $this->Hours = 9;
        }

        $this->Minutes = 45;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_HOURS_HALF_HOUR_EARLIER
     */
    public function errorAnalogueHoursHalfHourEarlier()
    {
        $this->deltaMinutes(-30);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_HOURS_HALF_HOUR_LATER
     *
     * @return bool
     */

    public function errorAnalogueHoursHalfHourLater()
    {
        if (in_array($this->Hours, array(11, 23)) && $this->Minutes <= 30) {
            $this->deltaMinutes(-660);
        } else {
            $this->deltaMinutes(+30);
        }

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_HOURS_HOUR_EARLIER
     *
     * @return bool
     */
    public function errorAnalogueHoursHourEarlier()
    {
        $hoursDelta = -1;

        if (self::$_checkTwentyFour && ($this->Hours === 0 || $this->Hours === 12) ) {
            $hoursDelta += 12;
        }

        $this->deltaHours($hoursDelta);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_HOURS_HOUR_LATER
     *
     * @return bool
     */
    public function errorAnalogueHoursHourLater()
    {
        $hoursDelta = 1;

        if (self::$_checkTwentyFour && ($this->Hours === 11 || $this->Hours === 23) ) {
            $hoursDelta -= 12;
        }

        $this->deltaHours($hoursDelta);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_HOURS_MIRROR_Y
     *
     * @return bool
     */
    public function errorAnalogueHoursMirrorY()
    {
        $this->Hours = ($this->Hours < 13 ? 12 : 24) - $this->Hours;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_HOURS_MIRROR_Z
     *
     * Six hours difference but keep before or after meridian.
     *
     * @return bool
     */
    public function errorAnalogueHoursMirrorZ()
    {
        if (( 0 <= $this->Hours && $this->Hours <=  6) ||
            (12 <= $this->Hours && $this->Hours <= 18)) {
            $this->deltaHours(+6);
        } else {
            $this->deltaHours(-6);
        }

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_01M_EARLIER
     *
     * One minute earlier.
     *
     * @return bool
     */
    public function errorAnalogueMinutes01mEarlier()
    {
        $this->deltaMinutes(-1);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_01M_DIFFERENCE
     *
     * One minute difference.
     *
     * @return bool
     */
    public function errorAnalogueMinutes01mDifference()
    {
        switch ($this->Minutes % 5) {
            case 0:
                return false;
                break;
            case 1:
                if ($this->Minutes < 30) {
                    return $this->errorAnalogueMinutes01mLater();
                } else {
                    return $this->errorAnalogueMinutes01mEarlier();
                }
                break;
            case 4:
                if ($this->Minutes < 30) {
                    return $this->errorAnalogueMinutes01mEarlier();
                } else {
                    return $this->errorAnalogueMinutes01mLater();
                }
                break;
            default:
                if (mt_rand(0, 1)) {
                    return $this->errorAnalogueMinutes01mEarlier();
                } else {
                    return $this->errorAnalogueMinutes01mLater();
                }
                break;
        }
    }

    /**
     * ERROR_ANALOGUE_MINUTES_01M_LATER
     *
     * One minute later.
     *
     * @return bool
     */
    public function errorAnalogueMinutes01mLater()
    {
        $this->deltaMinutes(+1);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_05M_EARLIER
     *
     * Five minutes earlier.
     *
     * @return bool
     */
    public function errorAnalogueMinutes05mEarlier()
    {
        $this->deltaMinutes(-5);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_05M_DIFFERENCE
     *
     * Five minutes difference.
     *
     * @return bool
     */
    public function errorAnalogueMinutes05mDifference()
    {
        if ($this->Minutes < 30) {
            return $this->errorAnalogueMinutes05mLater();
        } else {
            return $this->errorAnalogueMinutes05mEarlier();
        }
    }


    /**
     * ERROR_ANALOGUE_MINUTES_5_LATER
     *
     * Five minutes later.
     *
     * @return bool
     */
    public function errorAnalogueMinutes05mLater()
    {
        $this->deltaMinutes(+5);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_MIRROR_Y
     *
     * @return bool
     */
    public function errorAnalogueMinutesMirrorY()
    {
        if (in_array($this->Minutes, array(0, 30) ) ) {
            return false;
        } else {
            if ($this->Minutes < 30) {
                if (self::getTwentyFour()) {
                    if (in_array($this->Hours, array(0, 12) ) ) {
                        $this->deltaHours(+11);
                    } else {
                        $this->deltaHours(-1);
                    }
                } else {
                    $this->deltaHours(-1);
                }
            } else {
                $this->deltaHours(+1);
            }
            $this->Minutes = 60 - $this->Minutes;
        }

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_MIRROR_Z
     *
     * @return bool
     */
    public function errorAnalogueMinutesMirrorZ()
    {
        $this->deltaMinutes(30);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_READ_AS_ADDITION_TO_HOURS
     *
     * @return bool
     */
    public function errorAnalogueMinutesReadAsAdditionToHours()
    {
        $minutes = (int) ($this->Minutes / 5);
        $minutesModulo =  $this->Minutes % 5 ;

        if ($minutes === 0) {
            $minutes += 12;
        }

        if ($this->Minutes < 30) {
            $this->Minutes = $minutes + ($minutes < 12 && $minutesModulo == 4 ? 0 : $minutesModulo);
        } else {
            $this->Minutes = $minutes + ($minutes < 12 && $minutesModulo == 4 ? 0 : $minutesModulo);
            $this->errorAnalogueMinutesMirrorY();
        }

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_READ_AS_HOURS
     *
     * @return bool
     */
    public function errorAnalogueMinutesReadAsHours()
    {
        $this->Minutes = $this->_to12Hours($this->_toHours($this->Minutes));

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO
     *
     * @return bool
     */
    public function errorAnalogueMinutesReadAsHoursWithPastTo()
    {
        $minutes = $this->_toHours($this->Minutes);

        if ($minutes === 0) {
            $minutes = 12;
        }
        if ($minutes === 15) {
            $minutes = 3;
        }

        if ($this->Minutes <= 30) {
            $this->Minutes = $minutes;
        } else {
            $this->Minutes = 60 - $minutes;
        }

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_READ_AS_FULL_HOURS
     *
     * Only use when hands overlap.
     *
     * Set minutes to 0.
     *
     * @return bool
     */
    public function errorAnalogueReadAsFullHours()
    {
        $hours   = $this->_to12Hours($this->Hours);
        $minutes = $this->_to12Hours($this->_toHours($this->Minutes));

        // Return false if hands don't overlap.
        if ($hours !== $minutes) {
            return false;
        }

        $this->Minutes = 0;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_SWITCH_HANDS
     *
     * 1. Switch hands.
     *
     * @return bool
     */
    public function errorAnalogueSwitchHands()
    {
        $hours   = $this->_toHours($this->Minutes);
        $minutes = $this->_toMinutes($this->Hours);

        $this->Hours   = $hours;
        $this->Minutes = $minutes;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_SWITCH_HANDS_NEAREST_HOUR
     *
     * 1. Switch hands must have happened before.
     * 2. Keep hour hand to nearest hour marking.
     *
     * @return bool
     */
    public function errorAnalogueSwitchHandsNearestHour()
    {
        // Keep hour hand to nearest hour marking.
        if ($this->Minutes !== 0 && 30 <= $this->Minutes) {
            $this->deltaHours(-1);
        }

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_FULL_HOURS
     *
     * Only use when hands overlap.
     *
     * 1. Switch hands.
     * 2. Set minutes to 0.
     *
     * @return bool
     */
    public function errorAnalogueSwitchHandsReadAsFullHours()
    {
        $hours   = $this->_to12Hours($this->Hours);
        $minutes = $this->_to12Hours($this->_toHours($this->Minutes));

        // Return false if hands don't overlap.
        if ($hours !== $minutes) {
            return false;
        }

        $this->Hours   = $this->_to24Hours($minutes);
        $this->Minutes = 0;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS
     *
     * Only use when minutes are less than 23.
     *
     * @return bool
     */
    public function errorAnalogueSwitchHandsReadAsHours()
    {
        if (23 < $this->Minutes) {
            return false;
        }

        $hours   = $this->Minutes;
        $minutes = $this->Hours;

        $this->Hours   = $hours;
        $this->Minutes = $minutes;

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS_WITH_PAST_TO
     *
     * Not applied when time is 03:mm, 09:mm, 15:mm, or 21:mm.
     *
     * 1. Switch hands.
     * 2. Read minutes according to hour markings.
     * 3. Minutes past/to according to what half of the face the hand is on.
     *
     * @return bool
     */
    public function errorAnalogueSwitchHandsReadAsHoursWithPastTo()
    {
        if (!in_array($this->Minutes, array(50)) && (
                in_array($this->Hours, array(3, 15, 9, 21)               ) ||
               !in_array($this->Hours, array(1, 13, 5, 17, 7, 19, 11, 23))
            )
        ) {
            return false;
        }

        $this->errorAnalogueSwitchHands();
        $this->errorAnalogueMinutesReadAsHoursWithPastTo();

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_MINUTES_WITH_PAST_TO
     *
     * @return bool
     */
    public function errorAnalogueSwitchHandsReadAsMinutesWithPastTo()
    {
        $minutes = $this->_to12hours($this->Hours);
        $hours = $this->_toHours($this->Minutes);

        if ($hours <= 6) {
            $this->Minutes = $minutes;
        } else {
            $this->Minutes = 60 - $minutes;
        }
        $this->Hours = $hours;
        $this->deltaHours(-1);

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_EARLIER_BY_30M
     */
    public function errorDigitalEarlierBy30M()
    {
        $this->errorAnalogueHoursHalfHourEarlier();

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_EARLIER_BY_60M
     */
    public function errorDigitalEarlierBy60M()
    {
        $this->errorAnalogueHoursHourEarlier();

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_H×10
     *
     * @return bool
     */
    public function errorDigitalReadH×10()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_H×100
     *
     * @return bool
     */
    public function errorDigitalReadH×100()
    {
        return $this->setMode(__FUNCTION__);
    }
    /**
     * ERROR_DIGITAL_READ_HOUR
     *
     * @return bool
     */
    public function errorDigitalReadHour()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_M00_AS_0
     *
     * @return bool
     */
    public function errorDigitalReadM00As0()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_REVERSE_M_DIGITS
     *
     * @return bool
     */
    public function errorDigitalReadReverseMDigits()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_QUARTERS_AS_NUMBER
     *
     * @return bool
     */
    public function errorDigitalReadQuartersAsNumber()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_SWITCH_H_AND_M
     *
     * @return bool
     */
    public function errorDigitalReadSwitchHAndM()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_PAST
     *
     * @return bool
     */
    public function errorDigitalReadSwitchHAndMThenPast()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_SWITCH_H_AND_M_THEN_REVERSE_H_DIGITS
     *
     * @return bool
     */
    public function errorDigitalReadSwitchHAndMThenReverseHDigits()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_READ_TO
     *
     * @return bool
     */
    public function errorDigitalReadTo()
    {
        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_SWITCH_PAST_AND_TO
     *
     * @return bool
     */
    public function errorDigitalSwitchPastAndTo()
    {
        $this->errorAnalogueMinutesMirrorY();

        return $this->setMode(__FUNCTION__);
    }

    /**
     * ERROR_DIGITAL_SWITCH_PAST_AND_TO_THEN_READ_QUARTERS_AS_NUMBER
     *
     * @return bool
     */
    public function errorDigitalSwitchPastAndToThenReadQuartersAsNumber()
    {
        $this->errorDigitalSwitchPastAndTo();

        return $this->setMode(__FUNCTION__);
    }

    /**
     * @param int $hours
     * @return int
     */
    protected function _to12Hours($hours)
    {
        if ($hours === 0) {
                $hours = 12;
            } elseif (12 < $hours) {
                $hours -= 12;
        }

        return $hours;
    }

    /**
     * @param int $hours
     * @return int
     */
    protected function _to24Hours($hours)
    {
        if (12 <= $this->Hours) {
            $hours += 12;
        }

        return $hours;
    }

    /**
     * @param int $minutes
     * @return int
     */
    protected function _toHours($minutes)
    {
        $hours = (int) round($minutes / 5);

        if (self::$_checkTwentyFour) {
            $hours = $this->_to24Hours($hours);
        } else {
            if ($hours == 12) {
                $hours -= 12;
            }
        }

        if (24 <= $hours) {
            $hours -= 24;
        }

        return $hours;
    }

    /**
     * @param int $hours
     * @return int
     */
    protected function _toMinutes($hours)
    {
        if (self::$_checkTwentyFour) {
            $hours = $this->_to12Hours($hours);
        }
        $minutes = ($hours < 12) ? $hours * 5 : 0;

        return $minutes;
    }
}
