<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @category   Ahs
 * @package    Ahs_Time
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Time_EN extends Ahs_Time_Abstract
{
    const ANTE_MERIDIEM                = 'a.m.';
    const FORMAT_DIGITAL_MINUTES_01_09 = '%1$d O %2$d';
    const FORMAT_DIGITAL_MINUTES_10_59 = '%1$d %2$d';
    const FORMAT_DIGITAL_FULL_HOUR     = '%1$d';
    const FORMAT_ANALOGUE_FULL_HOUR    = '%1$d o’clock';
    const FORMAT_ANALOGUE_PAST         = '%2$s past %1$d';
    const FORMAT_ANALOGUE_TO           = '%2$s to %1$d';
    const HALF                         = 'half';
    const POST_MERIDIEM                = 'p.m.';
    const QUARTER                      = 'a quarter';

    /**
     * Convert time in hours and minutes to a formatted text string.
     *
     * @param string $type
     * @return string
     * @throws ErrorException
     */
    public function toText($type = self::TEXT_ANALOGUE)
    {
        $h = $this->Hours;
        $m = $this->Minutes;

        switch ($type) {
            case self::TEXT_DIGITAL:
                if (0 <= $h && $h < 24) {
                    $meridian = ($h < 12) ? self::ANTE_MERIDIEM : self::POST_MERIDIEM;

                    if ($h === 0) {
                        $h = 12;
                    } elseif (12 < $h) {
                        $h -= 12;
                    }
                } else {
                    throw new ErrorException("Illegal value <strong>{$h}</strong> for hours in class <strong>" . get_called_class() . '</strong>');
                }

                if ($m === 0) {
                    $format = self::FORMAT_DIGITAL_FULL_HOUR;
                } elseif (1 <= $m && $m <= 9) {
                    $format = self::FORMAT_DIGITAL_MINUTES_01_09;
                } else {
                    $format = self::FORMAT_DIGITAL_MINUTES_10_59;
                }

                $format .= ' ' . $meridian;
                break;
            case self::TEXT_ANALOGUE:
            default:
                if (0 <= $h && $h < 24) {
                    if ($h === 0) {
                        $h = 12;
                    } elseif (12 < $h) {
                        $h -= 12;
                    }
                } else {
                    throw new ErrorException("Illegal value <strong>{$h}</strong> for hours in class <strong>" . get_called_class() . '</strong>');
                }

                if ($m === 0) {
                    $format = self::FORMAT_ANALOGUE_FULL_HOUR;
                } elseif (1 <= $m && $m <= 30) {
                    $format = self::FORMAT_ANALOGUE_PAST;
                } elseif (31 <= $m && $m <= 59) {
                    $format = self::FORMAT_ANALOGUE_TO;
                    if ($h++ === 12) {
                        $h = 1;
                    }
                    $m = 60 - $m;
                } else {
                    throw new ErrorException("Illegal value <strong>{$m}</strong> for minutes in class <strong>" . get_called_class() . '</strong>');
                }

                if ($m === 15) {
                    $m = self::QUARTER;
                }
                if ($m === 30) {
                    $m = self::HALF;
                }
                break;
        }

        return sprintf($format, $h, $m);
    }
}
