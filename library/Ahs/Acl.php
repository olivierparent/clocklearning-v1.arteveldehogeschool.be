<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 * Access Control List
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Acl extends Zend_Acl
{
    const ROLE_ADMIN      = 'ROLE_ADMIN'     ;
    const ROLE_ALL        =  null            ;
    const ROLE_GUEST      = 'ROLE_GUEST'     ;
    const ROLE_SUPERVISOR = 'ROLE_SUPERVISOR';
    const ROLE_USER       = 'ROLE_USER'      ;

    public function __construct()
    {
        $this->addRole(self::ROLE_GUEST                                   )
             ->addRole(self::ROLE_USER      , array(self::ROLE_GUEST     )) // USER inherits from GUEST
             ->addRole(self::ROLE_SUPERVISOR, array(self::ROLE_USER      )) // SUPERVISOR inherits from USER
             ->addRole(self::ROLE_ADMIN     , array(self::ROLE_SUPERVISOR)) // ADMIN inherits from SUPERVISOR

             ->allow(self::ROLE_ADMIN) // ADMIN is allowed access to all resources with all priveleges.
             ->_addModule()
             ->_addModuleAdmin()
             ->_addModuleService()
             ->_addModuleSupervisor()
        ;
    }

    /**
     * Default Module
     */
    protected function _addModule()
    {
        $r = array();
        $r['about'   ] = self::getResource('about'   );
        $r['contact' ] = self::getResource('contact' );
        $r['error'   ] = self::getResource('error'   );
        $r['exercise'] = self::getResource('exercise');
        $r['index'   ] = self::getResource(          );
        $r['user'    ] = self::getResource('user'    );

        return $this->addResources($r)
                    ->allow(self::ROLE_ALL, $r['about'   ])
                    ->allow(self::ROLE_ALL, $r['contact' ])
                    ->allow(self::ROLE_ALL, $r['error'   ])
                    ->allow(self::ROLE_ALL, $r['exercise'])
                    ->allow(self::ROLE_ALL, $r['index'   ])
                    ->allow(self::ROLE_ALL, $r['user'    ])
        ;
    }

    /**
     * Admin Module
     *
     * @return Ahs_Acl
     */
    protected function _addModuleAdmin()
    {
        $r = array();
        $r['index'] = self::getResource('index', 'admin');

        return $this->addResources($r)
                    ->allow(self::ROLE_ALL, $r['index'])
        ;
    }

    /**
     * Service Module
     *
     * @return Ahs_Acl
     */
    protected function _addModuleService()
    {
        $module = 'service';
        $r = array();
        $r['attempt' ] = self::getResource('attempt' , $module);
        $r['exercise'] = self::getResource('exercise', $module);
        $r['group'   ] = self::getResource('group'   , $module);
        $r['index'   ] = self::getResource('index'   , $module);
        $r['module'  ] = self::getResource('module'  , $module);
        $r['test'    ] = self::getResource('test'    , $module);
        $r['set'     ] = self::getResource('set'     , $module);
        $r['user'    ] = self::getResource('user'    , $module);

        return $this->addResources($r)
                    ->allow(self::ROLE_ALL, $r['attempt' ])
                    ->allow(self::ROLE_ALL, $r['exercise'])
                    ->allow(self::ROLE_ALL, $r['group'   ])
                    ->allow(self::ROLE_ALL, $r['index'   ])
                    ->allow(self::ROLE_ALL, $r['module'  ])
                    ->allow(self::ROLE_ALL, $r['set'     ])
                    ->allow(self::ROLE_ALL, $r['test'    ])
                    ->allow(self::ROLE_ALL, $r['user'    ])
        ;
    }

    /**
     * Supervisor Module
     *
     * @return Ahs_Acl
     */
    protected function _addModuleSupervisor()
    {
        $module = 'supervisor';

        $r['account' ] = self::getResource('account' , $module);
        $r['groups'  ] = self::getResource('groups'  , $module);
        $r['index'   ] = self::getResource('index'   , $module);
        $r['sets'    ] = self::getResource('sets'    , $module);
        $r['settings'] = self::getResource('settings', $module);
        $r['users'   ] = self::getResource('users'   , $module);

        $p = array();
        $p['add'     ] = self::getPrivilege('add'     );
        $p['delete'  ] = self::getPrivilege('delete'  );
        $p['edit'    ] = self::getPrivilege('edit'    );
        $p['exercise'] = self::getPrivilege('exercise');
        $p['index'   ] = self::getPrivilege('index'   );
        $p['list'    ] = self::getPrivilege('list'    );
        $p['login'   ] = self::getPrivilege('login'   );
        $p['logout'  ] = self::getPrivilege('logout'  );
        $p['module'  ] = self::getPrivilege('module'  );
        $p['register'] = self::getPrivilege('register');

        return $this->addResources($r)
                    ->allow(self::ROLE_ALL       , $r['index'   ])
                    ->allow(self::ROLE_GUEST     , $r['account' ], array(
                        $p['index'   ],
                        $p['login'   ],
                        $p['register'],
                    ))
                    ->deny( self::ROLE_GUEST     , $r['account' ], array(
                        $p['logout'  ],
                    ))
                    ->allow(self::ROLE_SUPERVISOR, $r['account' ], array(
                        $p['logout'  ],
                    ))
                    ->deny( self::ROLE_SUPERVISOR, $r['account' ], array(
                        $p['login'   ],
                    ))
                    ->allow(self::ROLE_SUPERVISOR, $r['settings'], array(
                        $p['exercise'],
                        $p['index'   ],
                        $p['module'  ],
                    ))
                    ->allow(self::ROLE_SUPERVISOR, $r['users'   ], array(
                        $p['add'     ],
                        $p['delete'  ],
                        $p['edit'    ],
                        $p['index'   ],
                        $p['list'    ],
                    ))
                    ->allow(self::ROLE_SUPERVISOR, $r['groups'  ], array(
                        $p['add'     ],
                        $p['delete'  ],
                        $p['edit'    ],
                        $p['index'   ],
                        $p['list'    ],
                    ))
                    ->allow(self::ROLE_SUPERVISOR, $r['sets'    ], array(
                        $p['add'     ],
                        $p['delete'  ],
                        $p['edit'    ],
                        $p['index'   ],
                        $p['list'    ],
                    ))
        ;
    }

    /**
     * @param array $resources
     *
     * @return Ahs_Acl
     */
    public function addResources($resources = array()) {
        foreach ($resources as $resource) {
            $this->addResource($resource);
        }

        return $this;
    }

    /**
     * Get Controller class name as ACL Resource.
     *
     * @param string $controller Controller.
     * @param string $module Module where the controller is at.
     *
     * @return string Class name of the controller
     */
    public static function getResource($controller = 'index', $module = 'default')
    {
        $class_name = ucfirst($controller) . 'Controller';

        if ($module != 'default') {
            $class_name = ucfirst($module) . "_{$class_name}";
        }

        return $class_name;
    }

    /**
     * Get Action method name as ACL Privilege.
     *
     * @param string $action Action method of the controller.
     *
     * @return string Class name of the action method.
     */
    public static function getPrivilege($action = 'index')
    {
        $method_name = lcfirst($action) . 'Action';

        return $method_name;
    }
}
