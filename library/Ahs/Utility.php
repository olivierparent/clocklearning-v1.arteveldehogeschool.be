<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Utility
{
    const MESSAGE_DEFAULT = 'DEFAULT';
    const MESSAGE_ERROR   = 'ERROR';
    const MESSAGE_INFO    = 'INFO';
    const MESSAGE_SUCCESS = 'SUCCESS';

    /**
     * Return a message for the Backoffice.
     *
     * @param string $title Message title.
     * @param string $content Message content.
     * @param string $type const MESSAGE_DEFAULT, MESSAGE_ERROR, MESSAGE_INFO, MESSAGE_SUCCESS.
     * @return string
     */
    public static function getBackofficeMessage($title = '', $content = '', $type = self::MESSAGE_DEFAULT)
    {
        $translate = Zend_Registry::get('Zend_Translate');

        switch ($type) {
            case self::MESSAGE_ERROR:
                $class = ' alert-error';
                break;
            case self::MESSAGE_INFO:
                $class = ' alert-info';
                break;
            case self::MESSAGE_SUCCESS:
                $class = ' alert-success';
                break;
            default:
                $class = '';
                break;
        }

        return '<div class="alert alert-block' . $class . '" data-animation="true"><button class="close" data-dismiss="alert">&times;</button>'
             . '<h4>' . $translate->_($title  ) . '</h4>'
             . '<p>'  . $translate->_($content) . '</p>'
             . '</div>';
    }


    /**
     * Return a message for the Frontoffice.
     *
     * @param string $title Message title.
     * @param string $content Message content.
     * @param string $type const MESSAGE_DEFAULT, MESSAGE_ERROR, MESSAGE_INFO, MESSAGE_SUCCESS.
     * @return string
     */
    public static function getFrontofficeMessage($title = '', $content = '', $type = self::MESSAGE_DEFAULT)
    {
        $translate = Zend_Registry::get('Zend_Translate');

        switch ($type) {
            case self::MESSAGE_ERROR:
                $class = ' message-error';
                break;
            case self::MESSAGE_INFO:
                $class = ' message-info';
                break;
            case self::MESSAGE_SUCCESS:
                $class = ' message-success';
                break;
            default:
                $class = '';
                break;
        }

        return
            '<div class="ui-corner-all' . $class . '">'
            .   '<div class="ui-bar ui-bar-a">'
            .       '<h4>' . $translate->_($title) . '</h4>'
            .   '</div>'
            .   '<div class="ui-body ui-body-a">'
            .       '<p>' . $translate->_($content) . '</p>'
            .   '</div>'
            . '</div>'
        ;
    }

    /**
     * Calculates a hash code for the given string using an HMAC with SHA-512.
	 * Returns a 128 character string.
     *
     * @static
     * @param $data
     * @param string $algo
     * @return string
     */
    public static function hash($data, $algo = 'sha512')
    {
        $key  = '250179a1dd377c786ee19c56f0d4527d89e5f8b9cca9104896400c6f7015bd49';

        // Hash-based Message Authentication Code
        return hash_hmac($algo, $data, $key);
    }

    /**
     * Fetch and return the content of a remote file.
     *
     * @param string $url URL of the file.
     * @return string Content of the file.
     */
    public static function scrape($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);

        return nl2br($data);
    }
}
