<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * Abstract class for a Model Mapper.
 *
 * @category   Ahs
 * @package    Ahs_Model_Mapper
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Model_Mapper_Abstract extends Ahs_Model_Abstract
{
    /**
     * @var Zend_Db_Table_Abstract
     */
    protected $_dbTable;
    /**
     * @var string
     */
    protected $_dbTableClassName;

    /**
     * @return Zend_Db_Table_Abstract
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable($this->DbTableClassName);
        }
        return $this->_dbTable;
    }

    /**
     * @param mixed $dbTable
     * @return Zend_Db_Table_Abstract
	 * @throws ErrorException
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new ErrorException(
                Zend_Registry::get('Zend_Translate')->_('Invalid table data gateway provided')
            );
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    /**
     * @param Zend_Db_Table_Row_Abstract $row
     */
    abstract protected function _toObject(Zend_Db_Table_Row_Abstract $row = null);

    /**
     * @param Zend_Db_Table_Rowset_Abstract $rows
     * @return array
     */
    protected function _toObjects(Zend_Db_Table_Rowset_Abstract $rows = null)
    {
        $objects = array();

        if ($rows) {
            foreach ($rows as $row) {
                $objects[] = $this->_toObject($row);
            }
        }

        return $objects;
    }


//    protected function _toModel()
//    {
//
//    }
//
//    protected function _toModels()
//    {
//
//    }
}
