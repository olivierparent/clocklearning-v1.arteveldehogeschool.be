<?php
// application.ini: autoloaderNamespaces[] = "Ahs"
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * Abstract class for a Model.
 *
 * @category   Ahs
 * @package    Ahs_Model
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
abstract class Ahs_Model_Abstract extends Ahs_Object_Abstract
{
    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->_setOptions($options);
        }
    }

	/**
	 * @param array $options
	 */
	protected function _setOptions(array $options)
	{
		$methods = get_class_methods($this);
        foreach ($options as $key => $value) {
			$method_name = 'set' . ucfirst($key);
			if ( in_array($method_name, $methods) ) {
				$this->$method_name($value);
			} else {
                $property = ucfirst($key);
                $this->$property = $value;
            }
		}
	}

    /**
     * @param array $properties Array of Model properties.
     * @return array
     */
    public function toArray(array $properties)
    {
        $array = array();
        foreach ($properties as $property) {
            try {
                $getter = ucfirst($property);
                $array[$property] = $this->{$getter};
            } catch (Exception $e) {

            }
        }

        return $array;
    }

}
