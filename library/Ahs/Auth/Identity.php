<?php
/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
class Ahs_Auth_Identity extends Ahs_Object_Abstract
{
    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_role;

    public function getId()
    {
        throw new Zend_Exception('Use getAdminId(), getSupervisorId() or getUserId() instead.');
    }

    public function isAdmin()
    {
        return ($this->Role == Ahs_Acl::ROLE_ADMIN);
    }

    public function getAdminId()
    {
        if ($this->isAdmin() ) {
            return $this->_id;
        } else {
            throw new Zend_Exception('Role is not ' . Ahs_Acl::ROLE_ADMIN);
        }
    }

    public function setAdminId($id)
    {
        $this->_id   = (int) $id;
        $this->_role = Ahs_Acl::ROLE_ADMIN;
    }

    public function isSupervisor()
    {
        return ($this->Role == Ahs_Acl::ROLE_SUPERVISOR);
    }

    public function getSupervisorId()
    {
        if ($this->isSupervisor() ) {
            return $this->_id;
        } else {
            throw new Zend_Exception('Role is not ' . Ahs_Acl::ROLE_SUPERVISOR);
        }
    }

    public function setSuperviosrId($id)
    {
        $this->_id   = (int) $id;
        $this->_role = Ahs_Acl::ROLE_SUPERVISOR;
    }

    public function isUser()
    {
        return ($this->Role == Ahs_Acl::ROLE_USER);
    }


    public function getUserId()
    {
        if ($this->isUser() ) {
            return $this->_id;
        } else {
            throw new Zend_Exception('Role is not ' . Ahs_Acl::ROLE_USER);
        }
    }

    public function setUserId($id)
    {
        $this->_id   = (int) $id;
        $this->_role = Ahs_Acl::ROLE_USER;
    }
}
