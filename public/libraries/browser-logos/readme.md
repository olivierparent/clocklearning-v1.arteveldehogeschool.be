Browser logos
=============

- [Chrome](https://github.com/alrra/browser-logos/blob/master/chrome/chrome_256x256.png)
- [Internet Explorer](https://github.com/alrra/browser-logos/blob/master/internet-explorer/internet-explorer_256x256.png)
- [Firefox](https://github.com/alrra/browser-logos/blob/master/firefox/firefox_256x256.png)
- [Opera](https://github.com/alrra/browser-logos/blob/master/opera/opera_256x256.png)
- [Safari](https://github.com/alrra/browser-logos/blob/master/safari/safari_256x256.png)
