/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(function () {
/******************************************************************************/
$.fn.radioGroup = function (action)
{
    switch (action) {
        case "reset":
            this
                .find("input[type='radio']")
                    .removeAttr("checked")
                    .checkboxradio("refresh")
                    .checkboxradio("enable")
                    .end()
                .find("input[type='button']")
                    .button("disable")
            ;
            break;
        case "enable":
            this
                .find("input[type='button']")
                    .button("enable")
            ;
            break;
        case "disable":
            this
                .find("input[type='radio']")
                    .checkboxradio("disable")
                    .end()
                .find("input[type='button']")
                    .button("disable")
            ;
            break;
        default:
            break;
    }

    return this;
};
/******************************************************************************/
});
