/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(function () {
/******************************************************************************/
$.fn.rotatable = function ()
{
    var currentWidget = {
        "clock" : null,
        "hand"  : null,
        "center": {
            "x": null,
            "y": null
        }
    };

    var handlerUp = function (event) {
        console.log("handlerUp");
        currentWidget.hand
            .removeClass("active")
        ;
        currentWidget.clock
            .off("mousemove", handlerMouseMove)
            .off("mouseout" , handlerMouseOut )
            .off("mouseup"  , handlerMouseUp  )
            .setTime()
        ;
        currentWidget = {
            "clock"  : null,
            "hand"   : null,
            "center" : {
                "x" : null,
                "y" : null
            }
        };
    };

    var handlerMouseDown = function (event) {
        console.log("handlerMouseDown");
        currentWidget.hand  = $(this).parent();
        currentWidget.clock = $(this).parents("figure");
        var offset = currentWidget.clock.offset();
        currentWidget.center.x = offset.left + Math.round(currentWidget.clock.width()  / 2);
        currentWidget.center.y = offset.top  + Math.round(currentWidget.clock.height() / 2);

        currentWidget.clock
            .on("mousemove", handlerMouseMove)
            .on("mouseout" , handlerMouseOut )
            .on("mouseup"  , handlerMouseUp  )
        ;
        currentWidget.hand
            .addClass("active")
        ;
    };

    var handlerMouseUp = function (event) {
        console.log("handlerMouseUp");

        handlerUp(event);
    };

    var handlerMouseOut = function (event) {
        console.log("handlerMouseOut");

        handlerMouseUp(event);
    };

    var handlerMouseMove = function (event) {
        console.log("handlerMouseMove");

        var deg = ( Math.atan2(event.pageY - currentWidget.center.y, event.pageX - currentWidget.center.x) * 180 / Math.PI ) + 90;
        if (deg < 0) {
            deg += 360;
        }

        //console.log("move\tx: " + event.pageX + "\ty: " + event.pageY + "\tdeg: " + deg);
        //debug("move x: " + event.pageX + ", y: " + event.pageY + ", deg: " + Math.round(deg) + "<br>");

        currentWidget.hand
            .rotate(deg)
        ;

        if ( currentWidget.hand.hasClass("hour") ) {
            //console.log("hours: " + toTime(deg, "m") );
            currentWidget.clock
                .attr("data-h", toTime(deg, "h") )
            ;
        } else if ( currentWidget.hand.hasClass("minute") ) {
            //console.log("minutes: " + toTime(deg, "m") );
            currentWidget.clock
                .attr("data-m", toTime(deg, "m") )
            ;
        }
        //handlerMove(event);
    };

    /**
     * @param int deg
     * @param int unit
     */
    function toTime(deg, unit) {
        var increment = (unit == "h") ? 30 : 6;
        var remainder = deg % increment;
        deg = Math.round( (remainder <= increment / 2 ? 0 : increment) + deg - remainder);
        var time = deg / increment;
        if (increment * time == 360) {
            time = 0;
        }
        return time;
    }

    // If browser supports touch events
    if (Modernizr.touch) {
        $(document).on("touchmove", function (event) {
            event.preventDefault();
        });

        this
            .on("vmousemove mousemove", handlerMouseMove)
            .on("vmouseup vmouseout"  , handlerUp  )
        ;
    } else {
        this
            .on("mousedown", handlerMouseDown)
        ;
    }

    return this;
};
/******************************************************************************/
});
