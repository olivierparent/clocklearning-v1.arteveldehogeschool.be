/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(function () {
/******************************************************************************/
$.fn.setTime = function ()
{
    var h = this.attr("data-h"),
        m = this.attr("data-m"),
        precise = this.hasClass("precise"); // uses precise hour calculation when this class is set on <figure>
    if (typeof h === "undefined" || typeof m === "undefined" || !/^([01]?\d|2[0-3]):([0-5]?\d)$/.test(h + ":" + m) ) {
        var d = new Date();
        if (typeof h === "undefined" || !/^([01]?\d|2[0-3])$/.test(h)) {
            h = d.getHours();
        }
        if (typeof m === "undefined" || !/^([0-5]?\d)$/.test(m)) {
            m = d.getMinutes();
        }
    }
    if (this.hasClass("analogue") ) {
        console.log("analogue");
        var hdeg = (precise) ? Math.round(h * 30 + m / 2) : h * 30,
            mdeg = m * 6;

        this
            .find(".hour")
                .rotate(hdeg)
                .end()
            .find(".minute")
                .rotate(mdeg)
        ;

    } else if (this.hasClass("digital") ) {
        console.log("digital");
        this
            .find(".digit")
            .removeClass("num-0 num-1 num-2 num-3 num-4 num-5 num-6 num-7 num-8 num-9")
        ;
        if (h.toString().length <= 1) {
            this
                .find(".digit.hour.tens")
                    .addClass("num-0")
                    .end()
                .find(".digit.hour.ones")
                    .addClass("num-" + h)
            ;
        } else {
            var ah = h.toString().split("");
            this
                .find(".digit.hour.tens")
                    .addClass("num-" + ah[0])
                    .end()
                .find(".digit.hour.ones")
                    .addClass("num-" + ah[1])
            ;
        }
        if (m.toString().length <= 1) {
            this
                .find(".digit.minute.tens")
                    .addClass("num-0")
                    .end()
                .find(".digit.minute.ones")
                    .addClass("num-" + m)
            ;
        } else {
            var am = m.toString().split("");
            this
                .find(".digit.minute.tens")
                    .addClass("num-" + am[0])
                    .end()
                .find(".digit.minute.ones")
                    .addClass("num-" + am[1])
            ;
        }
    }
    return this;
};
/******************************************************************************/
});
