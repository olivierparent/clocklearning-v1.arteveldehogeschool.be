/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["ahs"], function () {
/******************************************************************************/
Ahs.Debug = Ahs.Debug || (function ()
{
    /* PROPERTIES */

    var _active = true;
    var TAB = "  ";

    /* METHODS */

    var _indentation = function (indent)
    {
        var indentationString = "";
        if (typeof indent !== "undefined") {
           for (var i = 0; i < indent; i++) {
               indentationString += TAB;
           }
        }

        return indentationString;
    };

    var error = function (message, indent)
    {
        if (_active) {
            console.log("%c" + _indentation(indent) + message, "color:white;background:red");
        }
    };

    var info = function (message, indent)
    {
        if (_active) {
            console.log("%c" + _indentation(indent) + message, "color:white;background:lightblue");
        }
    };

    var log = function (message, indent)
    {
        if (_active) {
            console.log(_indentation(indent) + message);
        }
    };

    var prepend = function (message, indent)
    {
        $(".debug").prepend(_indentation(indent) + message);
    };

    var success = function (message, indent)
    {
        if (_active) {
            console.log("%c" + _indentation(indent) + message, "color:white;background:green");
        }
    };

    var warn = function (message, indent)
    {
        if (_active) {
            console.log("%c" + _indentation(indent) + message, "color:white;background:orange");
        }
    };

    /* PUBLIC PROPERTIES & METHODS */

    return {
        "error"  : error,
        "info"   : info,
        "log"    : log,
        "prepend": prepend,
        "success": success,
        "warn"   : warn
    };
})();
/******************************************************************************/
});
