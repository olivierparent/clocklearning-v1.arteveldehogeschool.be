/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["ahs", "ahs/debug", "ahs/dialog", "ahs/exercise", "ahs/uri"], function () {
/******************************************************************************/
Ahs.Service = Ahs.Service || (function ()
{
    /* PROPERTIES */

    /* METHODS */

    var getExercise = function (initApplication)
    {
        var service = "exercise/" + window.exerciseType.toLowerCase();
        $.getJSON(Ahs.Uri.service(service))
            .success(function (exercise) {
                Ahs.Exercise.init(exercise);
                initApplication();
                Ahs.Debug.info(Ahs.Exercise);
            })
            .error(function () {
                Ahs.Dialog.error("#dialog-error", Ahs.Uri.service(service));
            })
        ;
    };

    var putAttempt = function ()
    {
        if (!Ahs.Exercise.isDemo()) {
            var data = JSON.stringify(Ahs.Exercise.data);
            Ahs.Debug.info(data);
            $.ajax({
                "url": Ahs.Uri.service("attempt/"),
                "type": "PUT",
                "contentType": "application/json",
                "data": data,
                "success": function (result) {
                    var data = JSON.parse(result);
                    Ahs.Exercise.setId(data.id);
                    Ahs.Debug.success(Ahs.Exercise.data.id);
                }
            });
        }
        Ahs.Debug.info("Demo exercise: Ahs.Service.putAttempt() skipped")
    }

    /* PUBLIC PROPERTIES & METHODS */

    return {
        "getExercise": getExercise,
        "putAttempt": putAttempt
    };
})();
/******************************************************************************/
});
