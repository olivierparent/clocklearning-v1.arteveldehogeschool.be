/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define([
    "ahs",
    "ahs/debug",
    "ahs/feedback",
    "ahs/time"
], function () {
/******************************************************************************/
Ahs.Exercise = Ahs.Exercise || (function ()
{
    /* PROPERTIES */

    var data = {};
    var feedback = {};

    /* METHODS */

    var _now = function ()
    {
        return new Date().getTime();
    };

    var _addFeedback = function (that)
    {
        Ahs.Feedback.addScenario(that.data.attempts.length);
    };

    var duration = function ()
    {
        // Duration in seconds
        return Math.round((this.data.timeEnd - this.data.timeStart) / 1000);
    };

    var getAnswer = function (index)
    {
        try {
            return this.data.answers[index];
        } catch (e) {
            Ahs.Debug.error(e);
        }
    };

    var getQuestion = function (index)
    {
        try {
            return this.data.questions[index];
        } catch (e) {
            Ahs.Debug.error(e);
        }
    };

    var init = function (exercise)
    {
        console.log(this);
        this.data     = exercise.data;
        this.feedback = exercise.feedback;
    };

    var isAttempt1 = function ()
    {
        if (this.data.attempts.length         === 1           &&
            typeof this.feedback              !== "undefined" &&
            typeof this.feedback.verbalVisual !== "undefined" &&
            $("#page-exercise").attr('data-feedback').indexOf("verbal") >= 0 ) {
            Ahs.Debug.warn("ATTEMPT 1 - FEEDBACK: Verbal");
            _addFeedback(this);
            return true;
        } else {
            return false;
        }
    };

    var isAttempt2 = function ()
    {
        if (this.data.attempts.length         === 2           &&
            typeof this.feedback              !== "undefined" &&
            typeof this.feedback.verbalVisual !== "undefined" &&
            $("#page-exercise").attr('data-feedback').indexOf("visual") >= 0 ) {
            $("#dialog").text("");
            Ahs.Debug.warn("ATTEMPT 2 - FEEDBACK: Verbal + Visual");
            _addFeedback(this);
            return true;
        } else {
            return false;
        }
    };

    var isAttempt3 = function ()
    {
        if (this.data.attempts.length      === 3           &&
            typeof this.feedback           !== "undefined" &&
            typeof this.feedback.modelling !== "undefined" &&
            $("#page-exercise").attr('data-feedback').indexOf("modelling") >= 0 ) {
            $("#dialog").text("");
            Ahs.Debug.warn("ATTEMPT 3 - FEEDBACK: Modelling");
            _addFeedback(this);
            return true;
        } else {
            return false;
        }
    };

    var isCorrect = function ()
    {
        return this.data.correct;
    };

    var isDemo = function ()
    {
        return $("#page-exercise").attr("data-demo") === "true";
    };

    var setId = function (id)
    {
        this.data.id = id;
    };

    var setTimeEnd = function ()
    {
        this.data.timeEnd = _now();
    };

    var setTimeStart = function ()
    {
        this.data.timeStart = _now();
    };

    var toJSON = function ()
    {
        return JSON.stringify(this.data);
    };

    var processAnswer = function (answer)
    {
        var attempt = this.data.answers.indexOf(answer);
        this.data.attempts.push(attempt);

        this.data.correct = attempt === this.data.solution[0];
    };

    var processAnswer12Hours = function (answer)
    {
        this.data.attempts.push(answer);

        this.data.correct = Ahs.Time.convert12(answer) === Ahs.Time.convert12(this.data.answers[this.data.solution[0]]);
    };

    var processAnswerPairs = function (pairs)
    {
        var success = true;
        for (var i in pairs) {
            if (this.data.solution[pairs[i].q] !== pairs[i].a) {
                success = false;
                break;
            }
        }
        this.data.correct = success;
    };

    /* PUBLIC PROPERTIES & METHODS */
    return {
        "data": data,
        "duration": duration,
        "getAnswer": getAnswer,
        "getQuestion": getQuestion,
        "init": init,
        "isAttempt1": isAttempt1,
        "isAttempt2": isAttempt2,
        "isAttempt3": isAttempt3,
        "isCorrect": isCorrect,
        "isDemo": isDemo,
        "feedback": feedback,
        "setId": setId,
        "setTimeEnd": setTimeEnd,
        "setTimeStart": setTimeStart,
        "toJSON": toJSON,
        "processAnswer": processAnswer,
        "processAnswer12Hours": processAnswer12Hours,
        "processAnswerPairs": processAnswerPairs
    };
})();
/******************************************************************************/
});
