/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["ahs"], function () {
/******************************************************************************/
Ahs.Time = Ahs.Time || (function ()
{
    /* PROPERTIES */

    var SEPARATOR = ":";

    /* METHODS */

    var _format = function (n)
    {
        return (n < 10 ? "0" : "") + n;
    };

    var format = function (h, m)
    {
        h = parseInt(h, 10);
        m = parseInt(m, 10);

        return _format(h) + SEPARATOR + _format(m);
    };

    var convert12 = function (time)
    {
        time = time.split(SEPARATOR);
        var h = parseInt(time[0], 10);
        var m = parseInt(time[1], 10);
        if (11 < h ) {
            h -= 12;
        }

        return format(h, m);
    };

    /* PUBLIC PROPERTIES & METHODS */

    return {
        "format"   : format,
        "convert12": convert12
    };
})();
/******************************************************************************/
});
