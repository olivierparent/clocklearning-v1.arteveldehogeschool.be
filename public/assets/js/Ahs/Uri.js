/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["ahs"], function () {
/******************************************************************************/
Ahs.Uri = Ahs.Uri || (function ()
{
    /* PROPERTIES */

    var audioExtension = Modernizr.audio.mp3 == 'probably' ? "mp3" : "ogg";

    var baseUrl = window.baseUrl;

    // @todo Temporary force to "nl"
    //var lang = $("html").attr("lang") + "/";
    var lang = "nl/";

    var videoExtension = Modernizr.video.h264 ? "mp4" : "theora.ogv";

    /* METHODS */

    var audio = function (urn)
    {
        return baseUrl + "/assets/audio/" + lang + urn;
    };

    var js = function (urn)
    {
        return baseUrl + "/assets/js/" + urn;
    };

    var service = function (urn)
    {
        var d = new Date();
        return baseUrl + "/service/" + (typeof(urn) === "undefined" ? "exercise" : urn) + "?" + d.getTime();
    };

    var video = function (urn)
    {
        return baseUrl + "/assets/video/" + urn;
    };

    /* PUBLIC PROPERTIES & METHODS */
    return {
        "audio"         : audio,
        "audioExtension": audioExtension,
        "js"            : js,
        "service"       : service,
        "video"         : video,
        "videoExtension": videoExtension
    };
})();
/******************************************************************************/
});
