/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["ahs"], function () {
/******************************************************************************/
Ahs.Dialog = Ahs.Dialog || (function ()
{
    /* PROPERTIES */

    /* METHODS */

    var error = function (dialogId, message)
    {
        $(dialogId)
            .find("b")
                .each(function (index) {
                    $(this).text(message);
                })
        ;
        $.mobile.changePage(dialogId, {transition: "pop"});
    };

    var show = function (dialogId)
    {
        $.mobile.changePage(dialogId, {transition: "flip"});

        $("[title=Close]").hide();

        $(dialogId)
            .find("video")
                .first()
                    .queue(function() {
                        this.play();
                    })
        ;

        $(dialogId)
            .find("audio")
                .on("ended", function() {
                    $(this)
                        .next("audio")
                        .delay(1000)
                        .queue(function() {
                            this.play();
                        });
                })
                .first()
                    .queue(function() {
                        this.play();
                    })
        ;
    };

    /* PUBLIC PROPERTIES & METHODS */
    return {
        "error": error,
        "show" : show
    };
})();
/******************************************************************************/
});
