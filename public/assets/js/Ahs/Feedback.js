/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define([
    "ahs",
    "ahs/debug",
    "ahs/exercise",
    "jquery/radiogroup"
], function () {
/******************************************************************************/
Ahs.Feedback = Ahs.Feedback || (function ()
{
    /* PROPERTIES */

    var _audio = new Audio();

    var _head = 0;

    var _list = [];

    var _extension = "ogg";

    function FeedbackAudio(src)
    {
        this.src = src;
    }

    FeedbackAudio.prototype.toString = function ()
    {
        return this.src;
    };

    $(_audio).on("ended", function() {
        Ahs.Debug.warn("EVENT CAPTURED: 'ended' dispached by Audio()", 1);
        playNext();
    });

    /* METHODS */

    var addScenario = function (level)
    {
        var feedback = (level <= 2) ? Ahs.Exercise.feedback.verbalVisual : Ahs.Exercise.feedback.modelling;
        if (1 < level) {
            clear();
        }
        _addAction("__DISABLE__");
        for (var object in feedback) {
            for (var property in feedback[object]) {
                for (var key in feedback[object][property]) {
                    var value = feedback[object][property][key];
                    switch (key) {
                        case "__FILE__":
                            _addAudio(value);
                            break;
                        case "__FILES__":
                            for (index in value) {
                                _addAudio(value[index]);
                            }
                            break;
                        case "__TRANSCRIPTION__":
                            _addTranscription(value);
                            break;
                        case "__ACTION__":
                            if (1 < level) {
                                _addAction(value);
                            }
                            break;
                        case "__PAUSE__":
                            _addPause(value);
                            break;
                        default:
                            Ahs.Debug.error("No feedback");
                            break;
                    }
                }
            }
        }
        _addAction("__ENABLE__");
        playNext();
    };

    var _addAction = function (action)
    {
        var length = _list.length;

        Ahs.Debug.log(action, 2);

        switch (action) {
            case "__DISABLE__":
                _list[length] = function() {
                    $("#exercise-answers, #exercise-questions").radioGroup("disable");
                    playNext();
                };
                break;
            case "__ENABLE__":
                _list[length] = function() {
                    $("#exercise-answers, #exercise-questions")
                        .radioGroup("reset")
                    ;
                    playNext();
                };
                break;
            case "__HIGHLIGHT_HOURS_ON__":
                _list[length] = function() {
                    $(".clock-widget .hand.hour")
                        .addClass("animate")
                    ;
                    playNext();
                };
                break;
            case "__HIGHLIGHT_HOURS_OFF__":
                _list[length] = function() {
                    $(".clock-widget .hand.hour")
                        .removeClass("animate")
                    ;
                    playNext();
                };
                break;
            case "__HIGHLIGHT_MINUTES_ON__":
                _list[length] = function() {
                    $(".clock-widget .hand.minute")
                        .addClass("animate")
                    ;
                    playNext();
                };
                break;
            case "__HIGHLIGHT_MINUTES_OFF__":
                _list[length] = function() {
                    $(".clock-widget .hand.minute")
                        .removeClass("animate")
                    ;
                    playNext();
                };
                break;
            default:
                break;
        }
        Ahs.Debug.log("add[" + length +  "] Action: " + action, 1);
    };

    var _addAudio = function (file)
    {
        file = file + "." + _extension;
        var length = _list.length;
        _list[length] = new FeedbackAudio(Ahs.Uri.audio(file));
        _audio.volume = 1;
        Ahs.Debug.log("add[" + length +  "] Audio: " + file, 1);
        Ahs.Debug.log(Ahs.Uri.audio(file), 2);
    };

    var _addPause = function (milliseconds)
    {
        var length = _list.length;
        _list[length] = function() {
            Ahs.Debug.log("__PAUSE__", 2);
            setTimeout(function() {
                playNext();
            }, milliseconds);
        };
        Ahs.Debug.log("add[" + length +  "] Pause: " + milliseconds + "ms", 1);
    };

    var _addTranscription = function (transcription)
    {
        var length = _list.length;
        _list[length] = transcription;
        Ahs.Debug.log("add[" + length +  "] Transcription: " + transcription, 1);
    };

    var clear = function ()
    {
        Ahs.Debug.info("CLEAR FEEDBACK");

        $(".clock-widget .hand")
            .removeClass("animate")
        ;

        // Clear _list and reset _head
        _list = [];
        _head = 0;
    };

    var playNext = function ()
    {
        Ahs.Debug.info("playNext");
        if (_head < _list.length) {
            Ahs.Debug.log("play[" + _head + "]: " + typeof(_list[_head]), 1);

            if (_list[_head] instanceof FeedbackAudio) {
                Ahs.Debug.log("FeedbackAudio ", 2);
                _audio.src = _list[_head++];
                _audio.play();
            } else {
                switch (typeof(_list[_head])) {
                    case "string":
                        $("#dialog")
                            .append('<p>' + _list[_head] + '</p>')
                                .find("p")
                                    .last()
                                        .hide()
                                        .show("slow")
                        ;
                        _head++;
                        playNext();
                        break;
                    case "function":
                        _list[_head++]();
                        break;
                    default:
                        _head++;
                        playNext();
                        break;
                }
            }
        }
    };

    var setExtension = function (extension)
    {
        _extension = extension;
    };

    /* PUBLIC PROPERTIES & METHODS */

    return {
        "addScenario" : addScenario,
        "clear"       : clear,
        "playNext"    : playNext,
        "setExtension": setExtension
    };
})();
/******************************************************************************/
});
