/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
window.baseUrl = document.getElementsByTagName("body")[0].getAttribute("data-base-url");

require.config({
    "baseUrl": window.baseUrl + "/assets/js",
    "paths": {
        "ahs": "Ahs/Ahs",
        "ahs/debug": "Ahs/Debug",
        "ahs/dialog": "Ahs/Dialog",
        "ahs/exercise": "Ahs/Exercise",
        "ahs/feedback": "Ahs/Feedback",
        "ahs/mascot": "Ahs/Mascot",
        "ahs/service": "Ahs/Service",
        "ahs/time": "Ahs/Time",
        "ahs/uri": "Ahs/Uri",
        "bootstrap": "//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min",
//        "jquery": "//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min",
        "jquery": "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min",
        "jquery/radiogroup": "jQuery/fn/radioGroup",
        "jquery/rotatable": "jQuery/fn/rotatable",
        "jquery/rotate": "jQuery/fn/rotate",
        "jquery/settime": "jQuery/fn/setTime",
        "jquery-mobile": "//cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.3.2/jquery.mobile",
        "knockout": "//cdnjs.cloudflare.com/ajax/libs/knockout/3.1.0/knockout-min",
        "less": "//cdnjs.cloudflare.com/ajax/libs/less.js/1.7.0/less.min"
    },
    "shim": {
        "bootstrap": ["jquery"]
    }
});

require(["less"], function () {
    less.env = "development"; // Do not use local storage to save processed CSS
//    less.watch(); // Apply changes immediately without browser refresh
});
