/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define([
    "ahs/debug",
    "ahs/dialog",
    "ahs/exercise",
    "ahs/feedback",
    "ahs/mascot",
    "ahs/service",
    "ahs/uri",
    "jquery/rotatable",
    "jquery/rotate",
    "jquery/settime"
], function () {
/******************************************************************************/
$(document).ready(function () {
    /**
     * Initialize application
     */
    Ahs.Debug.info(Modernizr.touch ? "touch" : "no-touch");
    Ahs.Debug.info(window.exerciseType);
    Ahs.Feedback.setExtension(Ahs.Uri.audioExtension);

    var initApplication = function ()
    {
        $("#exercise-questions")
            .find("input[type='checkbox']")
                .each(function (index) {
                    $(this).attr("value", Ahs.Exercise.getQuestion(index));
                })
                .end()
            .find("label > b")
                .each(function (index) {
                    $(this).text(Ahs.Exercise.getQuestion(index));
                })
        ;
        $("#exercise-answers")
            .find("input[type='checkbox']")
                .each(function (index) {
                    $(this).attr("value", Ahs.Exercise.getAnswer(index));
                })
                .end()
            .find("figure")
                .each(function (index) {
                    var answer = Ahs.Exercise.getAnswer(index).split(":");
                    $(this)
                        .attr("data-h", answer[0])
                        .attr("data-m", answer[1])
                        .setTime()
                    ;
                })
        ;

        $(".controls .control > div").rotatable();

        var pairs = [];
        $("#exercise-questions").find("input").each(function () {
            pairs.push({q: null, a: null});
        });

        $("#exercise-questions, #exercise-answers")
            .find("label").on("click touchstart", function () {
                var id = $(this).parents("form").get(0).id;

                if (id === "exercise-questions") {
//                    Ahs.Debug.log("exercise-questions");
                    if ($(this).hasClass("ui-checkbox-off")) {
//                        Ahs.Debug.log("on");
                        for (var i in pairs) {
                            if (pairs[i].q === null) {
                                pairs[i].q = $("#exercise-questions").find("label").index(this);
                                $(this).addClass("pair-" + (parseInt(i, 10) + 1))
                                break;
                            }
                        }
                    } else if ($(this).hasClass("ui-checkbox-on")) {
//                        Ahs.Debug.log("off");
                        for (var i in pairs) {
                            if (pairs[i].q === $("#exercise-questions").find("label").index(this)) {
                                if (pairs[i].a !== null) {
                                    $("#exercise-answers").find("input")
                                        .eq(pairs[i].a)
                                            .removeAttr("checked")
                                            .checkboxradio("refresh")
                                    ;
                                }
                                $("#exercise-questions, #exercise-answers").find("label").removeClass("pair-" + (parseInt(i, 10) + 1));
                                pairs[i] = {q: null, a: null};
                                break;
                            }
                        }
                    }
                } else if (id === "exercise-answers") {
//                    Ahs.Debug.log("exercise-answers");
                    if ($(this).hasClass("ui-checkbox-off")) {
//                       Ahs.Debug.log("on");
                       for (var i in pairs) {
                           if (pairs[i].a === null) {
                               pairs[i].a = $("#exercise-answers").find("label").index(this);
                               $(this).addClass("pair-" + (parseInt(i, 10) + 1));
                               break;
                           }
                       }
                    } else if ($(this).hasClass("ui-checkbox-on")) {
//                        Ahs.Debug.log("off");
                        for (var i in pairs) {
                            if (pairs[i].a === $("#exercise-answers").find("label").index(this)) {
                                if (pairs[i].q !== null) {
                                    $("#exercise-questions").find("input")
                                        .eq(pairs[i].q)
                                            .removeAttr("checked")
                                            .checkboxradio("refresh")
                                    ;
                                }
                                $("#exercise-questions, #exercise-answers").find("label").removeClass("pair-" + (parseInt(i, 10) + 1));
                                pairs[i] = {q: null, a: null};
                                break;
                            }
                        }
                    }
                }

                // Enable submit button when all pairs have been made.
                var enable = "enable";
                for (i in pairs) {
                    if (pairs[i].q === null || pairs[i].a === null) {
                        enable = "disable";
                        break;
                    }
                }
                $("#exercise-answers").radioGroup(enable);
                Ahs.Debug.log(enable);
            })
        ;

        $(document)
            .on("click touchstart", "#exercise-answers input[type='button']", function () {
                Ahs.Exercise.setTimeEnd();

                Ahs.Exercise.processAnswerPairs(pairs);

                Ahs.Service.putAttempt();

                if (Ahs.Exercise.isCorrect()) {
                    /**
                     * Exercise completed: SUCCESS
                     */
                    $("#dialog-success").find("[data-role='content'] h2 + p > b")
                        .text(Ahs.Exercise.duration());

                    $("#exercise").radioGroup("reset");

                    Ahs.Debug.success("SUCCESS");
                    Ahs.Dialog.show("#dialog-success");
                } else {
                    /**
                     * Exercise completed: INCORRECT
                     */
                    if (Ahs.Exercise.isAttempt1()) {
                        /**
                         * ATTEMPT 1 - FEEDBACK: Verbal
                         */
                        Ahs.Mascot.play("rika-tika/sad_01." + Ahs.Uri.videoExtension);
                    } else if (Ahs.Exercise.isAttempt2()) {
                        /**
                         * ATTEMPT 2 - FEEDBACK: Verbal + Visual
                         */
                        Ahs.Mascot.play("rika-tika/sad_02." + Ahs.Uri.videoExtension);
                    } else if (Ahs.Exercise.isAttempt3()) {
                        /**
                         * ATTEMPT 3 - FEEDBACK: Modelling
                         */
                        Ahs.Mascot.play("rika-tika/sad_01." + Ahs.Uri.videoExtension);
                    } else {
                        /**
                         * ATTEMPT 4 - FAIL
                         */
                        Ahs.Debug.error("FAIL");
                        Ahs.Feedback.clear();
                        Ahs.Dialog.show("#dialog-fail");
                    }
                }
            })
        ;
        Ahs.Exercise.setTimeStart();
    };

    Ahs.Service.getExercise(initApplication);
});
/******************************************************************************/
});
