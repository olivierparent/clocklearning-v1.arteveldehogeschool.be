/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define([
    "ahs/debug",
    "ahs/dialog",
    "ahs/exercise",
    "ahs/feedback",
    "ahs/mascot",
    "ahs/service",
    "ahs/uri",
    "jquery/rotatable",
    "jquery/rotate",
    "jquery/settime"
], function () {
/******************************************************************************/
$(document).ready(function () {
    Ahs.Debug.info(Modernizr.touch ? "touch" : "no-touch");
    Ahs.Debug.info(window.exerciseType);
    Ahs.Feedback.setExtension(Ahs.Uri.audioExtension);

    /**
     * Initialize application
     */
    var initApplication = function ()
    {
        $("#exercise-questions")
            .find("figure")
                .each(function (index) {
                    var question = Ahs.Exercise.getQuestion(index).split(":");
                    $(this)
                        .attr("data-h", question[0])
                        .attr("data-m", question[1])
                        .setTime()
                    ;
                })
        ;

        $("#exercise-answers")
            .find("input[type='radio']")
                .each(function (index) {
                    $(this).attr("value", Ahs.Exercise.getAnswer(index));
                })
                .end()
            .find("figure")
                .each(function (index) {
                    var answer = Ahs.Exercise.getAnswer(index).split(":");
                    $(this)
                        .attr("data-h", answer[0])
                        .attr("data-m", answer[1])
                        .setTime()
                    ;
                })
        ;

        $(".controls .control > div").rotatable();

        $(document)
            .on("click touchstart", "#exercise-answers input[type='button']", function () {
                //$("#exercise-answers").radioGroup("disable");
                Ahs.Exercise.setTimeEnd();

                var answer  = $("#exercise-answers").find("input:checked").val();
                Ahs.Exercise.processAnswer(answer);

                Ahs.Service.putAttempt();

                if (Ahs.Exercise.isCorrect()) {
                    /**:)
                     * Exercise completed: SUCCESS
                     */
                    $("#dialog-success").find("[data-role='content'] h2 + p > b")
                        .text(Ahs.Exercise.duration());

                    $("#exercise-answers").radioGroup("reset");

                    Ahs.Debug.success("SUCCESS");
                    Ahs.Dialog.show("#dialog-success");
                } else {
                    /**
                     * Exercise completed: INCORRECT
                     */
                    if (Ahs.Exercise.isAttempt1()) {
                        /**
                         * ATTEMPT 1 - FEEDBACK: Verbal
                         */
                        Ahs.Mascot.play("rika-tika/sad_01." + Ahs.Uri.videoExtension);
//                        Ahs.Feedback.addScenario(Ahs.Exercise.data.attempts);
                    } else if (Ahs.Exercise.isAttempt2()) {
                        /**
                         * ATTEMPT 2 - FEEDBACK: Verbal + Visual
                         */
                        Ahs.Mascot.play("rika-tika/sad_02." + Ahs.Uri.videoExtension);
//                        Ahs.Feedback.addScenario(Ahs.Exercise.data.attempts);
                    } else if (Ahs.Exercise.isAttempt3()) {
                        /**
                         * ATTEMPT 3 - FEEDBACK: Modelling
                         */
                        Ahs.Mascot.play("rika-tika/sad_01." + Ahs.Uri.videoExtension);
//                        Ahs.Feedback.addScenario(Ahs.Exercise.data.attempts);
                    } else {
                        /**
                         * ATTEMPT 4 - FAIL
                         */
                        Ahs.Debug.error("FAIL");
                        Ahs.Feedback.clear();
                        Ahs.Dialog.show("#dialog-fail");
                    }
                }
            })
            .on("click touchstart", "#exercise-answers .ui-radio", function () {
                $("#exercise-answers").radioGroup("enable");
            })
        ;
        Ahs.Exercise.setTimeStart();
    };

    Ahs.Service.getExercise(initApplication);
});
/******************************************************************************/
});
