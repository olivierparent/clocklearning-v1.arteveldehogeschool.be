/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define([
    "ahs/debug",
    "ahs/dialog",
    "ahs/exercise",
    "ahs/feedback",
    "ahs/mascot",
    "ahs/service",
    "ahs/time",
    "ahs/uri",
    "jquery/rotatable",
    "jquery/rotate",
    "jquery/settime"
], function () {
/******************************************************************************/
$(document).ready(function () {
    Ahs.Debug.info(Modernizr.touch ? "touch" : "no-touch");
    Ahs.Debug.info(window.exerciseType);
    Ahs.Feedback.setExtension(Ahs.Uri.audioExtension);

    /**
     * Initialize application
     */
    var initApplication = function ()
    {
        var question = Ahs.Exercise.getQuestion(0).split(":");
        $("#exercise-questions").find(".clock-widget").each(function () {
            $(this)
                .attr("data-h", question[0])
                .attr("data-m", question[1])
                .setTime()
            ;
        });
        $("#exercise-answers").find(".clock-widget").each(function () {
            $(this)
                .attr("data-h", 12)
                .attr("data-m",  0)
                .setTime()
            ;
        });

        $(document)
            .on("mousemove touchmove", "#slider-h + .ui-slider-track .ui-slider-handle", function (){
                $("#clock-answer-1").attr('data-h', $(this).attr("aria-valuenow"));
                console.log($(this).attr("aria-valuenow"));
                $("#exercise-answers").find(".clock-widget").each(function () {
                    $(this).setTime();
                });
            })
            .on("mousemove touchmove", "#slider-m + .ui-slider-track .ui-slider-handle", function (){
                $("#clock-answer-1").attr('data-m', $(this).attr("aria-valuenow"));
                console.log($(this).attr("aria-valuenow"));
                $("#exercise-answers").find(".clock-widget").each(function () {
                    $(this).setTime();
                });
            })
            .on("click touchstart", "#exercise input[type='button']", function () {
                Ahs.Exercise.setTimeEnd();

                var answer = Ahs.Time.format($("#clock-answer-1").attr('data-h'), $("#clock-answer-1").attr('data-m'));
                Ahs.Exercise.processAnswer12Hours(answer);

                Ahs.Service.putAttempt();

                if (Ahs.Exercise.isCorrect()) {
                    /**
                     * Exercise completed: SUCCESS
                     */
                    $("#dialog-success").find("[data-role='content'] h2 + p > b")
                        .text(Ahs.Exercise.duration());

                    Ahs.Debug.success("SUCCESS");
                    Ahs.Dialog.show("#dialog-success");
                } else {
                    /**
                     * Exercise completed: INCORRECT
                     */
                    if (Ahs.Exercise.isAttempt1()) {
                        /**
                         * ATTEMPT 1 - FEEDBACK: Verbal
                         */
                        Ahs.Mascot.play("rika-tika/sad_01." + Ahs.Uri.videoExtension);
                    } else if (Ahs.Exercise.isAttempt2()) {
                        /**
                         * ATTEMPT 2 - FEEDBACK: Verbal + Visual
                         */
                        Ahs.Mascot.play("rika-tika/sad_02." + Ahs.Uri.videoExtension);
                    } else if (Ahs.Exercise.isAttempt3()) {
                        /**
                         * ATTEMPT 3 - FEEDBACK: Modelling
                         */
                        Ahs.Mascot.play("rika-tika/sad_01." + Ahs.Uri.videoExtension);
                    } else {
                        /**
                         * ATTEMPT 4 - FAIL
                         */
                        Ahs.Debug.error("FAIL");
                        Ahs.Feedback.clear();
                        Ahs.Dialog.show("#dialog-fail");
                    }
                }
            })
            .on("mousemove touchmove", ".ui-slider-handle", function () {
                $("#exercise").radioGroup("enable");
            })
        ;
        Ahs.Exercise.setTimeStart();
    };

    Ahs.Service.getExercise(initApplication);
});
/******************************************************************************/
});
