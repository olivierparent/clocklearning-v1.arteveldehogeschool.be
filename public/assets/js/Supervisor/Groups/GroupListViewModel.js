/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["jquery", "knockout", "./GroupViewModel", "./UserViewModel"],
function ($, ko, GroupViewModel, UserViewModel) {
/******************************************************************************/
return function GroupListViewModel() {
    var that = this;

    // Data variables
    this.groups            = ko.observableArray([]);
    this.groupSelected     = ko.observable();
    this.groupDefault      = ko.observable();
    this.usersGroup        = ko.observableArray([]);
    this.usersGroupDefault = ko.observableArray([]);

    // Operation methods
    this.addGroup = function (group) {
        that.groups.push(group);
    };

    this.selectGroup = function (group) {
        that.usersGroup.removeAll();
        that.usersGroupDefault.removeAll();
        that.groupSelected(group.id);
        that.refreshGroups();
    };

    this.addUserGroup = function (user) {
        that.usersGroup.push(user);
    };

    this.deleteUserGroup = function (user) {
        that.usersGroup.remove(user);
    };

    this.addUserGroupDefault = function (user) {
        that.usersGroupDefault.push(user);
    };

    this.deleteUserGroupDefault = function (user) {
        that.usersGroupDefault.remove(user);
    };

    this.addUser = function (user) {
        console.log("addUser");
        user.groupId = that.groupSelected;
        that.deleteUserGroupDefault(user);
        that.addUserGroup(user);
        $.ajax({
            url: $("body").attr("data-base-url") + "/service/user/id/" + user.id + "/group/" + that.groupSelected(),
            type: "PUT",
            statusCode: {
               200: function () {
                   console.log("PUT add");
               }
            },
            error: function () {
                that.deleteUserGroup(user);
                that.addUserGroupDefault(user);
            }

        });
    };

    this.removeUser = function (user) {
        if (that.groupSelected() != that.groupDefault() ) {
            console.log("removeUser");
            user.groupId = that.groupDefault;
            that.deleteUserGroup(user);
            that.addUserGroupDefault(user);
            $.ajax({
                url: $("body").attr("data-base-url") + "/service/user/id/" + user.id,
                type: "PUT",
                statusCode: {
                   200: function () {
                       console.log("PUT removed");
                   }
                },
                error: function () {
                    that.deleteUserGroupDefault(user);
                    that.addUserGroup(user);
                }
            });
        }
    };

    this.refreshGroups = function () {
        $.ajax({
            url: $("body").attr("data-base-url") + "/service/user/group/" + that.groupSelected(),
            statusCode: {
                200: function (data) {
                    var users = data.users;
                    ko.utils.arrayMap(users, function (user) {
                       that.addUserGroup(user);
                    });
                },
                204: function () {
                    console.log("no result");
                }
            },
            error: function () {
                console.log("error");
            }
        });
        if (that.groupSelected() != that.groupDefault() ) {
            $.ajax({
                url: $("body").attr("data-base-url") + "/service/user/group/" + that.groupDefault(),
                statusCode: {
                    200: function (data) {
                        var users = data.users;
                        ko.utils.arrayMap(users, function (user) {
                           that.addUserGroupDefault(user);
                        });
                    },
                    204: function () {
                        console.log("no result");
                    }
                },
                error: function () {
                    console.log("error");
                }
            });
        }
    };

    // Constructor
    this.constructor = function (that) {
        $.ajax({
            url: $("body").attr("data-base-url") + "/service/group/",
            statusCode: {
                200: function (data) {
                    var groups = data.groups;
                    that.groupSelected(groups[0].id);
                    that.groupDefault(groups[0].id);
                    ko.utils.arrayMap(groups, function (group) {
                        that.addGroup(group);
                    });
                },
                204: function () {
                    console.log("no result");
                }
            },
            error: function () {
                console.log("error");
            }
        });
        $.ajax({
            url: $("body").attr("data-base-url") + "/service/user/",
            statusCode: {
                200: function (data) {
                    var users = data.users;
                    ko.utils.arrayMap(users, function (user) {
                        that.addUserGroup(user);
                    });
                },
                204: function () {
                    console.log("no result");
                }
            },
            error: function () {
                console.log("error");
            }
        });
    }(this);
};
/******************************************************************************/
});
