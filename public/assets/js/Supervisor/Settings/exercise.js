/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
require(["jquery"],
function ($) {
/******************************************************************************/
$("#levelForm-minutes")
    .each(function () {
        if (!this.checked) {
            $("#fieldset-clocklevelminutes input[type=radio]").attr("disabled", "disabled");
        }
    })
    .on("click", function () {
        if (this.checked) {
            $("#fieldset-clocklevelminutes input[type=radio]")
                .removeAttr("disabled")
                .first().attr("checked", "checked")
            ;
        } else {
            $("#fieldset-clocklevelminutes input[type=radio]")
                .removeAttr("checked")
                .attr("disabled", "disabled")
            ;
        }
    })
;
/******************************************************************************/
});
