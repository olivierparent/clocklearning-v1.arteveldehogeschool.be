/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["jquery", "knockout","./ModuleViewModel", "ahs/debug"],
function ($, ko, ModuleViewModel) {
/******************************************************************************/
return function SetViewModel() {
    var that = this;
    // Data variables
    this.group   = ko.observable();
    this.modulesAvailable = ko.observableArray([]);
    this.modulesAvailableSelected = ko.observable();
    this.modules = ko.observableArray([]);
    this.busy    = ko.observable(false);
    this.success = ko.observable(false);
    this.error   = ko.observable(false);

    // Operation methods
    this.addModuleAvailable = function (module) {
        this.modulesAvailable.push(new ModuleViewModel(module) );
    };

    this.addModule = function () {
        var module = this.modulesAvailableSelected()[0];
        that.modules.push(module.clone() );
    };

    this.deleteModule = function (module) {
        that.modules.remove(module);
    };

    this.save = function () {
        this.busy(true);
        var modules = ko.utils.arrayMap(that.modules(), function (module) {
            if (module.exerciseTypesSelected().length) {
                return {
                    id: module.id(),
                    amount: parseInt(module.exerciseAmount()),
                    exerciseTypes: module.exerciseTypesSelected()
                };
            }
        });
        if (modules.length) {
            Ahs.Debug.info(ko.toJSON(modules));
            $.ajax({
               url: $("body").attr("data-base-url") + "/service/set/",
               data: ko.toJSON({
                   group: this.group,
                   modules: modules
               }),
               type: "POST",
               contentType: "application/json",
               success: function(result) {
                   Ahs.Debug.success(result);
                   that.success(true);
               },
               error: function(result) {
                   Ahs.Debug.error(result);
                   that.saved(false);
                   that.error(true);
               }
            });
        }
    };

    // Constructor
    this.constructor = function (that) {
        $.ajax({
            url: $("body").attr("data-base-url") + "/service/module/",
            statusCode: {
                200: function (modulesAvailable) {
                    ko.utils.arrayMap(modulesAvailable, function (module) {
                        that.addModuleAvailable(module);
                    });
                },
                204: function () {
                    $("#form-add-module").hide();
                    Ahs.Debug.error("no result");
                }
            },
            error: function () {
                Ahs.Debug.error("error");
                $("#form-add-module").hide();
            }
        });
    }(this);
};
/******************************************************************************/
});
