/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
define(["knockout", "./ExerciseTypeViewModel"],
function (ko, ExerciseTypeViewModel) {
/******************************************************************************/
return function ModuleViewModel(module) {
    var that = this;

    console.log(module);
    // Data variables
    this.id             = ko.observable(module.id);
    this.name           = ko.observable(module.name);
    this.exerciseAmount = ko.observable(module.exerciseAmount);
    this.exerciseTypes  = ko.observableArray([]);

    // Computed variables
    this.exerciseTypesSelected = ko.computed(function() {
        return $.map(that.exerciseTypes(), function (exerciseType) {
            if (exerciseType.include()) {
                return {
                    id: exerciseType.id()
                };
            }
        });
    });

    // Operation methods
    this.addExerciseType = function (exerciseType) {
        this.exerciseTypes.push(new ExerciseTypeViewModel(exerciseType));
    };

    // Clone an object
    this.clone = function () {
        var module = JSON.parse(ko.toJSON(this));
        return new ModuleViewModel(module);
    };

    // Constructor
    this.constructor = function (that) {
        ko.utils.arrayMap(module.exerciseTypes, function (exerciseType) {
            that.addExerciseType(exerciseType);
        });
    }(this);
};
/******************************************************************************/
});
