/******************************************************************************
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *                        aaaAAaaa            HHHHHH                          *
 *                     aaAAAAAAAAAAaa         HHHHHH                          *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
 *                   aAAAAAa    aAAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   AAAAAa      AAAAAA                                       *
 *                   aAAAAAa     AAAAAA                                       *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
 *                         aaAAAAAAAAAA       HHHHHH                          *
 *                                                                            *
 *                                                                            *
 *                                                                            *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
 *                                                                            *
 *                                                                            *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
 *                                                                            *
 *                                                                            *
 ******************************************************************************
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2011-2014 Artevelde University College Ghent
 * @license    http://www.clocklearning.org/LICENSE.txt
 */
require(["bootstrap"],
function() {
    $(document)
        .on("click", ".folderbutton", function () {
            $(this)
                .find("i")
                    .toggleClass("glyphicon-folder-open glyphicon-folder-close")
            ;
        })
        .on("click", "button[data-set-delete]", function () {
            var exerciseSet = $(this).parents("[data-set]").first();
            var id = Number($(this).attr("data-set-delete"));
            exerciseSet.hide();

            $.ajax({
                url: $("body").attr("data-base-url") + "/service/set/id/" + id,
                type: "DELETE",
                statusCode: {
                    200: function (data) {
                        console.log(data);
                    },
                    204: function () {
                        exerciseSet.show();
                    }
                },
                error: function () {
                    console.log("error");
                }
            });
        })
        .on("click", "button[data-exercise-delete]", function () {
            var exercise = $(this).parents("[data-exercise]").first();
            var id = Number($(this).attr("data-exercise-delete"));
            var badge = $(this).parents("[data-set]").first().find(".badge");
            var amount = Number(badge.html());
            badge.html(--amount);
            exercise.hide();

            $.ajax({
                url: $("body").attr("data-base-url") + "/service/exercise/id/" + id,
                type: "DELETE",
                statusCode: {
                    200: function (data) {
                        console.log(data);
                    },
                    204: function () {
                        exercise.show();
                    }
                },
                error: function () {
                    console.log("error");
                }
            });
        })
    ;
    $(".pop-over")
        .popover({
            "container": "body",
            "html": true,
            "placement": "top",
            "trigger": "hover"
        })
    ;
});
