	 ******************************************************************************
	 *                                                                            *
	 *                                                                            *
	 *                                                                            *
	 *                        aaaAAaaa            HHHHHH                          *
	 *                     aaAAAAAAAAAAaa         HHHHHH                          *
	 *                    aAAAAAAAAAAAAAAa        HHHHHH                          *
	 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                          *
	 *                   aAAAAAa    aAAAAAA                                       *
	 *                   AAAAAa      AAAAAA                                       *
	 *                   AAAAAa      AAAAAA                                       *
	 *                   aAAAAAa     AAAAAA                                       *
	 *                    aAAAAAAaaaaAAAAAA       HHHHHH                          *
	 *                     aAAAAAAAAAAAAAAA       HHHHHH                          *
	 *                      aAAAAAAAAAAAAAA       HHHHHH                          *
	 *                         aaAAAAAAAAAA       HHHHHH                          *
	 *                                                                            *
	 *                                                                            *
	 *                                                                            *
	 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t      *
	 *                                                                            *
	 *                                                                            *
	 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION      *
	 *                                                                            *
	 *                                                                            *
	 ******************************************************************************

Clocklearning.org
=================

Deployment instructions
-----------------------

# 1. Database

- Backup existing database.

- Create new database.
    - Forward engineer the schema.
    - Or import a database dump.

# 2. Code

- Copy Application and libraries:

        clocklearning/library/Ahs                      → /private/library/Ahs
        clocklearning/library/languages/               → /private/library/languages/
        clocklearning/library/Zend/                    → /private/library/Zend/
        clocklearning/library/ZFDebug                  → /private/library/ZFDebug
        clocklearning/application/* (except: configs/) → /private/application/*
        clocklearning/application/configs/navigation/* → /private/application/configs/navigation/*

- Create configuration

    `v1.clocklearning.org/application/configs/application.ini` based on `application.template.ini`

- Copy

   `v1.clocklearning.org/public/*` (except: `index.php`) → `/www.klokleren.be/*`
   `v1.clocklearning.org/public/libraries/flags/flags-iso/shiny/64/*`
   rename `index.hosting.php` to `index.php`

