USE `v1_clocklearning_org`;

-- Language
INSERT INTO `Language` (`lan_id`,`lan_name`) VALUES ('EN','English');
INSERT INTO `Language` (`lan_id`,`lan_name`) VALUES ('NL','Nederlands');

-- ExerciseSettings
INSERT INTO `ExerciseSettings` (`exeset_id`,`exeset_timestamp`,`exeset_amount`,`exeset_hour`,`exeset_quarterpast`,`exeset_halfhour`,`exeset_quarterto`,`exeset_minutesincrement`,`exeset_seconds`,`exeset_mode`,`exeset_assistance`,`exeset_subtitles`) VALUES (NULL,NULL,10,TRUE,TRUE,TRUE,TRUE,'0',FALSE,'EXERCISE','MODELLING',TRUE);

-- ModuleSettings
INSERT INTO `ModuleSettings` (`modset_id`,`modset_timestamp`,`modset_twentyfour`,`modset_quadrants`,`modset_sound`,`modset_mascot`,`modset_colour`,`modset_colour-switch`,`modset_analogue-hours`,`modset_analogue-minutes`,`modset_analogue-dialnumbers`,`modset_analogue-colours`,`modset_digital-hours`,`modset_digital-minutes`,`modset_digital-twentyfour`,`modset_digital-colours`) VALUES (NULL,NULL,TRUE,'OFFICIAL',FALSE,TRUE,TRUE,FALSE,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- Admin
INSERT INTO `Admin` (`adm_id`,`adm_timestamp`,`adm_email`,`adm_username`,`adm_password`,`adm_givenname`,`adm_familyname`,`adm_active`,`adm_deleted`,`lan_id`) VALUES (NULL,NULL,'admin@clockreading.org','admin','35a9b2c4b5a2239e2706287f55772bacb507af333de1616d49cd2ce1c3d8f484eae2fa9e0ea870e81ec7242baa5e6fbfa37b47ef7b311af313dbd111c1452b13','Default','Admin',TRUE,FALSE,'EN');

-- Supervisor
INSERT INTO `Supervisor` (`sup_id`,`sup_timestamp`,`sup_email`,`sup_username`,`sup_password`,`sup_givenname`,`sup_familyname`,`sup_role`,`sup_active`,`sup_deleted`,`exeset_id`,`lan_id`,`modset_id`) VALUES (NULL,NULL,'supervisor@clockreading.org','supervisor','60638286f4a834b35c806edc521ab95897c444a931b499cb5c25cff59dc1af8d68527a27b525d045a39ef4be025b023cd7f34813f5448fd7f9c970ed5a30599a','Default','Supervisor','THERAPIST',TRUE,FALSE,1,'EN',1);

-- Group
INSERT INTO `Group` (`grp_id`,`grp_timestamp`,`grp_name`,`grp_active`,`grp_deleted`,`sup_id`,`exeset_id`,`modset_id`) VALUES (NULL,NULL,'--DEFAULT--',TRUE,FALSE,1,NULL,NULL);

-- User
INSERT INTO `User` (`usr_id`,`usr_timestamp`,`usr_username`,`usr_password`,`usr_givenname`,`usr_familyname`,`usr_birthday`,`usr_role`,`usr_active`,`usr_deleted`,`grp_id`,`exeset_id`,`modset_id`) VALUES (NULL,NULL,'user','3c90fab5fb833f372f8451f8c9e415266ebac9a46071244a767611908c09b2feb4a60291a8206cc504a49308832c5ed4bfd1732dbf4a0e1fd9810f40eeba4224','Default','User','2005-01-01','CLIENT',TRUE,FALSE,1,NULL,NULL);

-- MessageRecipient
INSERT INTO `MessageRecipient` (`msgrcp_id`,`msgrcp_email`,`msgrcp_name`) VALUES (NULL,'olivier.parent@arteveldehs.be','Olivier Parent');
INSERT INTO `MessageRecipient` (`msgrcp_id`,`msgrcp_email`,`msgrcp_name`) VALUES (NULL,'veerle.vanvooren@arteveldehs.be','Veerle Van Vooren');

-- Module
INSERT INTO `Module` (`mod_id`,`mod_name`,`mod_active`) VALUES (1,'Module clock knowledge',FALSE);
INSERT INTO `Module` (`mod_id`,`mod_name`,`mod_active`) VALUES (2,'Module clock reading',TRUE);
INSERT INTO `Module` (`mod_id`,`mod_name`,`mod_active`) VALUES (3,'Module setting time',TRUE);
INSERT INTO `Module` (`mod_id`,`mod_name`,`mod_active`) VALUES (4,'Module transposing clocks',TRUE);
INSERT INTO `Module` (`mod_id`,`mod_name`,`mod_active`) VALUES (5,'Module interpreting clocks',FALSE);

-- ExerciseType
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (1,'Recognizing: analogue clock → 4 notations',FALSE,TRUE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (2,'Recognizing: digital clock → 4 notations',TRUE,TRUE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (3,'Recognizing: notation → 4 analogue clocks',FALSE,TRUE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (4,'Recognizing: notation → 4 digital clocks',TRUE,TRUE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (5,'Combining: 4 notations → 4 analogue clocks',FALSE,TRUE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (6,'Combining: 4 notations → 4 digital clocks',TRUE,TRUE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (7,'Time notation: analogue clock → fill in notation',FALSE,FALSE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (8,'Time notation: digital clock → fill in notation',TRUE,FALSE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (9,'Time notation: analogue clock → assignment, fill in notation',FALSE,FALSE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (10,'Time notation: digital clock → assignment, fill in notation',TRUE,FALSE,2);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (11,'Clock setting: notation → set analogue clock',FALSE,TRUE,3);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (12,'Clock setting: notation → set digital clock',TRUE,TRUE,3);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (13,'Clock setting: notation → assignment, set analogue clock',FALSE,FALSE,3);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (14,'Clock setting: notation → assignment, set digital clock',TRUE,FALSE,3);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (15,'Recognizing: analogue clock → 4 digital clocks',FALSE,TRUE,4);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (16,'Recognizing: digital clock → 4 analogue clocks',TRUE,TRUE,4);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (17,'Combining: 4 analogue clocks → 4 digital clocks',FALSE,TRUE,4);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (18,'Clock setting: analogue clock → set digital clock',FALSE,TRUE,4);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (19,'Clock setting: digital clock → set analogue clock',TRUE,TRUE,4);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (20,'Clock setting: analogue clock → set 2 digital clocks (12/24 hour)',FALSE,FALSE,4);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (21,'Time notation: 2 analogue clocks → fill in 2 notations, assignment',FALSE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (22,'Time notation: 2 digital clocks → fill in 2 notations, assignment',TRUE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (23,'Clock setting: analogue clock → assignment, set analogue clock later',FALSE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (24,'Clock setting: digital clock → assignment, set digtal clock later',TRUE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (25,'Order: 2 analogue clocks → which clock shows earlier?',FALSE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (26,'Order: 2 digital clocks → which clock shows earlier?',TRUE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (27,'Order: 2 analogue clocks → which clock shows later?',FALSE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (28,'Order: 2 digital clocks → which clock shows later?',TRUE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (29,'Order: 4 analogue clocks → order clocks from early to late',FALSE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (30,'Order: 4 digital clocks → order clocks from early to late',TRUE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (31,'Order: 4 analogue clocks → order clocks from late to early',FALSE,FALSE,5);
INSERT INTO `ExerciseType` (`exetyp_id`,`exetyp_name`,`exetyp_digital`,`exetyp_active`,`mod_id`) VALUES (32,'Order: 4 digital clocks → order clocks from late to early',TRUE,FALSE,5);
